$(document).ready(function() {
	var table, datatables;
	//Activate Datables for tables with class .data-tables
    var url = location.protocol + "//" + location.hostname;
    datatables = $('.datatables').dataTable( {
    	"sDom": "<'row-fluid'<'span3'T><'span9'f>r>t<'row-fluid'<'span3'l><'span9'p>>",
		"oTableTools": {
    		"sSwfPath": url+"resources/js/copy_csv_xls_pdf.swf",
    		"aButtons": [
				"copy",
				"print",
				{
					"sExtends":    "collection",
					"sButtonText": 'Save <span class="caret" />',
					"aButtons":    [ "csv", "xls", "pdf" ]
				}
			]
		},
        "aaSorting": [],
		"sPaginationType": "bootstrap",
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]], 
		"iDisplayLength" : 10,
    } );
    
    //Activate Datatable with minimal features for tables with class .table
    table = $('.plain').dataTable( {
    	"bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false
    } );
    
    //Homepage Marquee
    $('marquee').marquee('pointer').mouseover(function () {
        $(this).trigger('stop');
    }).mouseout(function () {
        $(this).trigger('start');
    }).mousemove(function (event) {
        if ($(this).data('drag') == true) {
            this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
        }
    }).mousedown(function (event) {
        $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
    }).mouseup(function () {
        $(this).data('drag', false);
    });
    
    //Home Page Tabs
	$('#country-volume a:first').tab('show'); 
	$('#product-volume a:first').tab('show'); 
	
	 //Map Tabs
    $('#map_tab a').click(function (e) {
    	e.preventDefault();
    	$(this).tab('show');
    });
	
	//Chosen Jquery plugin
	$(".chzn-select").chosen();
	
} );
