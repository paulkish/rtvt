$(document).ready(function() {
	var url = location.protocol + "//" + location.hostname + '/rtvt/';
    $("#map").goMap();
    //Get Data via JSON
    $.get('site/json', function(data) {
		$.each( data.positions, function(i, marker) {
			var marker = this;
			$.goMap.createMarker({
				latitude: marker.y,
				longitude: marker.x,
				title: marker.n,
				html: {
					content: '<center><img src="'+url+'resources/img/ajax-loader.gif" alt="Loading.."></img></center>',
                	ajax: 'site/ajax/'+marker.i 
            	},
				icon: '../resources/icons/'+marker.o+'.png'
			});
		});
		$.goMap.fitBounds('visible'); 
	}, 'json');
    
    //Map Controls
    /*
    $("[id^=input]").bind("change keyup input",function() {
		$.post(url+'site/json',$('#map-control').serialize(),function(data)
		{
			//Clear all map markers
			$.goMap.clearMarkers(); 
			$.each( data.positions, function(i, marker) {
				var marker = this;
				$.goMap.createMarker({
					latitude: marker.y,
					longitude: marker.x,
					title: marker.name,
					html: {
						content: '<center><img src="'+url+'/resources/img/ajax-loader.gif" alt="Loading.."></img></center>',
	                	ajax: url+'site/ajax/'+marker.i 
	            	}, 
					icon: url+'resources/icons/'+marker.o+'.png'
				});
			});
			$.goMap.fitBounds('visible'); 
		}
		,'json'); 
	}); */
});