$(document).ready(function() {
	
	// On change set cookie for pagination library to read
	$('.page-size').change(function(){
	    $.cookie('per_page', $(this).val());
	    window.location.reload(true); 
	});

	var pagesize = $.cookie("per_page");
	if(typeof pagesize != 'undefined'){
		$('.page-size').val(pagesize);
	}

	// btn-convert on click 
	$('.btn-convert').click(function(){
		$.cookie('currency', $('#selCurrency').val());
		$.cookie('measure', $('#selMeasure').val());
	    window.location.reload(true); 
	    return false;
	});

	$('#selMeasure').val($.cookie("measure"));
	$('#selCurrency').val($.cookie("currency"));

 	var picker = new Pikaday({
        field: document.getElementById('inputStart'),
        format: 'YYYY-MM-DD',
        defaultDate: new Date(moment().subtract({
            day: 0
        }).toDate()),
        maxDate: new Date(moment().subtract({
            day: 0
        }).toDate())
    });

    var picker1 = new Pikaday({
        field: document.getElementById('inputEnd'),
        format: 'YYYY-MM-DD',
        defaultDate: new Date(moment().subtract({
            day: 0
        }).toDate()),
        maxDate: new Date(moment().subtract({
            day: 0
        }).toDate())
    });

    $('.submit-change').change(function(){
    	// submit form
    	$(this).parents("form").submit();
    });

    // multiple select
    $('.multiple-select').multipleSelect({
        selectAll:false,
        placeholder: "Please select country first",
        width: "100%"
    });
});