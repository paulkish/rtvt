<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// get rates latest or for date
function exchange_rate($date=NULL,$avg=1){
	// get rate 
	$CI = get_instance();
    $CI->load->model('codegen_model');

    // if avg is one then get data based on individual days
    if($avg == 2){
		$week_date = explode(' ',$date); // separate from week 23,2015 week and 23,2015
		$date = $week_date[1];
		$week = str_replace(',','',$week_date[1]);
		$year = $week_date[2];
		$query = $CI->db->query(
	    	"SELECT avg(kshusd) as kshusd,avg(tshusd) as tshusd,avg(ugxusd) as ugxusd,
	    	avg(bifusd) as bifusd,avg(rwfusd) as rwfusd,avg(sspusd) as sspusd  
	    	FROM exchange_rates WHERE YEAR(date) = '$year' AND WEEK(date) = '$week'"
	    );
	    $rates = $query->row(); 
	}elseif($avg == 3){
		$date = explode(',',$date);
		$month_name = date_parse($date[0]);
		$month = $month_name['month'];
		$year = str_replace(' ','',$date[1]);
		$query = $CI->db->query(
	    	"SELECT avg(kshusd) as kshusd,avg(tshusd) as tshusd,avg(ugxusd) as ugxusd,
	    	avg(bifusd) as bifusd,avg(rwfusd) as rwfusd,avg(sspusd) as sspusd 
	    	FROM exchange_rates WHERE YEAR(date) = '$year' AND MONTH(date) = '$month'"
	    );
	    $rates = $query->row(); 
	}elseif($avg == 4){
		$query = $CI->db->query(
	    	"SELECT avg(kshusd) as kshusd,avg(tshusd) as tshusd,avg(ugxusd) as ugxusd,
	    	avg(bifusd) as bifusd,avg(rwfusd) as rwfusd,avg(sspusd) as sspusd  
	    	FROM exchange_rates WHERE YEAR(date) = '$date'"
	    );
	    $rates = $query->row(); 
	}else{
		if($date != NULL){
	    	$rates = $CI->codegen_model->record('exchange_rates','kshusd,tshusd,ugxusd,bifusd,rwfusd,sspusd',"date = '$date'"); 
	    	if($rates == NULL){ // get record closest to date
	    		$query = $CI->db->query(
	    			"SELECT kshusd,tshusd,ugxusd,bifusd,rwfusd,sspusd,date FROM exchange_rates ORDER by ABS(DATEDIFF(date,'$date')) LIMIT 1"
	    		);
	    		$rates = $query->row();
	    	}
	    } else{
	    	$rates = $CI->codegen_model->record('exchange_rates','kshusd,tshusd,ugxusd,bifusd,rwfusd,sspusd',NULL,'date');
	    }
	}
    return $rates;
}

/**
* convert 
* read from cookie currency and measure
* default is USD/KG
* */
function convert($rate,$value){
	if(isset($_COOKIE['currency']) && isset($_COOKIE['measure'])){
		$currency = $_COOKIE['currency'];
		$measure = $_COOKIE['measure'];
	}else{
		$CI = get_instance();

		$country = 'Kenya';
		// ip lookup is failing default to Kenya
		/*
    	$CI->load->library('ip2location');
		// get ip and set country
        $ip = $CI->input->ip_address();
        $location = $CI->ip2location->getCountry($ip);
        if($CI->ip2location->getError() == null){
            $country = $location['countryName'];
        }else{
            $country = null;
        }*/

        //set default country
        switch ($country) {
			case 'Kenya':
				$currency = 'KES';
				break;
			case 'Uganda':
				$currency = 'UGX';
				break;
			case 'Tanzania':
				$currency = 'TSH';
				break;
			case 'Rwanda':
				$currency = 'RWF';
				break;
			case 'Burundi':
				$currency = 'BIF';
				break;
			case 'South Sudan':
				$currency = 'SSP';
				break;
			default:
				$currency = 'USD';
				break;
		}

        $measure = 90; // default measure

        set_default($country); // set default
	}

	// calculate value by rate
	switch ($currency) {
		case 'KES':
			$value = $value * $rate->kshusd;
			break;
		case 'TSH':
			$value = $value * $rate->tshusd;
			break;
		case 'UGX':
			$value = $value * $rate->ugxusd;
			break;
		case 'BIF':
			$value = $value * $rate->bifusd;
			break;
		case 'RWF':
			$value = $value * $rate->rwfusd;
			break;
		case 'SSP':
			$value = $value * $rate->sspusd;
			break;
		default: // do nothing
			break;
	}

	// calculate value by measure
	$value = $value * $measure;
	
	return round($value);
}

// set currency and measure
function set_default($country){
	if(!isset($_COOKIE['currency']) && !isset($_COOKIE['measure'])){
		switch ($country) {
			case 'Kenya':
				setcookie('currency','KES');
				setcookie('measure',90);
				break;
			case 'Uganda':
				setcookie('currency','UGX');
				setcookie('measure',90);
				break;
			case 'Tanzania':
				setcookie('currency','TSH');
				setcookie('measure',90);
				break;
			case 'Rwanda':
				setcookie('currency','RWF');
				setcookie('measure',90);
				break;
			case 'Burundi':
				setcookie('currency','BIF');
				setcookie('measure',90);
				break;
			case 'South Sudan':
				setcookie('currency','SSP');
				setcookie('measure',90);
				break;
			default:
				setcookie('currency','USD');
				setcookie('measure',90);
				break;
		}
	}
}

function get_conversion(){
	if(isset($_COOKIE['measure']) && isset($_COOKIE['currency'])){
		if($_COOKIE['measure'] < 1000){
			$measure = $_COOKIE['measure'].'Kg';
		}else{
			$measure = 'MT';
		}
		return $_COOKIE['currency'].'/'.$measure;
	}else{
		return '';
	}
}