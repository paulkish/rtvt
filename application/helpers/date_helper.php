<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function formatDate($date)
{
	date_default_timezone_set('UTC');
	return  (strtotime($date) * 1000) - (strtotime('02-01-1970 00:00:00') * 1000);
}

function modDate($e_date)
{
	$mod_date = date('Y,n,j', strtotime($e_date));
	return $mod_date;
}

function date_week($date)
{
	return date('Y-m-d', strtotime('-14 days', strtotime($date)));
}

function date_today()
{
	return date('Y-m-d', strtotime('2012-10-01'));
}

function format_date($dDate,$value)
{
    $Date = DateTime::createFromFormat( 'Y-m-d', $dDate); 
    $str = '[Date.UTC(' . $Date->format('Y') . ', ';
    $str .= $Date->format('n') - 1 . ', ';
    $str .= $Date->format('j') . '), ' . $value . ']';
    return $str;
}