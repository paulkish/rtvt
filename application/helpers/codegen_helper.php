<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function p($a)
{
    echo '<pre>';
    print_r($a);
    echo '</pre>';

}

function v($a)
{
    echo '<pre>';
    var_dump($a);
    echo '</pre>';

}

function clean_header($array)
{
    $CI = get_instance();
    $CI->load->helper('inflector');
    foreach($array as $a){
        $arr[] = humanize($a);
    }
    return $arr;
}

function limit_words($string, $word_limit=20)
{
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}

function segment($id)
{
    $segment = array(1=>'Retail Price', 2=>'Wholesale Price',3=>'Farmgate Price',4=>'Volumes');
    return $segment[$id];
}

// Basic scraping function
function scrape_between($data, $start, $end){
    $data = stristr($data, $start); // Stripping all data from before $start
    $data = substr($data, strlen($start));  // Stripping $start
    $stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
    $data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
    return $data;   // Returning the scraped data from the function
}

function table_summary($data,$header=NULL){
    if($header == NULL){
        $header = array_keys($data[0]);
    }

    $formatted = array();
    $existing_rate = array(); 
    for($i=0;$i<count($data);$i++)
    {
        $id = array_values($data[$i]);
        $date = $data[$i]['date'];
        // get rate only if date has changed else reuse existing rate
        if(!isset($existing_rate[$date])){
            $existing_rate[$date] = exchange_rate($date);
        }
        $rate = $existing_rate[$date];  
        foreach ($data[$i] as $key => $format){
            if($key == 'retail_Price' || $key == 'wholesale_Price'){
                $formatted[$i][] = convert($rate,$format);
            } else{
                $formatted[$i][] = $format;
            }
        }               
    }

    $CI =& get_instance();

    //set table styling
    $tmpl = array ('table_open'=>'<table class="table table-bordered table-condensed">');
    $CI->table->set_template($tmpl); 
        
    $clean_header = clean_header($header);
    $CI->table->set_heading($clean_header); 

    //Table
    echo $CI->table->generate($formatted);
}


// Selected
function selected($option,$value){
    if($option == $value){
        return 'selected="selected"';
    }
}

function about_articles(){
    $CI = get_instance();
    $query = $CI->db->query(
        "SELECT * FROM articles WHERE category = 4"
    );
    $pages = $query->result(); 
    return $pages;
}

function currencies(){
    $CI = get_instance();
    $query = $CI->db->query(
        "SELECT Currency FROM country WHERE publish = 1"
    );
    $currencies = $query->result_array(); 
    return $currencies;
}

function limit_data(){
    $CI = get_instance();
    $where = '';

    // check if logged in 
    if($CI->ion_auth->logged_in()){
        // check group
        if($CI->ion_auth->in_group('admin') || $CI->ion_auth->in_group('webmaster')){
            $where = 'interval 40 year'; // big value so that admin can see everything
        }elseif($CI->ion_auth->in_group('LevelOne')){
            $where = 'interval 3 year';
        }elseif($CI->ion_auth->in_group('LevelTwo')){
            $where = 'interval 2 year';
        }elseif($CI->ion_auth->in_group('LevelThree')){
            $where = 'interval 1 year';
        }else{
            $where = 'interval 6 month';
        }
    }else{
        $where = 'interval 1 week';
    }

    return $where;
}