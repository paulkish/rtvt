<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends CI_Model 
{
    public function country_list($condition=TRUE)
    {
		if($condition==TRUE)
		{
			$where = "Country_name ='Kenya' OR Country_name ='Uganda' OR Country_name ='Tanzania' ";
			$this->db->where($where);
		}
		$this->db->where('publish','1');
        $query = $this->db->get('country');
		return $query->result();
    }

    public function countrylist($id=NULL)
    {
        $this->db->select('id,Country_name as country_name,capital');
        if($id != NULL)
            $this->db->where('id',$id);
        $this->db->where('publish','1');
        $query = $this->db->get('country');
        return $query->result();
    }

    public function getCountryById($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get('country');
        return $query->row();
    }

    public function country_volumes($country)
    {
        $latest_date = $this->latest_date($country);
        $week_ago = date('Y-m-d', strtotime('-10 days', strtotime($latest_date)));
        $query = $this->db->query(
        		"
        		SELECT GROUP_CONCAT(sum_vol ORDER BY date asc) as data,Product as name
        		FROM (
        				SELECT concat( '[Date.UTC(',DATE_FORMAT(DATE_SUB(date, INTERVAL 1 MONTH),'%Y,%m,%e'), '),',SUM(Available_Volume),']') as sum_vol,Product,date
        				FROM whouse_cummulatives v
        				WHERE Country = '{$country}' AND
        				date BETWEEN '{$week_ago}' AND '{$latest_date}'
						GROUP by Product, date
        		) v
				GROUP by Product
        		");
        return $query->result();
    }

    public function latest_date($country)
    {
        $this->db->select('date');
        $this->db->where('Country',$country);
        $this->db->order_by('date','desc');
        $this->db->limit(1);
        $query = $this->db->get('whouse_cummulatives');
        return $query->row()->date;
    }

    public function ip_country($ip)
    {
        $this->load->library('ip2location');
        $location = $this->ip2location->getCountry($ip);
        if($this->ip2location->getError() == null){
            return $location['countryName'];
        }else{
            return 'Unable to detect';
        }
    }

    public function update_cumulative()
    {
        //This function will check the volumes for today from vw_whouse_cumulatives then add the new records
        $this->db->select('country as Country,product_name as Product,name as Grade,Available_Volume');
        $query = $this->db->get('vw_whouse_cummulatives');
        $cummulative = $query->result_array();
        $this->db->insert_batch('whouse_cummulatives', $cummulative);
        return ($this->db->affected_rows() > 1) ? false : true;
    }
}
