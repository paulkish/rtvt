<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*Border Model
*/
class Border extends CI_Model
{
	public function borders($product_id,$date,$limit=10)
	{
		$this->db->where('date',$this->latest_date());
		$this->db->where('product_id',$product_id);
		$this->db->order_by('volume','desc');
		$this->db->limit($limit);
		$query = $this->db->get('vw_crossborder');
		return $query->result();
	}

	public function borders_low($product_id,$date,$limit=5)
	{
		$this->db->where('date',$this->latest_date());
		$this->db->where('product_id',$product_id);
		$this->db->order_by('volume','asc');
		$this->db->limit($limit);
		$query = $this->db->get('vw_crossborder');
		return $query->result();
	}

	public function latest_date()
	{
		$this->db->order_by('date','desc');
		$this->db->limit(1);
		$query = $this->db->get('vw_crossborder');
		return $query->row()->date;
	}

	public function border_list($id=NULL)
	{
		$this->db->select('border_id as id,border as border_name,Longitude as x,Latitude as y,XMAX as x_max,
						   YMAX as y_max,XMIN as x_min,YMIN as y_min');
		if($id != NULL)
            $this->db->where('border_id',$id); //search by id
        $query = $this->db->get('crossborder_points');
		return $query->result();
	}

	/**
	* Daily Volume
	*/
	public function daily_volume($id=NULL,$product_id=NULL)
	{
		$this->db->select('trade_point_name as border_name,product_name,Source_Country as source,Destination_Country as destination,volume');
		if($id != NULL)
			$this->db->where('border_id',$id); //search by market id
		if($product_id != NULL)
			$this->db->where('product_id',$product_id); //search by product id
		$this->db->where('date',date('Y-m-d'));
		$this->db->where('publish','1');
		$query = $this->db->get('vw_crossborder');
		return $query->result();
	}

	/**
	* Search Volumes
	*/
	public function search_volume($id=NULL,$product_id=NULL,$start=NULL,$end=NULL)
	{
		$this->db->select('trade_point_name as border_name,product_name,Source_Country as source,Destination_Country as destination,volume,date');
		if($id != NULL)
			$this->db->where('border_id',$id); //search by market id
		if($product_id != NULL)
			$this->db->where('product_id',$product_id); //search by product id
		if($start != NULL AND $end !=NULL)
		{
			$start = date('Y-m-d',strtotime($start));
			$end = date('Y-m-d',strtotime($end));
			$where = "date BETWEEN '$start' AND '$end'";
			$this->db->where($where); //search by date
		}
		else
			$this->db->where('date',date('Y-m-d'));

		$query = $this->db->get('vw_crossborder');
		return $query->result();
	}
}