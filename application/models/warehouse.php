<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Warehouse extends CI_Model {

	/**
	 * Provides warehouse information
	 * @param warehouse Id $id
	 */
	public function warehouse_info($id)
	{
		$query = $this->db->get_where('warehouse', array('id' => $id),1);
		return $query->row();
	}

	/**
	 * List of warehouses map data
	 */
	public function warehouse_list($id=NULL)
	{
	    $this->db->select('id as i,warehouse_name as n,POINT_X as x, POINT_Y as y,Ownership as o');
	    if($id != NULL)
            $this->db->where('id',$id); //search by id
		$query = $this->db->get('warehouse');
		return $query->result();
	}

	/**
	 * List of warehouses
	 */
	public function warehouses_list($id=NULL,$country=NULL)
	{
		$this->db->select('id,warehouse_name,whouse_code as code,country,POINT_X as x,POINT_Y as y,Capacity as capacity,
							email,Location as location,Ownership as ownership,phone, whouseType as type,contactPerson as contact');
	    if($id != NULL)
            $this->db->where('id',$id); //search by id
        if($country != NULL)
            $this->db->where('country',$country); //search by country
		$query = $this->db->get('warehouse');
		return $query->result();
	}

	/**
	* Daily Volume
	*/
	public function daily_volume($id=NULL,$grade_id=NULL)
	{
		$this->db->select('warehouse_name,product_name,name as grade,volume,time');
		if($id != NULL)
			$this->db->where('whouse_id',$id); //search by market id
		if($grade_id != NULL)
			$this->db->where('grade_id',$grade_id); //search by product id
		$this->db->where('date',date('Y-m-d'));
		$query = $this->db->get('vw_warehouse_data_flow');
		return $query->result();
	}

	/**
	* Search Volumes
	*/
	public function search_volume($id=NULL,$grade_id=NULL,$start=NULL,$end=NULL)
	{
		$this->db->select('warehouse_name,product_name,name as grade,volume,date,time');
		if($id != NULL)
			$this->db->where('whouse_id',$id); //search by market id
		if($grade_id != NULL)
			$this->db->where('grade_id',$grade_id); //search by product id
		if($start != NULL AND $end !=NULL)
		{
			$start = date('Y-m-d',strtotime($start));
			$end = date('Y-m-d',strtotime($end));
			$where = "date BETWEEN '$start' AND '$end'";
			$this->db->where($where); //search by date
		}
		else
			$this->db->where('date',date('Y-m-d'));

		$query = $this->db->get('vw_warehouse_data_flow');
		return $query->result();
	}

	public function warehouse_link()
	{
	    $this->db->select('id,warehouse_name,whouse_code,Capacity,country');
		$query = $this->db->get('warehouse');
		return $query->result();
	}

	/**
	 * Last update time for warehouse capacity
	 *
	 */
	public function warehouse_update($id)
	{
	    $this->db->select('time, date');
	    $this->db->where('whouse_id',$id);
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $this->db->limit(1);
	    $query = $this->db->get('warehouse_data_flow');
		return $query->row();
	}

	public function latest_entry()
	{
		$this->db->select('date');
		//$this->db->where('publish','1');
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $this->db->limit(1);
	    $query = $this->db->get('vw_warehouse_data_flow');
		return $query->row();
	}

	public function warehouse_volume($id)
	{
	    $this->db->where('whouse_id',$id);
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $query = $this->db->get('warehouse_data_flow');
		return $query->result();
	}

	public function warehouse_volumes($id)
	{
	     $query = $this->db->query(
        		"
        		SELECT GROUP_CONCAT(sum_vol ORDER BY date asc) as data,product_name as name
        		FROM (
        				SELECT concat( '[Date.UTC(',DATE_FORMAT(DATE_SUB(date, INTERVAL 1 MONTH),'%Y,%m,%e'), '),',stock,']') as sum_vol,product_name,name,date
        				FROM vw_warehouse_data_flow v
        				WHERE whouse_id = $id
        				GROUP by product_name,date
						ORDER by date desc
        		) v
        		GROUP by product_name
        		");
        return $query->result();
	}

	public function warehouse_search($country=NULL,$product=NULL,$capacity=NULL,$category=NULL)
	{
		$query = 'SELECT w.id as i,w.warehouse_name as n,POINT_X as x,POINT_Y as y,Capacity as c,Ownership as o';
		$query .= ' FROM vw_warehouse_data_flow v, warehouse w';
		$query .= ' WHERE v.whouse_id = w.id ';
		if(isset($country) AND $country != NULL)
			$query .= " AND w.country IN ('".implode("','",$country)."') ";
		if(isset($product) AND $product != NULL)
			$query .= " AND product_name IN ('".implode("','",$product)."') ";
	    if(isset($capacity) AND $capacity != NULL)
			$query .= " AND stock BETWEEN  '{$capacity}' ";
		/*
        if(isset($category) AND $category != NULL)
			$query .= " AND Ownership = '{$category}' ";
		*/
	    $query .= ' GROUP by w.id';
	    $q = $this->db->query($query);
	    return $q->result();
	}

	public function search($country=NULL,$product=NULL,$capacity=NULL)
	{
		$query = 'SELECT w.warehouse_name,Capacity as capacity,Ownership as ownership,w.country';
		$query .= ' FROM vw_warehouse_data_flow v, warehouse w';
		$query .= ' WHERE v.whouse_id = w.id ';
		if(isset($country) AND $country != NULL)
			$query .= " AND w.country IN ('".implode("','",$country)."') ";
		if(isset($product) AND $product != NULL)
			$query .= " AND product_name IN ('".implode("','",$product)."') ";
	    if(isset($capacity) AND $capacity != NULL)
			$query .= " AND stock > '{$capacity}' ";
	    $query .= ' GROUP by w.id';
	    $q = $this->db->query($query);
	    return $q->result();
	}

    public function warehouse__search($country=NULL,$product=NULL,$capacity=NULL,$category=NULL,$start=NULL,$end=NULL)
	{
		$query = 'SELECT w.id as i,w.warehouse_name as n,GROUP_CONCAT(DISTINCT product SEPARATOR " ") as product,Capacity as c,Ownership as o,w.country';
		$query .= ' FROM (SELECT *,concat("<tr><td>",product_name,"</td><td>",name,"</td><td>",stock,"</td><td>",date,"</td><tr>") as product FROM vw_warehouse_data_flow) v, warehouse w';
		$query .= ' WHERE v.whouse_id = w.id ';
		if(isset($country) AND $country != NULL)
			$query .= " AND w.country IN ('".implode("','",$country)."') ";
		if(isset($product) AND $product != NULL)
			$query .= " AND product_name IN ('".implode("','",$product)."') ";
	    if(isset($capacity) AND $capacity != NULL)
			$query .= ' AND stock BETWEEN '.$capacity.' ';
		/*
        if(isset($category) AND $category != NULL)
			$query .= " AND Ownership = '{$category}' ";
		*/
        if(isset($start) AND $start != NULL AND isset($end) AND $end != NULL)
            $query .= " AND date between '{$start}' AND '{$end}'";
	    $query .= ' GROUP by w.id';
	    $q = $this->db->query($query);
	    return $q->result();
	}

	public function json($id)
	{
	     $this->load->library('datatables');
         $table = 'vw_warehouse_data_flow';
         $this->datatables
              ->select('product_name,name,volume,direction,time,date')
              ->from($table)
              ->where('whouse_id',$id);
         return $this->datatables->generate();
	}

	public function warehouse_vols($id)
	{
	    //Get Warehouse Volumes
	    $query = $this->db->query("SELECT whouse_id,product_name,name,stock,volume,direction,time,date
        		 FROM vw_warehouse_data_flow WHERE whouse_id = $id  ORDER by id desc LIMIT 100");
        if ($query->num_rows() > 0)
        {
	        $tmpl = array ( 'table_open'  => '<table class="datatables table table-condensed table-bordered table-striped volumes">' );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Product Name','Grade','RTVT Volume(MT)' ,'Inflow/Outflow','Date','Time');
	        $q = $query->result();
	        foreach($q as $t)
	        {
	            if($t->direction == 'IN'){
			        $sign = '<span class="text-up">+'.$t->volume.'</span> ';
			    }else{
			        $sign = '<span class="text-down">-'.$t->volume.'</span> ';
			    }
	            $this->table->add_row(anchor('rtvt/site/product_info/'.$t->product_name,$t->product_name),$t->name,$t->stock,$sign,$t->date,$t->time);
	        }
			return $this->table->generate();
        }
        else
        {
            return '';
        }
	}

	public function all_warehouse_data()
	{
		$latest_date = $this->latest_entry();
	    $this->db->select('whouse_id,name,warehouse_name,product_name,name,stock,volume,direction');
	    $this->db->where('date',$latest_date->date);
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $query = $this->db->get('vw_warehouse_data_flow');
		return $query->result();
	}

	public function product_volume($grade_id)
	{
		//Find latest entry date
		$latest = $this->latest_date($grade_id);

        $query = $this->db->query("
	        		SELECT whouse_id,warehouse_name,stock,volume,direction,date
	        		FROM vw_warehouse_data_flow
	        		WHERE grade_id = $grade_id
	        		AND date = '{$latest}'
	        		GROUP by whouse_id
	        		ORDER by stock desc
	        		LIMIT 5
        		");
        if ($query->num_rows() > 0)
        {
	        $tmpl = array(
		      'table_open'=>'<table style="
		            width: 100%; margin-bottom: 20px;
		            border: 1px solid #dddddd;
		            border-collapse:collapse;
		            background-color: #ffffff;">',
		      'heading_cell_start'  => '<th style="font-weight: bold;
		            padding: 8px;
		            line-height: 20px;
		            text-align: left;
		            border: 1px solid #dddddd;
		            vertical-align: top;">',
		      'cell_start' => '<td style="
		            padding: 8px;
		            line-height: 20px;
		            text-align: left;
		            vertical-align: top;
		            border: 1px solid #dddddd;
		            ">',
		      'cell_alt_start'  => '<td style="
		            padding: 8px;
		            line-height: 20px;
		            text-align: left;
		            vertical-align: top;
		            border: 1px solid #dddddd; ">',
		      );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Warehouse', 'RTVT Volume(MT)' ,'Inflow/Outflow','Date');
	        $q = $query->result();
	        foreach($q as $t)
	        {
	            if($t->direction == 'IN'){
			        $sign = '<span class="text-up">+'.$t->volume.'</span> ';
			    }else{
			        $sign = '<span class="text-down">-'.$t->volume.'</span> ';
			    }
	            $this->table->add_row(anchor(base_url('rtvt/site/warehouse_info/'.$t->whouse_id),$t->warehouse_name),$t->stock,$sign,$t->date);
	        }
			return $this->table->generate();
        }
        else
        {
            return '';
        }
	}

	public function latest_date($id)
	{
		$this->db->select('date');
		$this->db->where('grade_id',$id);
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $this->db->limit(1);
	    $query = $this->db->get('vw_warehouse_data_flow');
	    if($query->num_rows() > 0)
	    {
	    	return $query->row()->date;
	    }
	    else
	    {
	    	return date('Y-m-d');
	    }

	}

	public function cumulative_volume($country)
	{
	    $query = $this->db->query("SELECT product_name,SUM(volume) as sum_vol
        		 FROM vw_warehouse_data_flow WHERE country = '{$country}' GROUP by country,product_name");
	    if ($query->num_rows() > 0)
        {
	        $tmpl = array ( 'table_open'  => '<table class=" highchart table table-condensed  table-bordered table-striped volume">' );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Product','Volume');
			return $this->table->generate($query);
        }
        else
        {
            return '';
        }
	}

	public function cumulative_json($country)
	{
		$query = $this->db->query("SELECT SUM(volume) as sum_vol,product_name,date
        		 FROM vw_warehouse_data_flow WHERE country = '{$country}' GROUP by country,product_name");
	    return $query->result();
	}

	public function warehouse_product($name)
	{
	    //Get Product Volumes
	    $query = $this->db->query("SELECT whouse_id,warehouse_name,name,stock,country,volume,direction,time,date
        		 FROM vw_warehouse_data_flow WHERE product_name = '{$name}' ORDER by date desc, time desc  LIMIT 100");
        if ($query->num_rows() > 0)
        {
	        $tmpl = array ( 'table_open'  => '<table class="datatables table table-condensed table-bordered table-striped volumes">' );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Warehouse Name','Country','Grade','RTVT Volume(MT)' ,'Inflow/Outflow','Date','Time');
	        $q = $query->result();
	        foreach($q as $t)
	        {
	            if($t->direction == 'IN'){
			        $sign = '<span class="text-up">+'.$t->volume.'</span> ';
			    }else{
			        $sign = '<span class="text-down">-'.$t->volume.'</span> ';
			    }
	            $this->table->add_row(anchor('site/warehouse_info/'.$t->whouse_id,$t->warehouse_name),$t->country,$t->name,$t->stock,$sign,$t->date,$t->time);
	        }
			return $this->table->generate();
        }
        else
        {
            return '';
        }
	}

	public function daily_volumes($id)
	{
		$query = $this->db->query("
			SELECT name, direction, volume, product_name, stock
			FROM (
				SELECT * FROM vw_warehouse_data_flow
    			ORDER BY date DESC, time DESC
    			) AS sub
			WHERE whouse_id = '{$id}'
			GROUP BY product_name, name
			");
		/*
	    $this->db->select('name,direction,volume,product_name,stock');
	    $this->db->where('whouse_id',$id);
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $this->db->group_by('product_name');
	    $this->db->group_by('name');
	    //$this->db->limit(5);
	    $query = $this->db->get('vw_warehouse_data_flow');
	    */
		return $query->result();
	}

	public function checkLastEntry($warehouse,$id)
	{
		$this->db->select('stock');
	    $this->db->where('whouse_id',$warehouse);
	    $this->db->where('grade_id',$id);
	    $this->db->order_by('date','desc');
	    $this->db->order_by('time','desc');
	    $query = $this->db->get('vw_warehouse_data_flow');
		return $query->row();
	}

	public function checkCapacity($warehouse)
	{
		$this->db->select('Capacity, availableVolume');
	    $this->db->where('id',$warehouse);
	    $query = $this->db->get('warehouse');
		return $query->row();
	}



}