<?php
/**
* Export class
*
*/
class Export extends CI_Model  
{
    /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('PHPExcel'));
    }
 
 	/**
 	* export to excel
 	*/
    public function Excel($header,$data,$title,$sheet_title,$xrate=NULL,$avg=NULL)
    {
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()
					->setCreator("RATIN")
					->setLastModifiedBy("RATIN")
					->setTitle($title)
					->setSubject($title);

		// Create headings
		$letter = 'A';
		foreach ($header as $heading) {
			$objPHPExcel->getActiveSheet()->setCellValue($letter.'1',$heading);
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($letter.'1')->getFont()->setBold(true);
			$letter++;
		}

        // Populate actual data
        $number = 2;
        $existing_rate = array();
        foreach($data as $row)
        {
        	$letter = 'A';
        	$count = 0;
        	if($xrate != NULL)
        	{
        		$date = $row['date'];
                // get rate only if date has changed else reuse existing rate
                if(!isset($existing_rate[$date])){
                    $existing_rate[$date] = exchange_rate($date,$avg);
                }
                $rate = $existing_rate[$date];
        	}

        	foreach($row as $cell)
        	{
        		// check if header contains retail
        		if(strpos($header[$count],'Retail') !== false || strpos($header[$count],'Wholesale') !== false){
        			$cell = convert($rate,$cell);
        		}
        		$objPHPExcel->getActiveSheet()->setCellValue($letter.$number, $cell);
            	$letter++;
            	$count++;
        	}
            $number++;
        }

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle($sheet_title);

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		return $objPHPExcel;
	}

	public function PDF($header,$data,$title,$sheet_title,$xrate=NULL,$avg=NULL)
	{
		$rendererName = PHPExcel_Settings::PDF_RENDERER_DOMPDF;
		$rendererLibrary = 'dompdf';
		$rendererLibraryPath = FCPATH.APPPATH.'libraries'.DIRECTORY_SEPARATOR.$rendererLibrary;

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()
					->setCreator("RATIN")
					->setLastModifiedBy("RATIN")
					->setTitle($title)
					->setSubject($title);

		// Create headings
		$letter = 'A';
		foreach ($header as $heading) {
			$objPHPExcel->getActiveSheet()->setCellValue($letter.'1',$heading);
			$objPHPExcel->getActiveSheet()->getColumnDimension($letter)->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($letter.'1')->getFont()->setBold(true);
			$letter++;
		}

        // Populate actual data
        $number = 2; 
        $existing_rate = array();
        foreach($data as $row)
        {
        	$letter = 'A';
        	$count = 0;
        	
        	if($xrate != NULL)
        	{
        		$date = $row['date'];
                // get rate only if date has changed else reuse existing rate
                if(!isset($existing_rate[$date])){
                    $existing_rate[$date] = exchange_rate($date,$avg);
                }
                $rate = $existing_rate[$date];
        	}

        	foreach($row as $cell)
        	{
        		// check if header contains retail
        		if(strpos($header[$count],'Retail') !== false || strpos($header[$count],'Wholesale') !== false){
        			$cell = convert($rate,$cell);
        		}

        		$objPHPExcel->getActiveSheet()->setCellValue($letter.$number, $cell);
            	$letter++;
            	$count++;
        	}
            $number++;
        }

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle($sheet_title);
		$objPHPExcel->getActiveSheet()->setShowGridLines(true);

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		PHPExcel_Settings::setPdfRenderer($rendererName,$rendererLibraryPath);

		return $objPHPExcel;
	}
}