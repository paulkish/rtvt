<?php
/**
* Chart class
*
*/
class Chart extends CI_Model  {
    /**
    * Constructor
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('highcharts'));
        $this->load->helper('conversion');
    }

    public function average_price($price,$product)
    {
        $chart = new Highcharts(Highcharts::HIGHCHART);
        
        //Chart Settings
        $chart->chart = array(
            'renderTo' => 'average-price-chart',
            'type' => 'line',
            'zoomType'=>'x',
            'marginLeft' => 60,
            'marginTop' => 30,
            'marginBottom' => 70,
            'style'=>array(
                'fontSize'=>'11px'
            )
        );

        //Title
        $chart->title = array(
            'text' => 'Average Prices for '.$product,
            'style'=>array(
                'fontSize'=>'14px',
            )
        );

        //Legend
        $chart->legend = array(
            'itemStyle' => array(
                'fontSize'=>'10px'
            )
        );

        $chart->credits = array(
            'enabled'=>false
        );

        //X Axis
        $chart->xAxis = array(
            'labels' => array(
                'style'=>array(
                    'fontSize'=>'11px'
                )
            ),
            'type' => "datetime"
        );

        //Y Axis
        $chart->yAxis = array(
            'labels' => array(
                'style'=>array(
                    'fontSize'=>'11px'
                )
            ),
            'title'=>array(
                'text'=>get_conversion()
            )
        );


        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '<b>'+ this.series.name + ' | $product'+'</b><br/>'+
                Highcharts.dateFormat('%b %e, %Y', this.x) +': '+ this.y +' ".get_conversion()."';
            }"
        );

        // reverse array so that chart data is in asc order
        $price = array_reverse($price);
        $existing_rate = array(); 
        //Market Prices Wholesale
        $wholesale_price = array();
        foreach ($price as $wh) {
            // get rate only if date has changed else reuse existing rate
            if(!isset($existing_rate[$wh['date']])){
                $existing_rate[$wh['date']] = exchange_rate($wh['date']);
            }
            $rate = $existing_rate[$wh['date']];
            $date = date('Y, m-1, d',strtotime($wh['date']));
            $wholesale_price[] = array(new HighchartJsExpr("Date.UTC($date)"),convert($rate,$wh['wholesale']));
        }

        $chart->series[] = array(
            'name' => "Wholesale Prices",
            'color' => "#89A54E",
            'type' => "line",
            'data' => $wholesale_price
        );

        //Market Prices Retail
        $retail_price = array();
        foreach ($price as $rt) {
            if(!isset($existing_rate[$rt['date']])){
                $existing_rate[$rt['date']] = exchange_rate($rt['date']);
            }
            $rate = $existing_rate[$rt['date']];
            $date = date('Y, m-1, d',strtotime($rt['date']));
            $retail_price[] = array(new HighchartJsExpr("Date.UTC($date)"),convert($rate,$rt['retail']));
        }

        $chart->series[] = array(
            'name' => "Retail Prices",
            'color' => "#D62B1F",
            'type' => "line",
            'data' => $retail_price
        );

        return $chart->render("avg_price_chart");
    }

    // capital prices
    public function capital_price($capitals,$product)
    {
        $chart = new Highcharts(Highcharts::HIGHCHART);
        
        //Chart Settings
        $chart->chart = array(
            'renderTo' => 'capital-price-chart',
            'type' => 'spline',
            'zoomType'=>'x',
            'marginLeft' => 60,
            'marginTop' => 30,
            'marginBottom' => 70,
            'style'=>array(
                'fontSize'=>'11px'
            )
        );

        //Title
        $chart->title = array(
            'text' => 'Major East African Markets Prices for '.$product,
            'style'=>array(
                'fontSize'=>'14px',
            )
        );

        //Legend
        $chart->legend = array(
            'itemStyle' => array(
                'fontSize'=>'10px'
            )
        );

        $chart->credits = array(
            'enabled'=>false
        );

        //X Axis
        $chart->xAxis = array(
            'labels' => array(
                'style'=>array(
                    'fontSize'=>'11px'
                )
            ),
            'type' => "datetime"
        );

        //Y Axis
        $chart->yAxis = array(
            'labels' => array(
                'style'=>array(
                    'fontSize'=>'11px'
                )
            ),
            'title'=>array(
                'text'=>get_conversion()
            )
        );


        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '<b>'+ this.series.name + ' | $product'+'</b><br/>'+
                Highcharts.dateFormat('%b %e, %Y', this.x) +': '+ this.y +' ".get_conversion()."';
            }"
        );

        $existing_rate = array();
        foreach ($capitals as $key => $value) {
            // bundle capital prices into multi array
            $price = array_reverse($value); // reverse array so that chart data is in asc order
            $wholesale_price = array(); //Market Prices Wholesale
            foreach ($price as $wh) {
                // get rate only if date has changed else reuse existing rate
                if(!isset($existing_rate[$wh['date']])){
                    $existing_rate[$wh['date']] = exchange_rate($wh['date']);
                }
                $rate = $existing_rate[$wh['date']];
                $date = date('Y, m-1, d',strtotime($wh['date']));
                $wholesale_price[] = array(new HighchartJsExpr("Date.UTC($date)"),convert($rate,$wh['wholesale']));
            }

            $chart->series[] = array(
                'name' => $key,
                'type' => "spline",
                'data' => $wholesale_price
            );
        }

        return $chart->render("capital_price_chart");
    }

    public function sum_volume($volumes,$product)
    {
        $chart = new Highchart();
        //Chart Settings
        $chart->chart = array(
            'renderTo' => 'sum-volume-chart',
            'type' => 'spline',
            'zoomType'=>'x',
            'marginLeft' => 60,
            'marginTop' => 30,
            'marginBottom' => 70,
            'style'=>array(
                'fontSize'=>'11px'
            )
        );

        //Title
        $chart->title = array(
            'text' => 'Total volumes for '.$product.' by border point',
            'style'=>array(
                'fontSize'=>'14px',
            )
        );

        //Legend
        $chart->legend = array(
            'itemStyle' => array(
                'fontSize'=>'10px'
            )
        );

        $chart->credits = array(
            'enabled'=>false
        );

        //X Axis
        $chart->xAxis = array(
            'labels' => array(
                'style'=>array(
                    'fontSize'=>'11px'
                )
            ),
            'type' => "datetime"
        );

        //Y Axis
        $chart->yAxis = array(
            'labels' => array(
                'style'=>array(
                    'fontSize'=>'11px'
                )
            ),
            'title'=>array(
                'text'=>'MT'
            )
        );


        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '<b>'+ this.series.name + ' | $product'+'</b><br/>'+
                Highcharts.dateFormat('%b %e, %Y', this.x) +': '+ this.y +' MT';
            }"
        );

        foreach ($volumes as $key => $vol) {
            $vols = array();
            // reverse array so that chart data is in asc order
            $vol = array_reverse($vol);

            foreach ($vol as $sv) {
                $date = date('Y, m-1, d',strtotime($sv['date']));
                $vols[] = array(new HighchartJsExpr("Date.UTC($date)"),floatval($sv['volume']));
            }

            $chart->series[] = array(
                'name' => $key,
                'type' => "spline",
                'data' => $vols
            );
        }
        
        return $chart->render("sum_volumes_chart");
    }
}
