<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Model {

    public function checkExists($username,$password)
    {
        $this->db->where('username',$username);
        $this->db->where('password',$password);
		$this->db->where('publish','1');
		$this->db->where('web_access','1');
        $this->db->from('users');
        $count = $this->db->count_all_results();
        return ($count == 1) ? true : false;
    }
    
    public function getTradePoint($username){
        $this->db->where('username',$username);
        $query = $this->db->get('users');
        $data = $query->row();
        return $data->market_id;
    }
	
	public function getTradePointType($id)
	{
		$this->db->where('market_id',$id);
        $query = $this->db->get('Trade_points');
        $data = $query->row();
        return $data->TYPE;
	}
    
}
