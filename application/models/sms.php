<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*SMS Model
*/
class Sms extends CI_Model
{
	public function check_exist($phone,$market=NULL,$product=NULL)
	{
		$this->db->where('phone_no',$phone);
		if($market != NULL)
			$this->db->where('market',$market);
		if($product != NULL)
			$this->db->where('product',$product);
		$query = $this->db->get('alert_subscribers');
		$result = $query->num_rows();
		if($result >= 1)
			return 1;
		else
			return 0;
	}

	public function add($info)
	{
		$this->db->insert('alert_subscribers',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function edit($info,$phone)
	{
		$this->db->where('phone_no',$phone);
		$this->db->update('alert_subscribers',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function subscribers()
	{
		$query = $this->db->get('alert_subscribers');
		return $query->result();
	}

	public function message($info)
	{
		$this->db->insert('MessageOut_copy',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
}