<?php
class Codegen_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function get($table,$fields,$perpage=0,$start=0,$array='array',$order=FALSE,$where=NULL,$group=NULL)
    {
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($perpage,$start);

        if($where != NULL)
            $this->db->where($where);

        //Order results
        if($order!=FALSE)
            $this->db->order_by($order, "desc");

        if($group != FALSE)
            $this->db->group_by($group);
            
        $query = $this->db->get();
        
        $result = $array == 'array' ? $query->result_array() : $query->result();
        return $result;
    }

    public function get_all($table,$fields,$array='array',$order=FALSE,$where=NULL,$group=NULL)
    {
        $this->db->select($fields);
        $this->db->from($table);

        if($where != NULL)
            $this->db->where($where);

        //Order results
        if($order!=FALSE)
            $this->db->order_by($order, "desc");

        if($group != FALSE)
            $this->db->group_by($group);
            
        $query = $this->db->get();
        
        $result = $array == 'array' ? $query->result_array() : $query->result();
        return $result;
    }

    public function get_list($table,$fields,$array='array',$order=FALSE)
    {
        $this->db->select($fields);
        $this->db->from($table);

        //Order results
        if($order==TRUE)
            $this->db->order_by("id", "desc");
            
        $query = $this->db->get();
        
        $result = $array == 'array' ? $query->result_array() : $query->result();
        return $result;
    }

    public function record($table,$fields,$where=NULL,$order=FALSE)
    {
        $this->db->select($fields);
        $this->db->from($table);

        if($where != NULL)
            $this->db->where($where);

        //Order results
        if($order != FALSE)
            $this->db->order_by($order, "desc");
            
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function join_tables($table,$fields,$perpage=0,$start=0,$tables,$where=NULL,$order=FALSE)
    {
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->limit($perpage,$start);
        if($where != NULL)
            $this->db->where($where);

        foreach($tables as $key => $value)
        {
            $this->db->join($key, $value);
        }

        //Order results
        if($order!=FALSE)
            $this->db->order_by($order, "desc");
            
        $query = $this->db->get();
        
        $result = $query->result_array();
        return $result;
    }

    public function add($table,$data)
    {
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
			return TRUE;
		
		return FALSE;       
    }
    
    public function edit($table,$data,$fieldID,$ID)
    {
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
			return TRUE;
		
		return FALSE;       
    }
    
    public function delete($table,$fieldID,$ID)
    {
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
			return TRUE;
		
		return FALSE;        
    }   
	
	public function count($table)
    {
		return $this->db->count_all($table);
	}

    public function count_where($table,$where)
    {
        $this->db->from($table);
        if($where != NULL){
            $this->db->where($where,NULL, FALSE);
        }
        return $this->db->count_all_results();
    }

    public function count_join($table,$join)
    {
        $this->db->select('COUNT(id)')
         ->from($table);

        //count based on join
        foreach ($join as $key => $value) 
             $this->db->join($key, $value);

        return $this->db->count_all_results();
    }

    public function unsafe_join_tables($table,$fields,$perpage=0,$start=0,$tables,$order=FALSE)
    {
        $this->db->select($fields,FALSE); //allow building a query which CodeIgniter will not try to protect your field or table names with backticks
        $this->db->from($table);
        $this->db->limit($perpage,$start);

        foreach($tables as $key => $value)
        {
            $this->db->join($key, $value);
        }

        //Order results
        if($order==TRUE)
            $this->db->order_by("id", "desc");
            
        $query = $this->db->get();
        
        $result = $query->result_array();
        return $result;
    }
}