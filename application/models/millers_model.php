<?php
class Millers_model extends CI_Model 
{
    public $db1;

    public function __construct() 
    {
        parent::__construct();
        $this->db1 = $this->load->database('miller', TRUE);
    }

    public function get($table,$fields,$perpage=0,$start=0,$array='array',$order=FALSE,$where=NULL)
    {
        $this->db1->select($fields);
        $this->db1->from($table);
        $this->db1->limit($perpage,$start);

        if($where != NULL)
            $this->db1->where($where,NULL, FALSE);

         //Order results
        if($order==TRUE)
            $this->db1->order_by("id", "desc");
            
        $query = $this->db1->get();
        
        $result = $array == 'array' ? $query->result_array() : $query->result();
        return $result;
    }

    public function get_list($table,$fields,$array='array')
    {
        $this->db1->select($fields);
        $this->db1->from($table);
            
        $query = $this->db1->get();
        
        $result = $array == 'array' ? $query->result_array() : $query->result();
        return $result;
    }

    public function get_list_where($table,$fields,$where,$array='array')
    {
        $this->db1->select($fields);
        $this->db1->from($table);
        $this->db1->where($where);
        //$this->db1->order_by('date','desc');
            
        $query = $this->db1->get();
        
        $result = $array == 'array' ? $query->result_array() : $query->result();
        return $result;
    }

    public function record($table,$fields,$where='')
    {
        $this->db1->select($fields);
        $this->db1->from($table);
        $this->db1->where($where);
            
        $query = $this->db1->get();
        $result = $query->row();
        return $result;
    }

    public function join_tables($table,$fields,$perpage=0,$start=0,$tables,$where=NULL)
    {
        $this->db1->select($fields);
        $this->db1->from($table);
        $this->db1->limit($perpage,$start);
        if($where != NULL)
            $this->db1->where($where);

        foreach($tables as $key => $value)
        {
            $this->db1->join($key, $value);
        }
            
        $query = $this->db1->get();
        
        $result = $query->result_array();
        return $result;
    }

    public function add($table,$data)
    {
        $this->db1->insert($table, $data);         
        if ($this->db1->affected_rows() == '1')
			return TRUE;
		
		return FALSE;       
    }
    
    public function edit($table,$data,$fieldID,$ID)
    {
        $this->db1->where($fieldID,$ID);
        $this->db1->update($table, $data);

        if ($this->db1->affected_rows() >= 0)
			return TRUE;
		
		return FALSE;       
    }
    
    public function delete($table,$fieldID,$ID)
    {
        $this->db1->where($fieldID,$ID);
        $this->db1->delete($table);
        if ($this->db1->affected_rows() == '1')
			return TRUE;
		
		return FALSE;        
    }   
	
	public function count($table)
    {
		return $this->db1->count_all($table);
	}

    public function count_join($table,$join)
    {
        $this->db1->select('COUNT(id)')
         ->from($table);

        //count based on join
        foreach ($join as $key => $value) 
             $this->db1->join($key, $value);

        return $this->db1->count_all_results();
    }

    public function unsafe_join_tables($table,$fields,$perpage=0,$start=0,$tables)
    {
        $this->db1->select($fields,FALSE); //allow building a query which CodeIgniter will not try to protect your field or table names with backticks
        $this->db1->from($table);
        $this->db1->limit($perpage,$start);

        foreach($tables as $key => $value)
        {
            $this->db1->join($key, $value);
        }
            
        $query = $this->db1->get();
        
        $result = $query->result_array();
        return $result;
    }
}