<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscribers extends CI_Model 
{

    public function subscibers_list()
    {
        $this->db->select('name,email,category');
        $this->db->where('subscribe','1');
        $query = $this->db->get('ratin_subscription');
		return $query->result();
    }
    
    public function add($info)
    {
        $this->db->insert('ratin_subscription',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    
    public function edit($email,$info)
    {
        $this->db->where('email', $email);
        $this->db->update('ratin_subscription',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function remove($email){
        $this->db->delete('ratin_subscription', array('email' => $email)); 
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    
    public function checkemail($email){
        $this->db->where('email',$email);
        $this->db->from('ratin_subscription');
        $count = $this->db->count_all_results();
        return ($count == 1) ? true : false;
    }
    
    public function checkemailId($id){
        $this->db->where('id',$id);
        $this->db->from('ratin_subscription');
        $count = $this->db->count_all_results();
        return ($count == 1) ? true : false;
    }
    
    public function findEmail($id){
        $this->db->where('email',$id);
        $query = $this->db->get('ratin_subscription');
        return $query->row();
    }
    
    public function findEmailId($id){
        $this->db->where('id',$id);
        $query = $this->db->get('ratin_subscription');
        return $query->row();
    }
    
    public function updateEmail($id){
        $this->db->update('ratin_subscription', array('subscribe'=>1), array('id' => $id));
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    
}
    
