<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*Markets Model
*/
class Market extends CI_Model
{

	/**
	 * Provides Market information
	 * @param $id - Market Id
	 */
	public function market_info($id)
	{
		$query = $this->db->get_where('market', array('market_id' => $id),1);
		return $query->row();
	}

	public function market_data($product,$market)
	{
		$this->db->where('market_name',$market);
		$this->db->where('product_name',$product);
		$this->db->order_by('date','desc');
		$query = $this->db->get('daily_info_summary');
		return $query->row();
	}

	public function markets($product_id,$date,$limit=5)
	{
		$this->db->where('date',$this->latest_date());
		$this->db->where('product_id',$product_id);
		$this->db->order_by('wholesale_Price','desc');
		$this->db->limit($limit);
		$query = $this->db->get('daily_info_summary');
		return $query->result();
	}

	public function markets_low($product_id,$date,$limit=5)
	{
		$this->db->where('date',$this->latest_date());
		$this->db->where('product_id',$product_id);
		$this->db->order_by('wholesale_Price','asc');
		$this->db->limit($limit);
		$query = $this->db->get('daily_info_summary');
		return $query->result();
	}

	public function latest_date()
	{
		$this->db->where('publish',1);
		$this->db->order_by('date','desc');
		$this->db->limit(1);
		$query = $this->db->get('daily_trading_info');
		return $query->row()->date;
	}

	public function market_grid($country)
	{
		$this->db->where('country',$country);
		$query = $this->db->get('market');
		$result = $query->result();
		if($result)
		{
			//Add CI table
			$tmpl = array('table_open'=>'<table class="table table-condensed table-bordered">' );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Country - '.$country,'Retail Price','WholeSale Price');
	        foreach($result as $row)
			{
				$this->table->add_row(
				$row->market_name,
				'<input type="hidden" name="market[]" id="'.$row->market_id.'-market"  value="'.$row->market_id.'"/>
				<input type="text" name="retail[]"  id="'.$row->market_id.'-retail"/>',
				'<input type="text" name="whole[]" id="'.$row->market_id.'-whole"/>'
				);
			}
			return $this->table->generate();
		}
		else
		{
			return NULL;
		}
	}

	public function rfbs_grid($country)
	{
		$query = $this->db->get('product');
		$result = $query->result();
		if($result)
		{
			//Add CI table
			$tmpl = array('table_open'=>'<table class="table table-condensed table-bordered">' );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Product','Volume');
	        foreach($result as $row)
			{
				$this->table->add_row(
				$row->product_name,
				'<input type="hidden" name="product[]" value="'.$row->product_id.'"/>
				<input type="hidden" name="country[]" value="'.$country.'"/>
				<input type="text" name="volume[]" />'
				);
			}
			return $this->table->generate();
		}
		else
		{
			return NULL;
		}
	}

	/*Insert Market Prices*/
	public function add($info)
    {
    	$market = $info['market_id'];
    	$retail = $info['retail_price'];
    	$wholesale = $info['wholesale_price'];
    	unset($info['retail_price']);
    	unset($info['wholesale_price']);
    	$this->db->set('retail_price', "$retail / convert_to_dollar_in($market)", FALSE); // run custom function
    	$this->db->set('wholesale_price', "$wholesale / convert_to_dollar_in($market)", FALSE);
        $this->db->insert('daily_trading_info',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

	/*Insert RFBS Prices*/
	public function add_volume($info)
    {
        $this->db->insert('rfbs_volume',$info);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

	/*
	* Checks if market data has already been submitted
	*/
	public function check_exist($market,$product,$date)
	{
		$this->db->select('id');
		$this->db->where('market_id',$market);
		$this->db->where('product_id',$product);
		$this->db->where('date',$date);
		$query = $this->db->get('daily_trading_info');
		$result = $query->num_rows();
		if($result >= 1)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/*
	* Checks if rfbs data has already been submitted
	*/
	public function rfbs_check_exist($country,$product,$date)
	{
		$this->db->select('id');
		$this->db->where('country_id',$country);
		$this->db->where('product_id',$product);
		$where = "YEAR(date) = YEAR('$date') AND MONTH(date) = MONTH('$date')";
		$this->db->where($where);
		$query = $this->db->get('rfbs_volume');
		$result = $query->num_rows();
		if($result >= 1)
			return 1;
		else
			return 0;

	}

	public function daily_prices($id=NULL,$product_id=NULL)
	{
		$this->db->select('market_name,product_name,retail_Price as retail,wholesale_Price as wholesale');
		if($id != NULL)
			$this->db->where('market_id',$id); //search by market id
		if($product_id != NULL)
			$this->db->where('product_id',$product_id); //search by product id
		$this->db->where('date',date('Y-m-d'));
		$query = $this->db->get('daily_data');
		return $query->result();
	}

	public function search_prices($id=NULL,$product_id=NULL,$start=NULL,$end=NULL)
	{
		$this->db->select('market_name,product_name,retail_Price as retail,wholesale_Price as wholesale,date');
		if($id != NULL)
			$this->db->where('market_id',$id); //search by market id
		if($product_id != NULL)
			$this->db->where('product_id',$product_id); //search by product id
		if($start != NULL AND $end !=NULL)
		{
			$start = date('Y-m-d',strtotime($start));
			$end = date('Y-m-d',strtotime($end));
			$where = "date BETWEEN '$start' AND '$end'";
			$this->db->where($where); //search by date
		}
		else
			$this->db->where('date',date('Y-m-d'));

		$query = $this->db->get('daily_data');
		return $query->result();
	}

	public function market_list($id=NULL,$country=NULL)
	{
		$this->db->select('market_id as id,market_name,country,POINT_X as x,POINT_Y as y');
		if($id != NULL)
            $this->db->where('market_id',$id); //search by id
        if($country != NULL)
            $this->db->where('country',$country); //search by country
        $query = $this->db->get('market');
		return $query->result();
	}

	public function market_monthly_sum($id,$country,$start,$end)
	{
		$this->db->select("product_name,retail,wholesale,date");
		$this->db->where('product_name',$id);
		$this->db->where('country',$country);
		$where = "date BETWEEN '$start' AND '$end'";
		$this->db->where($where);
		$this->db->order_by('date','asc');
		$query = $this->db->get('vw_monthly_market');
		return $query->result();
	}

	public function rfbs_monthly_sum($id,$country,$start,$end)
	{
		$this->db->select("product_name,volume,date");
		$this->db->where('product_name',$id);
		$this->db->where('Country_name',$country);
		$where = "date BETWEEN '$start' AND '$end'";
		$this->db->where($where);
		$this->db->order_by('date','asc');
		$query = $this->db->get('vw_rfbs');
		return $query->result();
	}
}