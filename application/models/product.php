<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Model {

    public function product_list($id=NULL)
    {
		$this->db->order_by('product_id','asc');
        if($id != NULL)
            $this->db->where('product_id',$id); //search by id

		$this->db->where('publish','1');
        $query = $this->db->get('product');
		return $query->result();
    }

    public function grade_list($product_id)
    {
        $this->db->where('productId',$product_id);
        $query = $this->db->get('grade');
		return $query->result();
    }

    public function product_volumes($product,$country=NULL)
    {
        if(isset($country) AND $country != NULL){
            $query = $this->db->query(
        		"
        		SELECT GROUP_CONCAT(sum_vol ORDER BY date asc) as data,name
        		FROM (
        				SELECT concat( '[Date.UTC(',DATE_FORMAT(DATE_SUB(date, INTERVAL 1 MONTH),'%Y,%c,%e'), '),',SUM(stock),']') as sum_vol,product_name,name,date
        				FROM vw_warehouse_data_flow v
        				WHERE product_name = '{$product}' AND country = '{$country}'
        				GROUP by date,name,time
                        ORDER by id desc
        				LIMIT 30
        		) v
        		GROUP by name
                ORDER by date asc
        		");
        }
        else{
             $query = $this->db->query(
        		"
        		SELECT GROUP_CONCAT(sum_vol ORDER BY date asc) as data,Product as name
        		FROM (
        				SELECT concat( '[Date.UTC(',DATE_FORMAT(DATE_SUB(date, INTERVAL 1 MONTH),'%Y,%c,%e'), '),',SUM(Available_Volume),']') as sum_vol,Product,date
        				FROM whouse_cummulatives v
        				WHERE Product = '{$product}'
                        AND date IS NOT NULL AND Available_Volume IS NOT NULL
                        GROUP by date
                        ORDER by id desc
                        LIMIT 30
        		) v
        		GROUP by Product
                ORDER by date asc
        		");
        }
        return $query->result();
    }

    public function productName($id){
        $this->db->where('product_id',$id);
        $query = $this->db->get('product');
		return $query->row();
    }

    public function gradeName($id){
        $this->db->where('id',$id);
        $query = $this->db->get('grade');
		return $query->row();
    }

    //Get all cummulative volume
    public function cumulativeGradeVolume($country)
    {
        $this->db->where('country',$country);
        $query = $this->db->get('vw_whouse_cummulatives');
        return $query->result();
    }

    public function cumulativeProductVolume($country)
    {
        $this->db->select('*,sum(Available_Volume) as Volume');
        $this->db->where('country',$country);
        $this->db->group_by('product_name');
        $query = $this->db->get('vw_whouse_cummulatives');
        return $query->result();
    }
}