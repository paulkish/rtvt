<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Miller
            <small>prices</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Millers</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Export Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <?= anchor(current_url().'/export/excel','<i class="fa fa-file-excel-o"></i> Export to Excel',array('class'=>'btn btn-default')); ?>
            <?= anchor(current_url().'/export/pdf','<i class="fa fa-file-pdf-o"></i> Export to PDF',array('class'=>'btn btn-default')); ?>
        </div>
    </div>
</div>
<hr>
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12">
        <!-- Add Table Responsiveness -->
        <div class="table-responsive"> 
            <?php
            if(!$results){
                echo '<h3>No Data</h3>';
            } else {
                //$header = array_keys($results[0]);

                for($i=0;$i<count($results);$i++)
                {
                    $id = array_values($results[$i]);
                    foreach ($results[$i] as $format) 
                        $formatted[$i][]= limit_words($format);

                    //array_shift($formatted[$i]); for hiding id                    
                }

                //set table styling
                $tmpl = array ('table_open'=>'<table class="table table-bordered table-condensed">');
                $this->table->set_template($tmpl); 
                    
                $clean_header = clean_header($header);
                //array_shift($clean_header); for hiding id
                $this->table->set_heading($clean_header); 

                //Table
                echo $this->table->generate($formatted);
            }
            ?>
            <div class="pull-left">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <div class="pull-right">
                <form class="form-inline" id="page-size-form" method="get">
                    <div class="form-group">
                        <select id="page-size" name="per_page" class="page-size form-control">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->