<div class="row"> 
    <div class="col-lg-12">
    	<div class="marquee">
    		<?php $existing_rate = array(); ?>
    		<?php foreach ($markets_marquee as $mm): ?>
    			<?php
    				$date = $mm['date'];
    				if(!isset($existing_rate[$date])){
	                    $existing_rate[$date] = exchange_rate($date);
	                }
	                $rate = $existing_rate[$date]; 
    			?>
    			[<?= $mm['market_name']; ?>,<?= $mm['country']; ?> 
    			<?= $mm['product_name']; ?>
    			Retail: <?= convert($rate,$mm['retail_Price']).' '.get_conversion(); ?> 
    			Wholesale: <?= convert($rate,$mm['wholesale_Price']).' '.get_conversion(); ?>
    			Date: <?= $mm['date']; ?>]
    		<?php endforeach; ?>
    	</div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Major East African Markets Prices for the Last 7 days</h3>
		  	</div>
		  	<div class="panel-body">
		  		<div id="capital-price-chart"></div>
		  		<div class="col-md-12">
		            <form class="form-inline" role="form" action="<?= base_url('site/index'); ?>" method="get">
		                <div class="form-group">
		                    <label for="selProduct">Product</label>
		                    <select name="product_market" class="form-control submit-change" id="selProduct">
		                        <option value="">--Please select--</option>
		                        <?php foreach ($products_list as $plist): ?>
		                            <option <?= selected($plist->product_id,$this->input->get('product_market')); ?> value="<?= $plist->product_id; ?>"><?= $plist->product_name; ?></option>
		                        <?php endforeach; ?>
		                    </select>
		                </div>
		            </form>
		        </div>
		  	</div>
		</div>
	</div>
	<div class="col-lg-6">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Total Border Volumes for the Last 7 days</h3>
		  	</div>
		  	<div class="panel-body">
		  		<div id="sum-volume-chart"></div>
		  		<div class="col-md-12">
		            <form class="form-inline" role="form" action="<?= base_url('site/index'); ?>" method="get">
		                <div class="form-group">
		                    <label for="selProduct">Product</label>
		                    <select name="product_border" class="form-control submit-change" id="selProductBorder">
		                        <option value="">--Please select--</option>
		                        <?php foreach ($products_list as $plist): ?>
		                            <option <?= selected($plist->product_id,$this->input->get('product_border')); ?> value="<?= $plist->product_id; ?>"><?= $plist->product_name; ?></option>
		                        <?php endforeach; ?>
		                    </select>
		                </div>
		            </form>
		        </div>
		  	</div>
		</div>
	</div>
</div>
<!-- Content Row -->
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Latest Market Prices <?= get_conversion(); ?></h3>
		  	</div>
		  	<div class="panel-body">
			  	<ul id="market-tab" class="nav nav-tabs" role="tablist">
			  		<?php foreach ($products as $product): ?>
			  			<li role="presentation">
			  				<a href="#<?= $product['product_name']; ?>-market" aria-controls="<?= $product['product_name']; ?>" role="tab" data-toggle="tab"><?= $product['product_name']; ?></a>
			  			</li>
			  		<?php endforeach; ?>
			  	</ul>
			  	<hr>
		  		<div class="tab-content">
				  	<?php foreach ($products as $product): ?>
				  		<div role="tabpanel" class="tab-pane" id="<?= $product['product_name']; ?>-market">
					    	<div class="table-responsive"> 
				            <?php table_summary($markets[$product['product_id']],$markets_header[$product['product_id']]); ?>
				        	</div>
				        </div>
			        <?php endforeach; ?>
		        </div>
		        <?= anchor('site/market','See more');?>
		  	</div>
		</div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Latest Border Volumes</h3>
		  	</div>
		  	<div class="panel-body">
		    	<ul id="border-tab" class="nav nav-tabs" role="tablist">
			  		<?php foreach ($products as $product): ?>
			  			<li role="presentation">
			  				<a href="#<?= $product['product_name']; ?>-border" aria-controls="<?= $product['product_name']; ?>" role="tab" data-toggle="tab"><?= $product['product_name']; ?></a>
			  			</li>
			  		<?php endforeach; ?>
			  	</ul>
			  	<hr>
		  		<div class="tab-content">
				  	<?php foreach ($products as $product): ?>
				  		<div role="tabpanel" class="tab-pane" id="<?= $product['product_name']; ?>-border">
					    	<div class="table-responsive"> 
				            <?php table_summary($borders[$product['product_id']],$borders_header[$product['product_id']]); ?>
				        	</div>
				        </div>
			        <?php endforeach; ?>
		        </div>
		        <?= anchor('site/border','See more');?>
		  	</div>
		</div>
    </div>
</div>
<!-- Content Row -->
<div class="row row-eq-height">
    <div class="col-lg-3">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Newsletter Subscription</h3>
		  	</div>
		  	<div class="panel-body">
		    	<form id="subscribe_form" class="form-horizontal" action="<?php echo base_url('site/subscribe'); ?>" method="POST">
		            <div class="form-group">
			            <div class="col-sm-12">
			            	<div class="info"></div>
			            </div>
		            </div>
		            <div class="form-group">
		                <div class="col-sm-offset-1 col-sm-10">
		                    <input type="text" name="email" id="inputEmail" class="form-control required" placeholder="Email">
		                </div>
		            </div>
		            <div class="form-group">
		                <div class="col-sm-offset-1 col-sm-10">
      						<div class="checkbox">
			                    <label>
			                        <input value="1" name="category[]" type="checkbox" checked/> Market Prices
			                    </label>
		                    </div>
		                    <div class="checkbox">
			                    <label>
			                        <input value="3" name="category[]" type="checkbox" /> Informal Border Trade Volumes
			                    </label>
		                    </div>
		                </div>
		            </div>
		            <div class="form-group">
		                <div class="col-sm-offset-1 col-sm-10">
			                <div class="radio">
			                    <label>
			                    <input type="radio" name="subscribe" id="optionsSubscribe1" value="1" checked>
			                        Subscribe
			                    </label>
			                </div>
			                <div class="radio">
			                    <label>
			                    <input type="radio" name="subscribe" id="optionsSubscribe2" value="0">
			                        Un-subscribe
			                    </label>
			                </div>
		                </div>
		            </div>
		            <div class="form-group">
		                <div class="col-sm-offset-1 col-sm-10">
		                	<input class="btn btn-primary" type="submit" id="submit" value="Subscribe">
		                </div>
		            </div>
		        </form>
		  	</div>
		</div>
    </div>
    <div class="col-lg-3">
    	<div class="panel panel-default">
			<div class="panel-heading">
		    	<h3 class="panel-title">SMS Subscription</h3>
		  	</div>
		  	<div class="panel-body">
		  		<?= $this->load->view('pages/sms'); ?>
		  	</div>
		</div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">News</h3>
		  	</div>
		  	<div class="panel-body">
		    	<?php if(!$news): ?>
		            <h4>No News Articles</h4>
		        <?php else: ?>
		            <?php foreach ($news as $article): ?>
		                <div class="list-group">
						  	<a href="<?= base_url('site/news_article/'.$article['id']); ?>" class="list-group-item">
						    	<h4 class="list-group-item-heading"><?= $article['title'];?></h4>
						    	<p class="list-group-item-text"><?= $article['description'] ;?></p>
						  	</a>
						</div>
		            <?php endforeach; ?>
		        <?php endif; ?>
		  	</div>
		</div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Bulletins</h3>
		  	</div>
		  	<div class="panel-body">
		    	<?php if(!$bulletins): ?>
		            <h4>No existing reports</h4>
		        <?php else: ?>
		            <?php foreach ($bulletins as $bulletin): ?>
		                <div class="list-group">
						  	<a href="<?= base_url('assets/uploads/files/'.$bulletin['file']); ?>" class="list-group-item">
						    	<h4 class="list-group-item-heading"><?= $bulletin['title'];?></h4>
						    	<p class="list-group-item-text"><?= $bulletin['description'];?></p>
						  	</a>
						</div>
		            <?php endforeach; ?>
		        <?php endif; ?>
		  	</div>
		</div>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
        <div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Partners</h3>
		  	</div>
		  	<div class="panel-body">
		  		<div class="als-container" id="my-als-list">
				  	<span class="als-prev"><i class="fa fa-2x fa-chevron-left"></i></span>
				  	<div class="als-viewport">
				    	<div class="als-wrapper">
				      		<div class="als-item"><?= img(array('src'=>base_url('resources/img/sida.png'),'alt'=>'SIDA','class'=>'img-responsive')); ?></div>
				      		<div class="als-item"><?= img(array('src'=>base_url('resources/img/usaid.png'),'alt'=>'USAID','class'=>'img-responsive')); ?></div>
				      		<div class="als-item"><?= img(array('src'=>base_url('resources/img/ukaid.png'),'alt'=>'UKAID','class'=>'img-responsive')); ?></div>
				    		<div class="als-item"><?= img(array('src'=>base_url('resources/img/agra.png'),'alt'=>'AGRA','class'=>'img-responsive')); ?></div>
				    		<div class="als-item"><?= img(array('src'=>base_url('resources/img/asareca.png'),'alt'=>'ASARECA','class'=>'img-responsive')); ?></div>
				    		<div class="als-item"><?= img(array('src'=>base_url('resources/img/cta.png'),'alt'=>'CTA','class'=>'img-responsive')); ?></div>
				    		<div class="als-item"><?= img(array('src'=>base_url('resources/img/baf.png'),'alt'=>'Business Advocacy Fund','class'=>'img-responsive')); ?></div>
				    		<div class="als-item"><?= img(array('src'=>base_url('resources/img/foodtrade.png'),'alt'=>'FoodTrade','class'=>'img-responsive')); ?></div>
				    	</div> <!-- als-wrapper end -->
				  	</div> <!-- als-viewport end -->
				  	<span class="als-next"><i class="fa fa-2x fa-chevron-right"></i></span>
				</div> <!-- als-container end -->
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#market-tab a:first').tab('show');
        $('#border-tab a:first').tab('show');

        <?= $market_chart; ?>
        <?= $border_chart; ?>

        $('#subscribe_form').validate({
            rules: {
            category: "required",
			email: {
				required: true,
				email: true
			},
            'category[]': {
                required: true
            }

		},
        messages:{
            category: 'A category is required',
			email: 'A valid email address is required',
            'category[]': {
                required: "You must pick at least one category",
            }
        }
        });
        var options = {
            success: function(info){
                $('.info').html(info);
                $('.info').addClass('well');
            }
        };
        $('#subscribe_form').ajaxForm(options);

        $("#my-als-list").als({
			visible_items: 4,
			autoscroll: "yes",
		});

		// marquee
		$('.marquee').marquee({
			duration:20000,
			pauseOnHover:true,
			allowCss3Support:true
		});
    });  
</script>