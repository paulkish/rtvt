<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo lang('reset_password_heading');?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Reset Password</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
		<?php if($message != NULL): ?>
        <div id="infoMessage" class="alert alert-info"><?php echo $message;?></div>
        <?php endif; ?>

		<?php echo form_open('site/reset/' . $code,array('class'=>'form-horizontal','role'=> 'form'));?>

		<div class="form-group">
			<label for="new_password" class="col-sm-2 control-label"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
			<div class="col-sm-10">
				<?php echo form_input($new_password);?>
			</div>
		</div>

		<div class="form-group">
			<label for="new_password_confirm" class="col-sm-2 control-label"><?php echo lang('reset_password_new_password_confirm_label');?></label>
			<div class="col-sm-10">
				<?php echo form_input($new_password_confirm);?>
			</div>
		</div>

		<?php echo form_input($user_id);?>
		<?php echo form_hidden($csrf); ?>

		<div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <?php $attrib = array('type'=>'submit','class'=>'btn btn-default','value'=>lang('reset_password_submit_btn')); ?>
				<?php echo form_submit($attrib);?>
			</div>
		</div>

		<?php echo form_close();?>
	</div>
</div>