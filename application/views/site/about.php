<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $article->title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active"><?= $article->title; ?></li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-8">
        <div class="text-justify">
            <?= $article->content;?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="well">
            <h4>Site pages</h4>
            <ul class="nav nav-pills nav-stacked">
            <?php foreach(about_articles() as $pages): ?>
            <li><a href="<?= base_url('site/about/'.$pages->id); ?>"><?= $pages->title; ?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>