<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Supermarket
            <small>prices</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Supermarket</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Export Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <?= anchor(base_url('site/supermarket_search?').$_SERVER['QUERY_STRING'].'&export=excel','<i class="fa fa-file-excel-o"></i> Export to Excel',array('class'=>'btn btn-default')); ?>
            <?= anchor(base_url('site/supermarket_search?').$_SERVER['QUERY_STRING'].'&export=pdf','<i class="fa fa-file-pdf-o"></i> Export to PDF',array('class'=>'btn btn-default')); ?>
        </div>
    </div>
</div>
<hr>
<!-- Content Row -->
<div class="row">
    <div class="col-lg-3">
        <div class="well">
            <h4>Filter Prices</h4>
            <hr>
            <form role="form" action="supermarket_search" method="get">
                <div class="form-group">
                    <label for="inputStart">Start Date</label>
                    <input type="text" name="start" class="form-control form-date" id="inputStart" value="<?= $this->input->get('start');?>">
                </div>
                <div class="form-group">
                    <label for="inputEnd">End Date</label>
                    <input type="text" name="end" class="form-control form-date" id="inputEnd" value="<?= $this->input->get('end');?>">
                </div>
                <div class="form-group">
                    <input type="submit" value="Filter Prices" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-lg-9">
        <!-- Add Table Responsiveness -->
        <div class="table-responsive"> 
            <?php
            if(!$results){
                echo '<h3>No Data</h3>';
            } else {
                //$header = array_keys($results[0]);

                for($i=0;$i<count($results);$i++)
                {
                    $id = array_values($results[$i]);
                    foreach ($results[$i] as $format) 
                        $formatted[$i][]= limit_words($format);

                    //array_shift($formatted[$i]); for hiding id                    
                }

                //set table styling
                $tmpl = array ('table_open'=>'<table class="table table-bordered table-condensed">');
                $this->table->set_template($tmpl); 
                    
                $clean_header = clean_header($header);
                //array_shift($clean_header); for hiding id
                $this->table->set_heading($clean_header); 

                //Table
                echo $this->table->generate($formatted);
            }
            ?>
        </div>
    </div>
</div>
<!-- /.row -->
<hr>