<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo lang('login_heading');?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Login</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12">
        <p><?php echo lang('login_subheading');?></p>
        
        <?php if($message != NULL): ?>
        <div id="infoMessage" class="alert alert-info"><?php echo $message;?></div>
        <?php endif; ?>

        <?php echo form_open('site/login',array('class'=>'form-horizontal','role'=>'form'));?>

        <div class="form-group">
            <label for="indentity" class="col-sm-2 control-label"><?php echo lang('login_identity_label');?></label>
            <div class="col-sm-10">
                <?php echo form_input($identity);?>
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-sm-2 control-label"><?php echo lang('login_password_label');?></label>
            <div class="col-sm-10">
                <?php echo form_input($password);?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label>
                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                        <?php echo lang('login_remember_label');?>
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <?php echo form_submit('submit', lang('login_submit_btn'),'class = "btn btn-primary"');?>
                <a href="forgot" class="btn btn-default"><?php echo lang('login_forgot_password');?></a></p>
            </div>
        </div>

        <?php echo form_close();?>
    </div>
</div>
<!-- /.row -->
<hr>