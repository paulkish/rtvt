<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Certified Warehouses
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Certified Warehouses</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12">
        <iframe src="https://www.google.com/maps/d/embed?mid=zINa3dgb9ePo.kRcdW8Wl2Nig" height="600px" width="100%" style="border:none;"></iframe>
    </div>
</div>
<!-- /.row -->