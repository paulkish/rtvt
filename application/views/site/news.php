<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">News
            <small><?= isset($category) ? $category->category:'latest news';?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">News</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-8">
        <?php if(!$results): ?>
            <h3>No Data</h3>
        <?php else: ?>
            <?php foreach ($results as $result): ?>
                <!-- First Blog Post -->
                <h2><a href="<?= base_url('site/news_article/'.$result['id']); ?>"><?= $result['title'];?></a></h2>
                <p><i class="fa fa-clock-o"></i> Posted on <?= date('F, j, Y',strtotime($result['created_on']));?> at <?= date('h:i a',strtotime($result['created_on']));?></p>
                <hr>
                <p><?= $result['description'] ;?></p>
                <a class="btn btn-primary" href="<?= base_url('site/news_article/'.$result['id']); ?>">Read More <i class="fa fa-angle-right"></i></a>
                <hr>
            <?php endforeach; ?>
            <div class="pull-left">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <div class="well">
            <h4>News Categories</h4>
            <ul class="nav nav-pills nav-stacked">
            <?php foreach ($categories as $category): ?>
                <li><a href="<?= base_url('site/category/'.$category['id']); ?>"><?= $category['category'];?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<!-- /.row -->