<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo lang('create_user_heading');?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Sign Up</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p><?php echo lang('create_user_subheading');?></p>

        <?php if($message != NULL): ?>
        <div id="infoMessage" class="alert alert-info"><?php echo $message;?></div>
        <?php endif; ?>

        <?php echo form_open("site/signup",array('class'=>'form-horizontal','role'=> 'form'));?>

        <div class="form-group">
            <label for="first_name" class="col-sm-2 control-label"><?php echo lang('create_user_fname_label');?></label>
            <div class="col-sm-10">
                <?php echo form_input($first_name);?>
            </div>
        </div>
        <div class="form-group">
            <label for="last_name" class="col-sm-2 control-label"><?php echo lang('create_user_lname_label');?></label>
            <div class="col-sm-10">    
                <?php echo form_input($last_name);?>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label"><?php echo lang('create_user_email_label');?></label>
            <div class="col-sm-10">  
                <?php echo form_input($email);?>
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-sm-2 control-label"><?php echo lang('create_user_phone_label');?></label>
            <div class="col-sm-10">
                <?php echo form_input($phone);?>
            </div>
        </div>
        <div class="form-group">
            <label for="category" class="col-sm-2 control-label"><?php echo lang('create_user_category_label');?></label>
            <div class="col-sm-10">
                <?php echo form_dropdown(
                    $category['name'],
                    array('Farmer'=>'Farmer/Producer','Processor'=>'Processor/Trader','Researcher/Student'=>'Researcher/Student','NGO'=>'NGO','Policy Maker/Government'=>'Policy Maker/Government','Other'=>'Other'),
                    $category['value'],
                    'class="form-control"'
                    );?>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label"><?php echo lang('create_user_password_label');?></label>
            <div class="col-sm-10">
                <?php echo form_input($password);?>
            </div>
        </div>
        <div class="form-group">
            <label for="password_confirm" class="col-sm-2 control-label"><?php echo lang('create_user_password_confirm_label');?></label>
            <div class="col-sm-10">
                <?php echo form_input($password_confirm);?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <?php $attrib = array('type'=>'submit','class'=>'btn btn-default','value'=>lang('create_user_submit_btn')); ?>
                <?php echo form_submit($attrib);?>
            </div>
        </div>
        <?php echo form_close();?>
    </div>
</div>