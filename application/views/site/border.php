<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Crossborder
            <small>volumes</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Crossborder</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Export Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <form role="form" action="border" method="get" class="form-inline">
                <div class="form-group">
                    <label for="selProduct">Product</label>
                    <select name="product" class="form-control submit-change" id="selProduct">
                        <option value="">--Please select--</option>
                        <?php foreach ($products as $product): ?>
                            <option <?= selected($product->product_id,$this->input->get('product')); ?> value="<?= $product->product_id; ?>"><?= $product->product_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </form>
        </div>
        <div class="pull-right">
            <?php if($this->ion_auth->logged_in()): ?>
            <?= anchor(current_url().'/export/excel','<i class="fa fa-file-excel-o"></i> Export to Excel',array('class'=>'btn btn-default')); ?>
            <?= anchor(current_url().'/export/pdf','<i class="fa fa-file-pdf-o"></i> Export to PDF',array('class'=>'btn btn-default')); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<hr>
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Tabular Data</a></li>
                    <li role="presentation"><a href="#chart" aria-controls="chart" role="tab" data-toggle="tab">Chart</a></li>
                </ul>
                <div class="tab-content tab-content-data">
                    <div role="tabpanel" class="tab-pane active" id="table">
                        <!-- Add Table Responsiveness -->
                        <div class="table-responsive"> 
                            <?php
                            if(!$results){
                                echo '<h4>No Data</h4>';
                            } else {
                                for($i=0;$i<count($results);$i++)
                                {
                                    $id = array_values($results[$i]);
                                    foreach ($results[$i] as $format) 
                                        $formatted[$i][]= limit_words($format);                  
                                }

                                //set table styling
                                $tmpl = array ('table_open'=>'<table class="table table-bordered table-condensed">');
                                $this->table->set_template($tmpl); 
                                    
                                $clean_header = clean_header($header);
                                $this->table->set_heading($clean_header); 

                                //Table
                                echo $this->table->generate($formatted);
                            }
                            ?>
                            <div class="pull-left">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <div class="pull-right">
                                <form class="form-inline" id="page-size-form" method="get">
                                    <div class="form-group">
                                        <select id="page-size" name="per_page" class="page-size form-control">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="75">75</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="chart">
                        <div id="sum-volume-chart"></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<script type="text/javascript">
$(document).ready(function() {
    <?= $chart; ?>
});
</script>