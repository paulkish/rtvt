<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Bulletins &amp; Reports</h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Bulletins &amp; Reports</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-8">
        <?php if(!$results): ?>
            <h4>No Bulletins or Reports</h4>
        <?php else: ?>
            <?php foreach ($results as $result): ?>
                <h2><?= $result['title'];?></h2>
                <p class="lead">by <?= $result['username'];?></p>
                <p><i class="fa fa-clock-o"></i> Posted on <?= date('F, j, Y',strtotime($result['created_on']));?> at <?= date('h:i a',strtotime($result['created_on']));?></p>
                <hr>
                <p><?= $result['description'] ;?></p>
                <p><?= anchor('assets/uploads/files/'.$result['file'],'<i class="fa fa-download"></i> Download '.$result['file'],array('class'=>'btn btn-primary','target'=>'_blank'));?></p>
                <hr>
            <?php endforeach; ?>
            <div class="pull-left">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- /.row -->