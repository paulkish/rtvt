<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Grain Storage Facilities Within EAC
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Certified Warehouses</li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12">
        <iframe src="https://www.google.com/maps/d/embed?mid=1b2JiGUDO7dWwtW_8j5e2Hhaxv9E" height="600px" width="100%" style="border:none;"></iframe>
    </div>
</div>
<!-- /.row -->