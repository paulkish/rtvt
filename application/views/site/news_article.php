<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $article->title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li><a href="<?= base_url('site/news'); ?>">News</a></li>
            <li class="active"><?= $article->title; ?></li>
        </ol>
    </div>
</div>
<!-- /.row -->
<!-- Content Row -->
<div class="row">
    <div class="col-lg-8">
        <!-- Date/Time -->
        <p><i class="fa fa-clock-o"></i> Posted on <?= date('F, j, Y',strtotime($article->created_on));?> at <?= date('h:i a',strtotime($article->created_on));?>
        <hr>
        <div class="text-justify">
            <?= $article->content;?>
        </div>
        <?php if(isset($article->file)): ?>
            <hr>
            <?= anchor('assets/uploads/files/'.$article->file,'<i class="fa fa-download"></i> Download '.$article->file,array('class'=>'btn btn-primary','target'=>'_blank'));?>
        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <div class="well">
            <h4>News Categories</h4>
            <ul class="nav nav-pills nav-stacked">
            <?php foreach ($categories as $category): ?>
                <li><a href="<?= base_url('site/category/'.$category['id']); ?>"><?= $category['category'];?></a></li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<!-- /.row -->