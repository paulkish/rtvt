<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo lang('forgot_password_heading');?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li class="active">Forgot Password</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
		<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

		<?php if($message != NULL): ?>
		<div id="infoMessage" class="alert alert-info"><?php echo $message;?></div>
		<?php endif; ?>

		<?php echo form_open("site/forgot",array('class'=>'form-horizontal','role'=> 'form'));?>

		<div class="form-group">
		    <label for="email" class="col-sm-2 control-label">Email:</label>
		    <div class="col-sm-10">
		    	<?php echo form_input($email);?>
			</div>
		</div>

		<div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		    	<?php $attrib = array('type'=>'submit','class'=>'btn btn-default','value'=>lang('forgot_password_submit_btn')); ?>
				<?php echo form_submit($attrib);?>
			</div>
		</div>

		<?php echo form_close();?>
	</div>
</div>