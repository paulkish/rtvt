<div class="row-fluid">
    <div class="span3"></div>
	<div class="span9">
        <h3>LOGIN</h3>
        <hr>
        <div class="info"></div>
        <form id="subscribe_form" class="form-horizontal" action="<?php echo base_url('site/checklogin'); ?>" method="POST">
            <div class="control-group">
                <label class="control-label" for="inputName">Username</label>
                <div class="controls">
                    <input type="text" name="username" id="inputName" class="required" placeholder="Username">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">Password</label>
                <div class="controls">
                    <input type="password" name="password" id="inputEmail" class="required" placeholder="Password">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <input type="submit" id="submit" value="Login" class="btn"/>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $( '#subscribe_form' ).validate({
            rules: {
			username: "required",
			password: "required",
		},
        messages:{
            username: 'A username is required',
			password: 'A valid password is required'
        }
        });
        var options = {
            success: function(info){
                $('.info').html(info);
                $('.info').addClass('well');
            }
        }
        //$('#subscribe_form').ajaxForm(options);
    });
</script>


