<div class="row-fluid">
	<div class="span12">
	<h3>RTVT Facilities List</h3>
		<?php
		     $tmpl = array ('table_open'=>'<table class="datatables table table-condensed table-striped table-hover">');
		     //Clear existing table instance
		     $this->table->clear();
			 $this->table->set_template($tmpl);
			 $this->table->set_heading(array('Warehouse Name','Country','Storage Capacity', 'Warehouse Code'));
			 foreach($data as $w)
		     {    
		        $this->table->add_row(anchor('site/warehouse_info/'.$w->id,$w->warehouse_name),$w->country,$w->Capacity,$w->whouse_code);
		     }
			 echo $this->table->generate();
		?>
	</div>
</div>