<div class="row-fluid">
	<div clas="span12" style="height:290px;">
		<small>
		<div class="tab-content">
			<ul class="nav nav-tabs" id="product-volume">
			    <?php foreach ($country as $c){?>
					<li><a href="<?php echo '#country'.$c->Country_id; ?>" data-toggle="tab"><?php echo $c->Country_name; ?></a></li>
				<?php }?>
			</ul>
			<?php 
			foreach ($country as $c){?>
				<div class="tab-pane" id="<?php echo 'country'.$c->Country_id; ?>">
					<?php
						$cumulative_volumes = $this->product->cumulativeProductVolume($c->Country_name); 
					   	$tmpl = array('table_open'=> '<table class="plain table table-condensed table-bordered table-striped">');
	        			$this->table->set_template($tmpl); 
	        			$this->table->set_heading('Product Name','Available Volume (MT)');
	        			foreach($cumulative_volumes as $volume)
	        			{
	        				$this->table->add_row($volume->product_name,$volume->Available_Volume);
	        			}
	        			echo $this->table->generate();
					?> 
				</div>
			<?php } ?>
		</div>
		<?php echo anchor('http://ratin.net/index.php/warehouses/real-time-volume-tracker','View More'); ?>
		</small>
	</div>
</div>