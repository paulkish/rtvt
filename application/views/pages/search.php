<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<h4>Filter RTVT Storage Facilities</h4>
			<?php echo form_open('site/search_results', array('id' => 'warehouse_search'));?>
			<label>Product</label>
			<select name="product[]" class="chzn-select" multiple="multiple">
			<?php
			    foreach ($product as $p)
			    {
			        echo '<option value="'.$p->product_name.'">'.$p->product_name.'</option>';
			    }
			?>
			</select>
			<label>Country</label>
			<select name="country[]" class="chzn-select" multiple="multiple">
			<?php
			    foreach ($country as $c)
			    {
			        echo '<option value="'.$c->Country_name.'">'.$c->Country_name.'</option>';
			    }
			?>
			</select>
      <!--Commented Out Ownership 
			<label>Ownership</label>
			<select name="category" class="chzn-select">
			<?php
			    $cat = array('Private'=>'Private','Government'=>'Government','NGO'=>'NGO'); 
			    foreach ($cat as $ct)
			    {
			        echo '<option value="'.$ct.'">'.$ct.'</option>';
			    }
			?>
			</select>
      -->
			<label>Real Time Volume (MT)</label>
			<?php $capacity = array(
                  '0 AND 499'=>'0-499 MT',
                  '500 AND 4999'=>'500-4990 MT',
                  '5000 AND 100000'=>'5000+ MT',      
                  );
			echo form_dropdown('capacity',$capacity,'default', 'class="chzn-select"');?>
            <label>Start Date</label>
            <input type="text" id="start" name="start" class="picker"/>
            <label>End Date</label>
            <input type="text" id="end" name="end" class="picker"/>
            
			<hr>
			<input type="submit" id="search" name="search" value="Search" class="btn btn-primary"/>
            <?php echo form_close(); ?>
		</div>
	</div>
	<div class="span9">
		<h3>Search Results</h3>
		<div id="search_results" class="search_results"></div>
	</div>
	<script>
     $(function() {
        //Date picker
        $('#start').Zebra_DatePicker({
           //direction: true,
           show_icon: false,
           pair: $('#end')
        });

        $('#end').Zebra_DatePicker({
           direction: 1,
           show_icon: false
        });
        
        //Ajax action
        $("#search").click(function() {
        	$('#search_results').empty();
  		    $('#search_results').html('<center><?php echo img('resources/img/ajax-loader.gif')?></center>');  
        	$.ajax({  
        	  type: "POST",  
        	  url: "<?php echo base_url('site/search_results'); ?>",  
        	  data: $("#warehouse_search").serialize(),  
        	  success: function(data) {  
        	     $('#search_results').html(data);  
        	     $('.plain').dataTable( {
        	     	"sDom": "<'row-fluid'<'span2'T><'span10'f>r>t<'row-fluid'<'span3'i><'span9'p>>",
        	 		"oTableTools": {
        	     		"sSwfPath": "../resources/js/copy_csv_xls_pdf.swf",
        	     		"aButtons": [
        	     						"print",
        	     						"csv",
        	     						"xls", 
        	     						"pdf" 
        	     					]
        	 		},
        	 		"sPaginationType": "bootstrap",
        	 		"iDisplayLength" : 10
        	     } );
        	  }  
        	});  
        	return false; 
        });  
      });  
	</script>
</div>
