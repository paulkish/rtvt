<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<h4>Warehouse Information</h4>
			<hr class="small">
			<?php 
			    $tmpl = array ('table_open'=>'<table class="table-condensed">');
				$this->table->set_template($tmpl);
				$this->table->add_row('Name',$info->warehouse_name); 
				$this->table->add_row('Country',$info->country);
			    $this->table->add_row('Location',$info->Location);
			    $this->table->add_row('Capacity',$info->Capacity);
				//$this->table->add_row('Volume',$info->availableVolume);
				$this->table->add_row('Ownership',$info->Ownership);
				$this->table->add_row('Contact Details');
				$this->table->add_row('Contact Person',$info->contactPerson);
				$this->table->add_row('Email','<p style="width:13em; word-wrap: break-word;">'.$info->email.'</p>');
				$this->table->add_row('Tel',$info->phone);
				echo $this->table->generate();
			?>
			<hr class="small">
			<?php echo img(array('src'=>'assets/uploads/'.$info->photo,'class'=>'img-polaroid','alt'=>'Image Not Available'));?>
		</div>
	</div>
	<div class="span9">
		<h3><?php echo $info->warehouse_name; ?> | Warehouse Volumes</h3>
		<div id="chart_ajax"></div>
		<hr>
		<?php echo $table_vols; ?>
	</div>
	<script>
	    $(document).ready(function() {
		    var base = '<?php echo base_url('rtvt/site/warehouse_chart');?>';
		    var warehouse = '<?php echo $info->id; ?>';
			//Onload
			$('#chart_ajax').html('<center><?php echo img('resources/img/ajax-loader.gif')?></center>');
	        $('#chart_ajax').load(base+'/'+warehouse); 
	    });
	</script>
</div>
