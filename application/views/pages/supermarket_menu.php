<ul class="nav nav-list">
	<li class="nav-header">supermarket</li>
	<li <?php if($this->uri->segment(3) == 'index') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/index'); ?>">Supermarket Grid</a></li>
	<li <?php if($this->uri->segment(3) == 'prices') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/prices'); ?>">Supermarket Prices</a></li>
	<li <?php if($this->uri->segment(3) == 'towns') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/towns'); ?>">Towns</a></li>
	<li <?php if($this->uri->segment(3) == 'products') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/products'); ?>">Products</a></li>
	<li <?php if($this->uri->segment(3) == 'brands') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/brands'); ?>">Brands</a></li>
	<li <?php if($this->uri->segment(3) == 'weight') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/weight'); ?>">Weight</a></li>	
	<li <?php if($this->uri->segment(3) == 'supermarkets') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket/supermarkets'); ?>">Supermarkets</a></li>	
	<li><a href="<?php echo base_url('admin/manage/markets'); ?>">Back to Main Menu</a></li>	
</ul>