<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<?php $this->load->view('pages/miller_price_menu',array('products'=>$products)); ?>
		</div>
	</div>
	<div class="span9">
		<h2>Miller Prices</h2>
		<hr>
		<!--build table -->
		<?php 
		   	$tmpl = array('table_open'=> '<table class="datatables table table-condensed table-bordered table-striped">');
			$this->table->set_template($tmpl); 
			$this->table->set_heading('Town','Miller','Weight(Kgs)','Price','Date');
			foreach($price as $mp)
			{
				$this->table->add_row($mp->town_name,$mp->miller_name,$mp->weight,$mp->price,$mp->date);
			}
			echo $this->table->generate();
		?>
	</div>
</div>