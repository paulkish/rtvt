<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<h4>PRODUCT LIST FLOWS</h4>
			<ol>
			<?php
			    foreach($product as $p)
			    {    
			        echo '<li>'.anchor('site/product_info/'.urlencode($p->product_name),$p->product_name).'</li>';
			    }
			?>
			</ol>
		</div>
	</div>
	<div class="span9">
		<h3>CUMULATIVE PRODUCT VOLUMES</h3>
		<ul class="nav nav-tabs" id="product-volume">
		    <?php foreach ($country as $c){?>
				<li><a href="<?php echo '#country'.$c->Country_id; ?>" data-toggle="tab"><?php echo $c->Country_name; ?></a></li>
			<?php }?>
		</ul>
		<div class="tab-content">
			<?php 
			foreach ($country as $c){?>
				<div class="tab-pane" id="<?php echo 'country'.$c->Country_id; ?>">
					<?php
						$cumulative_volumes = $this->product->cumulativeGradeVolume($c->Country_name); 
					   	$tmpl = array('table_open'=> '<table class="datatables table table-condensed table-bordered table-striped">');
	        			$this->table->set_template($tmpl); 
	        			$this->table->set_heading('Product Name','Grade','Available Volume (MT)');
	        			foreach($cumulative_volumes as $volume)
	        			{
	        				$this->table->add_row($volume->product_name,$volume->name,$volume->Available_Volume);
	        			}
	        			echo $this->table->generate();
					?> 
				</div>
			<?php } ?>
		</div>
	</div>
</div>