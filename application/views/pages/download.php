<div class="container-fluid">
	<div class="row-fluid">
		<h3>Grain Standards</h3>
		<table class="table">
			<?php
				if ($handle = opendir('resources/docs')) {
			    /* This is the correct way to loop over the directory. */
			    while (false !== ($entry = readdir($handle))) {
			    	if ($entry != "." && $entry != "..") {
			    		echo '<tr><td><a href="'.base_url('rtvt/download/pdf/'.$entry).'">'.$entry.'</a></td><td><i class="icon-download-alt"></i></td></tr>';
			    	}
			        //echo $entry;
			    }
			    closedir($handle);
			}
			?>
		</table>
	</div>
</div>
