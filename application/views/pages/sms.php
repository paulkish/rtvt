<div class="info-sms"></div>
<form id="sms_subscribe_form" role="form" class="form-horizontal" action="<?php echo base_url('market/subscription/register'); ?>" method="POST">
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10">
			<input type="text" name="name" id="inputName" class="form-control" placeholder="Name">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10">
			<input type="text" name="phone" id="inputPhone" class="form-control" placeholder="Include country code e.g +254">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10">
			<select name="category" class="form-control">
				<option value="">Select Category</option>
				<option value="0">Student/Researcher</option>
				<option value="1">Trader</option>
				<option value="2">Farmer</option>
				<option value="3">Policy maker/Government</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10">
			<select name="status" class="form-control">
				<option value="">Receive SMS Broadcasts?</option>
				<option value="1">Yes</option>
				<option value="0">No</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-10">
			<input type="submit" id="submit" value="Subscribe" class="btn btn-primary"/>
		</div>
	</div>
</form>
<script>
    $(document).ready(function() {
        $('#sms_subscribe_form').validate({
            rules: {
            name: "required",
            phone: "required",
            category: "required",
            status: "required"
        },
        messages:{
            name: 'A name is required',
            phone: 'A phone number is required',
            category: 'Category selection is required',
            status: 'Status selection is required'
        }
        });
        var options = {
            success: function(info){
                $('.info-sms').html(info);
                $('.info-sms').addClass('well');
            }
        };
        $('#sms_subscribe_form').ajaxForm(options);
    });
</script>

