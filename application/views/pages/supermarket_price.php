<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<?php $this->load->view('pages/supermarket_price_menu',array('products'=>$products)); ?>
		</div>
	</div>
	<div class="span9">
		<h2>Supermarket Prices</h2>
		<hr>
		<!--build table -->
		<?php 
		   	$tmpl = array('table_open'=> '<table class="datatables table table-condensed table-bordered table-striped">');
			$this->table->set_template($tmpl); 
			$this->table->set_heading('Town','Supermarket','Weight(Kgs)','Price','Date');
			foreach($price as $sp)
			{
				$this->table->add_row($sp->town_name,$sp->supermarket_name,$sp->weight,$sp->price,$sp->date);
			}
			echo $this->table->generate();
		?>
	</div>
</div>