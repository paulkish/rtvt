<div class="row-fluid">
	<div class="span12">
        <center><h4>NEWS LETTER SUBSCRIPTION</h4></center>
        <hr>
        <div class="info"></div>
        <form id="subscribe_form" class="form-horizontal" action="<?php echo base_url('site/subscribe'); ?>" method="POST">
            <div class="control-group">
                <label class="control-label" for="inputName">Name</label>
                <div class="controls">
                    <input type="text" name="name" id="inputName" class="required" placeholder="Name">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">Email</label>
                <div class="controls">
                    <input type="text" name="email" id="inputEmail" class="required" placeholder="Email">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="checkbox">
                        <input value="1" name="category[]" type="checkbox" checked/> Market Prices
                    </label>
                    <label class="checkbox">
                        <input value="2" name="category[]" type="checkbox" /> Warehouse Volumes
                    </label>
                    <label class="checkbox">
                        <input value="3" name="category[]" type="checkbox" /> Informal Border Trade Volumes
                    </label>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="radio">
                    <input type="radio" name="subscribe" id="optionsSubscribe1" value="1" checked>
                        Subscribe to mailing list
                    </label>
                    <label class="radio">
                    <input type="radio" name="subscribe" id="optionsSubscribe2" value="0">
                        Un-subscribe from mailing list
                    </label>
                    <input type="submit" id="submit" value="Submit" class="btn"/>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $( '#subscribe_form' ).validate({
            rules: {
			name: "required",
            category: "required",
			email: {
				required: true,
				email: true
			},
            'category[]': {
                required: true
            }

		},
        messages:{
            name: 'A name is required',
            category: 'A category is required',
			email: 'A valid email address is required',
            'category[]': {
                required: "You must pick at least one category",
            }
        }
        });
        var options = {
            success: function(info){
                $('.info').html(info);
                $('.info').addClass('well');
            }
        };
        $('#subscribe_form').ajaxForm(options);
    });
</script>


