<ul class="nav nav-list">
	<li class="nav-header">miller</li>
	<li <?php if($this->uri->segment(3) == 'index') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/index'); ?>">Miller Grid</a></li>
	<li <?php if($this->uri->segment(3) == 'prices') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/prices'); ?>">Miller Prices</a></li>
	<li <?php if($this->uri->segment(3) == 'towns') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/towns'); ?>">Towns</a></li>
	<li <?php if($this->uri->segment(3) == 'products') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/products'); ?>">Products</a></li>
	<li <?php if($this->uri->segment(3) == 'grades') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/grades'); ?>">Grades</a></li>
	<li <?php if($this->uri->segment(3) == 'weight') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/weight'); ?>">Weight</a></li>	
	<li <?php if($this->uri->segment(3) == 'millers') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller/millers'); ?>">Millers</a></li>	
	<li><a href="<?php echo base_url('admin/manage/markets'); ?>">Back to Main Menu</a></li>	
</ul>