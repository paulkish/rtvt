<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<?php $this->load->view('pages/supermarket_menu'); ?>
		</div>
	</div>
	<div class="span9">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12"><?php echo $this->session->flashdata('info'); ?></div>
			</div>
			<ul class="nav nav-pills">
			<?php foreach($products as $product): ?>
	    		<li <?php echo $product->id == $id ? 'class="active"':''; ?> >
	    			<?php echo anchor('admin/supermarket/index/'.$product->id,$product->product_name); ?>
	    		</li>
	    	<?php endforeach;?>
	    	</ul>
	    	<hr>
	    	<form action="<?php echo current_url(); ?>" method="post">
		    	<?php foreach($towns as $town): ?>
		    		<div class="row-fluid">
		    			<div class="span3">
		    				<label><?php echo $town->town_name; ?></label>
		    			</div>
		    			<div class="span9">
		    				<?php $smarkets = $this->smarket_model->get_list_where('supermarket','id,supermarket_name',array('town_id'=>$town->id),'object');?>
		    				<?php foreach($smarkets as $smarket): ?>
			    				<div class="row-fluid">
			    					<div class="span2">
			    						<?php echo $smarket->supermarket_name; ?>
			    					</div>
					    			<?php foreach($weights as $weight): ?>
					    				<div class="span2">
					    					<input type="text" name="<?php echo $town->id.'_'.$weight->id.'_'.$smarket->id; ?>" class="input-small" placeholder="<?php echo $weight->weight;?> Kg"/>
					    				</div>
					    			<?php endforeach; ?>
					    		</div>
					    	<?php endforeach; ?>
		    			</div>
		    		</div>
		    		<hr>
		    	<?php endforeach;?>
		    	<div class="row-fluid">
		    		<div class="span3">
		    			<label>Brand</label>
			    		<select name="brand">
			    			<?php $brands = $this->smarket_model->get_list_where('brand','id,brand_name',"product_id = $id",'object'); ?>
			    			<option value="">--Please select--</option>
			    			<?php foreach($brands as $brand): ?>
			    				<option value="<?= $brand->id; ?>"><?= $brand->brand_name; ?></option>
			    			<?php endforeach; ?>
			    		</select>
			    	</div>
			    </div>
		    	<div class="row-fluid">
		    		<div class="span3">
			    		<input id="date" name="date" type="text" placeholder="Date" class="datepicker"/>
			    	</div>
			    </div>
			    <div class="row-fluid">
			    	<div class="span3">
		    			<input type="submit" value="Submit Prices" name="submit" class="btn btn-primary"/>
		    		</div>
		    	</div>
	    	</form>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
	 	$('input.datepicker').Zebra_DatePicker();
	});
</script> 