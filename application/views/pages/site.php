<div class="row-fluid">
	<div class="span12">
		<marquee behavior="scroll" scrollamount="1" direction="left">
			<?php 
			foreach($marquee as $m) {
			    if($m->direction == 'IN'){
			        $sign = '<span class="text-up">+'.$m->volume.'</span> ';
			    }else{
			        $sign = '<span class="text-down">-'.$m->volume.'</span> ';
			    }
				echo anchor('rtvt/site/warehouse_info/'.$m->whouse_id,$m->warehouse_name).' '.$m->product_name.' '.$m->name.' RTVT Volume:'.$m->stock.' '.$sign;
			}
			?>
		</marquee>
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div  class="border" id="map"></div>
	</div>
	<div class="span6">
		<h4>PRODUCT VOLUMES</h4>
		<ul class="nav nav-tabs" id="product-volume">
		    <?php foreach ($product as $p){?>
				<li><a href="<?php echo '#product'.$p->product_id; ?>" data-toggle="tab"><?php echo $p->product_name; ?></a></li>
			<?php }?>
		</ul>
		<div class="tab-content">
			<?php 
			foreach ($product as $p){?>
				<div class="tab-pane" id="<?php echo 'product'.$p->product_id; ?>">
					<?php 
						//Find way to add data via ajax to make page render faster
					    $grade = $this->product->grade_list($p->product_id);
					    foreach($grade as $g)
					    {
					    	$product_vols = $this->warehouse->product_volume($g->id);
					        if($product_vols != NULL)
			            	{
			            	    echo "<strong>".strtoupper($p->product_name).'|'.strtoupper($g->name)."</strong>";
				                echo $this->warehouse->product_volume($g->id);
				            }
					    } 
					    echo anchor('rtvt/site/product_info/'.$p->product_name,'See More',array('class'=>'btn btn-inverse'));
					   ?> 
				</div>
			<?php } 
			?>
		</div>
	</div>
		<script>
	    $(document).ready(function() {
		    var base = '<?php echo base_url('rtvt/site/chart'); ?>';
		    var detected = '<?php echo $detected_country; ?>';
		    var country = '<?php echo strtoupper($detected_country); ?>';
			//Onload
			$('#chart_ajax').html('<center><?php echo img('resources/img/ajax-loader.gif')?></center>');
			//Choose Kenya by default
	        $('#chart_ajax').load(base+'/'+detected); 
	        $('a[href*='+country+']').addClass("btn-primary");    
		    
	    	$('a.chart').bind('click', function(e) {
    		  $(".btn-primary").removeClass("btn-primary");
    		  $(this).addClass("btn-primary");           
    		  var url = $(this).attr('href');
    		  $('#chart_ajax').empty();
    		  $('#chart_ajax').html('<center><?php echo img('resources/img/ajax-loader.gif')?></center>');
    		  $('#chart_ajax').load(base+'/'+url); 
    		  e.preventDefault();
    		});
	    });
		</script>
</div>