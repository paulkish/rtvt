<div class="row-fluid">
	<div class="span8">
		<div  class="border" id="map"></div>
	</div>
	<div class="span4">
		<ul class="nav nav-tabs" id="map_tab">
			<li class="active"><a href="#country">Country</a></li>
			<li><a href="#product">Product</a></li>
			<li><a href="#capacity">Capacity</a></li>
			<li><a href="#legend">Map Legend</a></li>
		</ul>
		<?php 
			$attributes = array('id' => 'map-control');
			echo form_open('', $attributes);
			echo '<div class="tab-content">';
			echo '<div class="tab-pane" id="product">';
			foreach($product as $p)
			{
				echo '<label class="control-label">';
				echo form_checkbox(array('id'=>'input'.$p->product_id,'name'=>'product[]','value'=>$p->product_name))
					.' '.$p->product_name;
				echo '</label>';
			}
			echo '</div>';
			echo '<div class="tab-pane active" id="country">';
			foreach ($country as $c)
			{
				echo '<label class="control-label">';
				echo form_checkbox(array('id'=>'input'.$c->Country_id,'name'=>'country[]','value'=> $c->Country_name))
				.' '.$c->Country_name;
				echo '</label>';
			}
			echo '</div>';
			
			echo '<div class="tab-pane" id="capacity">';
                  $capacity = array(
                  '0 AND 499'=>'0-499 MT',
                  '500 AND 4999'=>'500-4999 MT',
                  '5000 AND 100000'=>'5000+ MT',      
                  );
			echo form_dropdown('capacity',$capacity,'default','id="input_capacity" class="chz-select"');
			echo '</div>';
			/*
			echo '<div class="tab-pane" id="ownership">';
			  $categories = array('Government', 'NGO', 'Private', 'Cooperative');
			  foreach($categories as $cat)
			  {
					echo '<label class="control-label">';
					echo form_checkbox(array('id'=>'input'.$cat,'name'=>'categories[]','value'=>$cat))
					.' '.$cat;
					echo '</label>';
			  }
			echo '</div>';
			*/
			echo '<div class="tab-pane" id="legend">';
			$categories = array('Government', 'NGO', 'Private', 'Cooperative');
			foreach($categories as $icon)
			{
				echo $icon.img('resources/icons/'.$icon.'.png').'<br>';
			}
			echo '</div>';
			echo '</div>';
			echo form_close(); 
		?>
	</div>
</div>