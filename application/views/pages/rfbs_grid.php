<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<?php $this->load->view('pages/adminmenu'); ?>
		</div>
	</div>
	<div class="span9">
		<ul class="nav nav-pills">
			<?php 
				foreach($countries as $country)
				{
					if($country->id == $id)
						echo '<li class="active">'.anchor('admin/manage/rfbs_grid/'.$country->id,strtoupper($country->Country_name)).'</li>';
					else
						echo '<li>'.anchor('admin/manage/rfbs_grid/'.$country->id,strtoupper($country->Country_name)).'</li>';
				}
			?>
		</ul>
		<hr>
		<p><?php echo $msg; ?></p>
		<form action="" method="POST">
		<?php 
			echo '<h3>'.$this->country->getCountryById($id)->Country_name.'</h3>';
			echo $this->market->rfbs_grid($id);
		?>
		<input id="date" name="date" type="text" placeholder="Date" class="datepicker"/>
		<br>
		<input name="submit" type="submit" class="btn btn-primary" value="Send"/>
		</form>
	</div>
</div>