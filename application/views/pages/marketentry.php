<div class="container-fluid">
    <div class="row-fluid">
    <div class="span4">
        <div class="well">
            <strong>WELCOME <?php echo strtoupper($data['username']); ?></strong>
            <hr>
            <strong>TODAYS ENTRIES - USD/KG</strong>
            <?php 
                $prices = $this->market->daily_prices($market->market_id);
                $tmpl = array ('table_open'=>'<table class="table table-condensed table-bordered">');
                $this->table->set_template($tmpl); 
                $this->table->set_heading('Product Name', 'Retail Price', 'WholeSale Price');
                foreach($prices as $price)
                {
                    $this->table->add_row($price->product_name,$price->retail_Price, $price->wholesale_Price);
                }
                echo $this->table->generate();
            ?>
            <a href="#" onclick="location.reload();" class="btn btn-primary">Refresh</a>
        </div>
    </div>
    <div class="span8">
        <h3>Data Entry Grid for <?php echo $market->market_name; ?></h3>
        <hr>
        <div class="info"></div>
        <form id="entry_form" class="form-horizontal" action="<?php echo base_url('site/market_data_entry'); ?>" method="POST">
            <div class="control-group">
                <label class="control-label" for="inputProduct">Product</label>
                <div class="controls">
                    <select id="product" name="product" class="chzn-select">
                    <?php
                        foreach ($product as $p)
                        {
                            echo '<option value="'.$p->product_id.'">'.$p->product_name.'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label" for="inputRetail">Retail Price</label>
                <div class="controls">
                    <input type="text" name="retail" id="inputRetail"  class="required" placeholder="Retail Price in Local Currency">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputWholeSale">WholeSale Price</label>
                <div class="controls">
                    <input type="text" name="wholesale" id="inputWholeSale"  class="required" placeholder="WholeSale Price in Local Currency">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="submit" id="submit" value="Send" class="btn"/>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        
        //Form Validation
        $( '#entry_form' ).validate({
            rules: {
			retail: "required",
            wholesale: "required"
		},
        messages:{
            retail: 'Retail Price is required',
            grade: 'WholeSale Price is required'
        }
        });
        //Success Info
        var options = {
            success: function(info){
                $('.info').html(info);
                $('.info').addClass('well');
            }
        };
        //Form Submission
        $('#entry_form').ajaxForm(options);
    });
</script>