<div class="container-fluid">
    <div class="row-fluid">
    <div class="span4">
        <div class="well">
            <strong>WELCOME <?php echo strtoupper($data['username']); ?></strong>
            <hr>
            <strong>LATEST ENTRIES</strong>
            <?php 
                $volume = $this->warehouse->daily_volumes($warehouse->id);
                $tmpl = array ('table_open'=>'<table class="table table-condensed table-bordered">');
                $this->table->set_template($tmpl); 
                $this->table->set_heading('Product Name', 'Grade', 'Stock', 'IN/OUT');
                foreach($volume as $v)
                {
                    if($v->direction == 'IN'){
                        $sign = '<span class="text-up">+'.$v->volume.'</span> ';
                    }else{
                        $sign = '<span class="text-down">-'.$v->volume.'</span> ';
                    }
                    $this->table->add_row($v->product_name,$v->name, $v->stock,$sign);
                }
                echo $this->table->generate();
            ?>
            <a href="#" onclick="location.reload();" class="btn btn-primary">Refresh</a>
        </div>
    </div>
    <div class="span8">
        <h3>Data Entry Grid for <?php echo $warehouse->warehouse_name; ?></h3>
        <hr>
        <div class="info"></div>
        <form id="entry_form" class="form-horizontal" action="<?php echo base_url('site/data_entry'); ?>" method="POST">
            <div class="control-group">
                <label class="control-label" for="inputProduct">Product</label>
                <div class="controls">
                    <select id="product" name="product" class="chzn-select">
                    <?php
                        foreach ($product as $p)
                        {
                            echo '<option value="'.$p->product_id.'">'.$p->product_name.'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputGrade">Grade</label>
                <div class="controls">
                    <select id="grade" name="grade" class="required">
                        
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputDirection">Direction</label>
                <div class="controls">
                     <select name="direction" class="chzn-select">
                    <?php
                        $direction = array('IN'=>'IN','OUT'=>'OUT');
                        foreach ($direction as $d)
                        {
                            echo '<option value="'.$d.'">'.$d.'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputVolume">Volume</label>
                <div class="controls">
                    <input type="text" name="volume" id="inputVolume"  class="required" placeholder="Volume in Metric Tonnes">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="submit" id="submit" value="Send" class="btn"/>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        //Dependent dropdown
        $('#product').bind('change keyup',function(){
            $.ajax({
                url:'<?php echo base_url('site/grade'); ?>',
                type: 'POST',
                data: {
                    product: $('#product').val()
                },
                success:function(result){
                    $("#grade").html(result);
                }  
            });
        });
        
        //Form Validation
        $( '#entry_form' ).validate({
            rules: {
			volume: "required",
            grade: "required"
		},
        messages:{
            volume: 'Volume is required',
            grade: 'Grade is required'
        }
        });
        //Success Info
        var options = {
            success: function(info){
                $('.info').html(info);
                $('.info').addClass('well');
            }
        }
        //Form Submission
        $('#entry_form').ajaxForm(options);
    });
</script>