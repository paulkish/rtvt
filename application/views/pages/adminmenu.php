<ul class="nav nav-list">
	<li class="nav-header">Manage</li>
	<li <?php if($this->uri->segment(3) == 'articles') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/articles'); ?>">News Articles</a></li>
	<li <?php if($this->uri->segment(3) == 'categories') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/categories'); ?>">Article Categories</a></li>
	<li <?php if($this->uri->segment(3) == 'bulletins') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/bulletins'); ?>">Bulletins &amp; Reports</a></li>
	<li <?php if($this->uri->segment(3) == 'markets') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/markets'); ?>">Market Data</a></li>
	<li <?php if($this->uri->segment(3) == 'warehouses') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/warehouses'); ?>">Warehouse Data</a></li>
	<li <?php if($this->uri->segment(3) == 'borders') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/borders'); ?>">Border Data</a></li>
	<li <?php if($this->uri->segment(3) == 'rfbs') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/rfbs'); ?>">RFBS Data</a></li>
	<li <?php if($this->uri->segment(3) == 'field_reports') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/field_reports'); ?>">Field Reports</a></li>
	<li <?php if($this->uri->segment(3) == 'rates') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/rates'); ?>">Exchange Rates</a></li>
	<li <?php if($this->uri->segment(3) == 'grid') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/grid'); ?>">Market Grid</a></li>
	<li <?php if($this->uri->segment(3) == 'rfbs_grid') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/rfbs_grid'); ?>">RFBS Grid</a></li>
	<li <?php if($this->uri->segment(3) == 'tradepoints') { echo 'class="active"'; }?> ><a href="<?php echo base_url('admin/manage/tradepoints'); ?>">Trade Points</a></li>
	<li <?php if($this->uri->segment(3) == 'products') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/products'); ?>">Products</a></li>
	<li <?php if($this->uri->segment(3) == 'grades') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/grades'); ?>">Grades</a></li>
	<li <?php if($this->uri->segment(3) == 'countries') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/countries'); ?>">Countries</a></li>
	<li <?php if($this->uri->segment(3) == 'users') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/users'); ?>">Agents</a></li>
	<li <?php if($this->uri->segment(3) == 'warehouse_info') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/warehouse_info'); ?>">Warehouse Info</a></li>
	<li <?php if($this->uri->segment(3) == 'email_subscribers') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/email_subscribers'); ?>">Newsletter Subscribers</a></li>
	<li <?php if($this->uri->segment(3) == 'subscribers') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/manage/subscribers'); ?>">SMS Subscribers</a></li>
	<li <?php if($this->uri->segment(3) == 'supermarket') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/supermarket'); ?>">Supermarket Prices</a></li>
	<li <?php if($this->uri->segment(3) == 'supermarket') { echo 'class="active"'; }?>><a href="<?php echo base_url('admin/miller'); ?>">Miller Prices</a></li>
</ul>