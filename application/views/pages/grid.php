<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<?php $this->load->view('pages/adminmenu'); ?>
		</div>
	</div>
	<div class="span9">
		<ul class="nav nav-pills">
			<?php 
				foreach($products as $product)
				{
					echo '<li>'.anchor('admin/manage/grid/'.$product->product_id,$product->product_name).'</li>';
				}
			?>
		</ul>
		<hr>
		<h3><?php echo $this->product->productName($id)->product_name; ?></h3>
		<p><?php echo $msg; ?></p>
		<form action="" method="POST">
		<?php 
			foreach($countries as $country)
			{
				echo $this->market->market_grid($country->Country_name);
			}
		?>
		<input id="date" name="date" type="text" placeholder="Date" class="datepicker"/>
		<br>
		<input name="submit" type="submit" class="btn btn-primary" value="Send"/>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
	 	$('input.datepicker').Zebra_DatePicker();
	});
</script>    
    