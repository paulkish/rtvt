<div class="row-fluid">
	<div class="span12">
	<h3>RTVT Storage Facilities | <?php echo $name; ?> </h3>
		<div id="chart_ajax"></div>
		<hr>
		<?php echo $data; ?>
	</div>
	<script>
	    $(document).ready(function() {
		    var base = '<?php echo base_url('site/product_chart');?>';
		    var product = '<?php echo urlencode($name); ?>';
			//Onload
			$('#chart_ajax').html('<center><?php echo img('resources/img/ajax-loader.gif')?></center>');
	        $('#chart_ajax').load(base+'/'+product); 
	    });
	</script>
</div>