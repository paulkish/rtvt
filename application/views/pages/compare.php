<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<h4>Filter</h4>
			<form id="entry_form" method="POST">
                <label class="control-label" for="inputProduct">Country</label>
                <select id="country" name="country" class="chzn-select" style="width:100px;">
	                <?php
	                    foreach ($this->country->country_list(FALSE) as $c)
	                    {
	                        echo '<option value="'.$c->Country_name.'">'.strtoupper($c->Country_name).'</option>';
	                    }
	                ?>
                </select>

                <label class="control-label" for="inputProduct">Product</label>
                <select id="product" name="product" class="chzn-select" style="width:100px;">
	                <?php
	                    foreach ($this->product->product_list() as $p)
	                    {
	                        echo '<option value="'.$p->product_name.'">'.ucfirst($p->product_name).'</option>';
	                    }
	                ?>
                </select>

                <label class="control-label" for="inputVolume">Start Date</label>
                <input type="text" name="start" id="start" class="required" placeholder="Start Date" style="width:100px;">

                <label class="control-label" for="inputVolume">End Date</label>
                <input type="text" name="end" id="end" class="required" placeholder="End Date" style="width:100px;">
                <br>
	            <input type="submit" id="submit" value="Filter" class="btn btn-primary"/>
			</form>
		</div>
	</div>
	<div class="span9">
		<div id="container">
			<script type="text/javascript">
				document.addEventListener("DOMContentLoaded", function() {
					<?php echo $js; ?>
				}, false);

				$(function() {
			        //Date picker
			        $('#start').Zebra_DatePicker({
			           //direction: true,
			           show_icon: false,
			           pair: $('#end')
			        });

			        $('#end').Zebra_DatePicker({
			           direction: 1,
			           show_icon: false
			        });
			    });
			</script>
		</div>
	</div>
</div>