<?php
  foreach ($product as $p)
  {
    $borders = $this->border->borders($p->product_id,$date);
    if($borders != NULL)
    {
      echo '<h4 style="color: #202020;
      display: block;
      font-family: Arial;
      font-size: 18px;
      color: #505050;
      line-height: 100%;
      margin: 0 0 10px;
      text-align: left;">'.strtoupper($p->product_name).'</h4>';
      $tmpl = array(
      'table_open'=>'<table style="
            width: 100%; margin-bottom: 20px;
            border: 1px solid #dddddd;
            border-collapse:collapse;
            background-color: #ffffff;">',
      'heading_cell_start'  => '<th style="font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
            border: 1px solid #dddddd;
            vertical-align: top;">',
      'cell_start' => '<td style="
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd;
            ">',
      'cell_alt_start'  => '<td style="
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd; ">',
      );
      $this->table->set_template($tmpl);
      $this->table->set_heading('Border','Source Country','Destination Country','Volume(MT)');
      foreach ($borders as $border)
      {
        $this->table->add_row($border->trade_point_name,ucfirst($border->Source_Country),ucfirst($border->Destination_Country),$border->volume);
      }
      echo $this->table->generate();
      echo '<hr style="margin: 20px 0;
            border: 0;
            border-top: 1px solid #eeeeee;
            border-bottom: 1px solid #ffffff;">';
    }
  }
?>