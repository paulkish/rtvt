<?php 
  foreach ($product as $p)
  {
    $markets = $this->market->markets($p->product_id,$date);
    if($markets != NULL)
    {
      echo '<h3 style="color: #202020;
      display: block;
      font-family: Arial;
      font-size: 22px;
      text-transform:uppercase;
      color: #505050;
      line-height: 100%;
      margin: 0 0 10px;
      text-align: left;">TOP 5 Markets</h3>';
      echo '<h4 style="color: #202020;
      display: block;
      font-family: Arial;
      font-size: 18px;
      color: #505050;
      line-height: 100%;
      margin: 0 0 10px;
      text-align: left;">'.strtoupper($p->product_name).'</h4>';
      $tmpl = array(
      'table_open'=>'<table style="
            width: 100%; margin-bottom: 20px;
            border: 1px solid #dddddd; 
            border-collapse:collapse;
            background-color: #ffffff;">',
      'heading_cell_start'  => '<th style="font-weight: bold; 
            padding: 8px;
            line-height: 20px;
            text-align: left;
            border: 1px solid #dddddd; 
            vertical-align: top;">',
      'cell_start' => '<td style=" 
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd; 
            ">',
      'cell_alt_start'  => '<td style=" 
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd; ">',
      );
      $this->table->set_template($tmpl); 
      $this->table->set_heading('Market','Country','Wholesale Price(USD/MT)','Retail Price(USD/MT)');
      foreach ($markets as $market) 
      {
        $rate = $market->exchange_rate;
        $whole = round($market->wholesale_Price * 1000,2);
        $retail = round($market->retail_Price * 1000,2);
        $this->table->add_row($market->market_name,$market->country,$whole,$retail);
      }
      echo $this->table->generate();
      echo '<hr style="margin: 20px 0;
            border: 0;
            border-top: 1px solid #eeeeee;
            border-bottom: 1px solid #ffffff;">';
    } 

    $markets = $this->market->markets_low($p->product_id,$date);
    if($markets != NULL)
    {
      echo '<h3 style="color: #202020;
      display: block;
      font-family: Arial;
      font-size: 22px;
      text-transform:uppercase;
      color: #505050;
      line-height: 100%;
      margin: 0 0 10px;
      text-align: left;">BOTTOM 5 Markets</h3>';
      echo '<h4 style="color: #202020;
      display: block;
      font-family: Arial;
      font-size: 18px;
      color: #505050;
      line-height: 100%;
      margin: 0 0 10px;
      text-align: left;">'.strtoupper($p->product_name).'</h4>';
      $tmpl = array(
      'table_open'=>'<table style="
            width: 100%; margin-bottom: 20px;
            border: 1px solid #dddddd; 
            border-collapse:collapse;
            background-color: #ffffff;">',
      'heading_cell_start'  => '<th style="font-weight: bold; 
            padding: 8px;
            line-height: 20px;
            text-align: left;
            border: 1px solid #dddddd; 
            vertical-align: top;">',
      'cell_start' => '<td style=" 
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd; 
            ">',
      'cell_alt_start'  => '<td style=" 
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border: 1px solid #dddddd; ">',
      );
      $this->table->set_template($tmpl); 
      $this->table->set_heading('Market','Country','Wholesale Price(USD/MT)','Retail Price(USD/MT)');
      foreach ($markets as $market) 
      {
        $rate = $market->exchange_rate;
        $whole = round($market->wholesale_Price * 1000,2);
        $retail = round($market->retail_Price * 1000,2);
        $this->table->add_row($market->market_name,$market->country,$whole,$retail);
      }
      echo $this->table->generate();
      echo '<hr style="margin: 20px 0;
            border: 0;
            border-top: 1px solid #eeeeee;
            border-bottom: 1px solid #ffffff;">';
    } 
  }
?>