<!DOCTYPE html>
<html>
	<head>
		<!-- Metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title><?php echo $template['title']; ?></title>
	     
	    <!-- Bootstrap Css -->
	    <link href="<?php echo base_url('resources/css');?>/bootstrap.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/css');?>/global.css" rel="stylesheet" media="screen">
		<link href="<?php echo base_url('resources/css');?>/zebra_datepicker.css" rel="stylesheet" media="screen">
		<!-- Jquery -->
	    <script src="<?php echo base_url('resources/js');?>/jquery-1.8.3.min.js"></script>
    </head>
   
    <body>
    <div class="navbar">
		<div class="navbar-inner">
		    <a class="brand" href="<?php echo base_url('admin/manage'); ?>">RATIN ADMINISTRATOR</a>

			<div class="pull-right">
				<ul class="nav">
					<li><?php echo anchor('site/index','Main Site',array('target'=>'_blank'));?></li>
					<?php if($this->ion_auth->logged_in()):?>
					<li><?php echo anchor('admin/manage','Dashboard');?></li>
					<li><?php echo anchor('admin/auth/logout','Logout');?></li>
					<?php endif; ?>
				</ul>
			</div>
        </div>
	</div>
    <div class="container-fluid">
    	<!-- Body Content -->
 		<?php echo $template['body']; ?>
    </div>
    <!-- Load Js at the bottom -->
 	<!-- Site Wide Js -->
    <script src="<?php echo base_url('resources/js');?>/bootstrap.min.js"></script>
    <!-- Page specific Js -->
    <?php echo $template['metadata']; ?>
    <script src="<?= base_url('resources/js/jquery.cookie.js'); ?>"></script>
    </body>
</html>
