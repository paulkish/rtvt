<!DOCTYPE html>
<html>
    <head>
        <!-- Metadata -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $template['title']; ?></title>

        <!-- Bootstrap Css -->
        <link href="<?php echo base_url('resources/css');?>/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('resources/css');?>/global.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('resources/css/chosen');?>/chosen.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('resources/js/tabletools/css');?>/TableTools.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('resources/css');?>/zebra_datepicker.css" rel="stylesheet" media="screen">
        <!-- Jquery -->
        <script src="<?php echo base_url('resources/js');?>/jquery-1.8.3.min.js"></script>
    </head>

    <body>

    <div class="container-fluid">
        <!-- Body Content -->
        <?php echo $template['body']; ?>

    <!-- Load Js at the bottom -->
    <!-- Site Wide Js -->
    </div>

    <script src="<?php echo base_url('resources/js');?>/bootstrap.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.marquee.js"></script>
    <script src="<?php echo base_url('resources/js');?>/site.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/dt_bootstrap.js"></script>
    <script src="<?php echo base_url('resources/js');?>/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.form.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.validate.js"></script>

    <!-- Page specific Js -->
    <?php echo $template['metadata']; ?>
    </body>
</html>
