<!DOCTYPE html>
<html>
	<head>
		<!-- Metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title><?php echo $template['title']; ?></title>
	     
	    <!-- Bootstrap Css -->
	    <link href="<?php echo base_url('resources/css');?>/bootstrap.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/css');?>/global.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/js/tabletools/css');?>/TableTools.css" rel="stylesheet" media="screen">
		
		<style>
			a {
				color: #CC660B;
				font-weight: inherit;
				line-height: inherit;
				text-decoration: none;
			}
		</style>
	    <!-- Jquery -->
	    <script src="<?php echo base_url('resources/js');?>/jquery-1.8.3.min.js"></script>
    </head>
    <body>
        <div class="container-fluid">
     		<?php echo $template['body']; ?>
        </div>
    
        <script src="<?php echo base_url('resources/js');?>/bootstrap.min.js"></script>
        <script src="<?php echo base_url('resources/js');?>/jquery.marquee.js"></script>
    	<script src="<?php echo base_url('resources/js');?>/site.js"></script>
    	<script src="<?php echo base_url('resources/js');?>/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('resources/js');?>/dt_bootstrap.js"></script> 
        <script src="<?php echo base_url('resources/js');?>/chosen.jquery.min.js"></script>
        <!-- Page specific Js -->
        <?php echo $template['metadata']; ?>
    </body>
</html>
