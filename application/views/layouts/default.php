<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metadata']; ?>

	    <title><?php echo $template['title']; ?></title>

	    <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    	<link href="<?= base_url('resources/css/pikaday.css');?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url('resources/css/multiple-select.css');?>">
        <link rel="stylesheet" href="<?= base_url('resources/css/site.css');?>">
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    </head>
    <body>

     <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <a class="pull-left" href="<?= base_url(); ?>"><?= img(array('src'=>base_url('resources/img/logo.png'),'alt'=>'RATIN','height'=>'120')); ?></a>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ratin-navbar-collapse-top">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="ratin-navbar-collapse-top">
                <ul class="nav navbar-nav navbar-right navbar-top">
                    <li><a href="<?= base_url('site/page/kenya'); ?>"><?= img(array('src'=>base_url('resources/icons/ke.png'),'style'=>'margin-bottom:2px;','alt'=>'Kenya')); ?> Kenya</a></li>
                    <li><a href="<?= base_url('site/page/uganda'); ?>"><?= img(array('src'=>base_url('resources/icons/ug.png'),'style'=>'margin-bottom:2px;','alt'=>'Uganda')); ?> Uganda</a></li>
                    <li><a href="<?= base_url('site/page/tanzania'); ?>"><?= img(array('src'=>base_url('resources/icons/tz.png'),'style'=>'margin-bottom:2px;','alt'=>'Tanzania')); ?> Tanzania</a></li>
                    <li><a href="<?= base_url('site/page/rwanda'); ?>"><?= img(array('src'=>base_url('resources/icons/rw.png'),'style'=>'margin-bottom:2px;','alt'=>'Rwanda')); ?> Rwanda</a></li>
                    <li><a href="<?= base_url('site/page/burundi'); ?>"><?= img(array('src'=>base_url('resources/icons/bi.png'),'style'=>'margin-bottom:2px;','alt'=>'Burundi')); ?> Burundi</a></li>
                    <li><a href="<?= base_url('site/page/'.urlencode('south sudan')); ?>"><?= img(array('src'=>base_url('resources/icons/ss.png'),'style'=>'margin-bottom:2px;','alt'=>'South Sudan')); ?> South Sudan</a></li>
                </ul>
            </div>
        </div>
        <!-- /.container -->
    </nav>
    
    <!-- Navigation -->
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ratin-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="ratin-navbar-collapse">
                    <ul class="nav navbar-nav">
                    	<li><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php foreach(about_articles() as $pages): ?>
                                <li><a href="<?= base_url('site/about/'.$pages->id); ?>"><?= $pages->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Markets Prices <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('site/market'); ?>">Market Prices</a></li>
                                <li><a href="<?= base_url('site/market_search'); ?>">Market Prices Search</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Crossborder Trade <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('site/border'); ?>">Crossborder Trade</a></li>
                                <li><a href="<?= base_url('site/border_search'); ?>">Crossborder Trade Search</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Warehouses <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('site/map'); ?>">Certified Warehouses</a></li>
                                <li><a href="<?= base_url('site/grain_storage'); ?>">Grain Storage Facilities Within EAC</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">News &amp; Bulletins <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('site/news'); ?>">News</a></li>
                                <li><a href="<?= base_url('site/bulletins'); ?>">Bulletins &amp; Reports</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    	<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $this->ion_auth->logged_in() ? $this->ion_auth->user()->row()->first_name:'Login' ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php if(!$this->ion_auth->logged_in()):?>
                                    <li><?php echo anchor('site/login','Login'); ?></li>
                                    <li><?php echo anchor('site/signup','Signup'); ?></li>
                                <?php elseif($this->ion_auth->logged_in() AND !$this->ion_auth->is_admin()):?>
                                    <li><?php echo anchor('site/logout','Logout'); ?></li>
                                <?php else: ?>
                                    <li><?php echo anchor("/admin/auth/index", 'Admin') ;?></li>
                                    <li><?php echo anchor('site/logout','Logout'); ?></li>
                                <?php endif;?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> Currency &amp; Packaging <b class="caret"></b></a>
                            <ul class="dropdown-menu message-dropdown">
                                <li class="message-preview">
                                    <div class="media">
                                        <div class="media-body">
                                            <h5 class="media-heading"><strong>Adjust</strong></h5>
                                            <form role="form">
                                                <div class="form-group">
                                                    <select id="selCurrency" name="currency" class="form-control">
                                                        <option value="USD">USD</option>
                                                        <?php foreach (currencies() as $currency): ?>
                                                        <option value="<?= $currency['Currency']; ?>"><?= $currency['Currency']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select id="selMeasure" name="measure" class="form-control">
                                                        <option value="1">1 Kg</option>
                                                        <option value="50">50 Kg</option>
                                                        <option value="90">90 Kg</option>
                                                        <option value="1000">Metric Tonne</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-convert" value="Convert">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
    </div>

    <div class="container">

	   	<!-- Body Content -->
	 	<?php echo $template['body']; ?>

        <hr>
	 	<!-- Footer -->
	    <footer>
	        <div class="row">
	            <div class="col-lg-12">
	                <p>Copyright &copy; RATIN <?= date('Y');?></p>
	            </div>
	        </div>
	    </footer>
    </div>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
    <script src="<?= base_url('resources/js/jquery.cookie.js'); ?>"></script>
    <script src="<?= base_url('resources/js/moment.js'); ?>"></script>
    <script src="<?= base_url('resources/js/pikaday.js'); ?>"></script>
    <script src="<?= base_url('resources/js/jquery.form.js');?>"></script>
    <script src="<?= base_url('resources/js/jquery.validate.js');?>"></script>
    <script src="<?= base_url('resources/js/jquery.als-1.7.min.js');?>"></script>
    <script src="<?= base_url('resources/js/multiple-select.js');?>"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <?php if($this->ion_auth->logged_in()):?>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
    <?php endif; ?>
    <script src="//cdn.jsdelivr.net/jquery.marquee/1.3.1/jquery.marquee.min.js" type="text/javascript"></script>
    <script src="<?= base_url('resources/js/site-new.js'); ?>"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-24462677-2', 'ratin.net');
        ga('send', 'pageview');
    </script>
    </body>
</html>
