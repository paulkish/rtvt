<!DOCTYPE html>
<html>
	<head>
		<!-- Metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title><?php echo $template['title']; ?></title>

	    <!-- Bootstrap Css -->
	    <link href="<?php echo base_url('resources/css');?>/bootstrap.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/css');?>/global.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/css/chosen');?>/chosen.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/js/tabletools/css');?>/TableTools.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('resources/css');?>/zebra_datepicker.css" rel="stylesheet" media="screen">
	    <!-- Jquery -->
	    <script src="<?php echo base_url('resources/js');?>/jquery-1.8.3.min.js"></script>
    </head>

    <body>
    <div class="navbar">
		<div class="navbar-inner">
			<div class="container">
			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</a>
		    <a class="brand" href="<?php echo base_url('rtvt/site'); ?>">REAL TIME VOLUME TRACKING</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
				<li><?php echo anchor('rtvt/site/storage','STORAGE FACILITIES');?></li>
				<li><?php echo anchor('rtvt/site/product','RTVT PRODUCTS');?></li>
				<li><?php echo anchor('rtvt/site/search','SEARCH');?></li>
				<li><?php echo anchor('rtvt/download','GRAIN STANDARDS');?></li>
				<?php $data = $this->session->all_userdata(); ?>
				<?php
					if(isset($data['logged_in'])){
						echo '<li>'.anchor('rtvt/site/logout','LOGOUT').'</li>';
					}
					else{
						echo '<li>'.anchor('rtvt/site/login','LOGIN').'</li>';
					}
				?>
				</ul>
			</div>
		    <div class="pull-right">
                <?php
                if(isset($template['partials']['help']))
                {
                	echo $template['partials']['help'];
                }
                ?>
                <?php echo anchor('rtvt/site/subscribe','SUBSCRIBE',array('role'=>'button',
                'class'=>'btn btn-small btn-info','data-toggle'=>'modal','data-target'=>'#myModal'));?>
                </ul>
            </div>
			</div>
        </div>
	</div>
    <div class="container-fluid">
    	<!-- Body Content -->
 		<?php echo $template['body']; ?>

 	<!-- Modal site help -->
 	<!-- Modal -->
	<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		</div>
		<div class="modal-body">
		  <center><?php echo img(base_url('resources/img/ajax-loader.gif')); ?></center>
		</div>
		<div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		</div>
	</div>
 	<!-- Load Js at the bottom -->
 	<!-- Site Wide Js -->
    </div>

    <script src="<?php echo base_url('resources/js');?>/bootstrap.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.marquee.js"></script>
	<script src="<?php echo base_url('resources/js');?>/site.js"></script>
	<script src="<?php echo base_url('resources/js');?>/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/dt_bootstrap.js"></script>
    <script src="<?php echo base_url('resources/js');?>/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.form.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.validate.js"></script>
    <script>
        //Force modal to destroy after close
        $('body').on('hidden', '.modal', function () {
          $(this).removeData('modal');
        });
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-24462677-2', 'ratin.net');
        ga('send', 'pageview');
    </script>
    <!-- Page specific Js -->
    <?php echo $template['metadata']; ?>
    </body>
</html>
