<!DOCTYPE html>
<html>
	<head>
		<!-- Metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title><?php echo $template['title']; ?></title>
	     
	    <!-- Bootstrap Css -->
	    <link href="<?php echo base_url('resources/css');?>/bootstrap.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/css');?>/global.css" rel="stylesheet" media="screen">
		
        <?php
		if(isset($css_files))
		foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php
		if(isset($js_files))
		foreach($js_files as $file): ?>
            <script src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
    </head>
   
    <body>
    <div class="navbar">
		<div class="navbar-inner">
		    <a class="brand" href="<?php echo base_url('admin/manage'); ?>">RATIN ADMINISTRATOR</a>
			
			<div class="pull-right">
				<ul class="nav">
					<li><?php echo anchor('site/index','Main Site',array('target'=>'_blank'));?></li>
					<?php if($this->ion_auth->logged_in()):?>
					<li><?php echo anchor('admin/manage','Dashboard');?></li>
						<?php if($this->ion_auth->is_admin()): ?>
							<li><?php echo anchor('admin/auth/index','Users');?></li>
						<?php endif; ?>
					<li><?php echo anchor('admin/auth/logout','Logout');?></li>
					<?php endif; ?>
				</ul>
			</div>
        </div>
	</div>
    <div class="container-fluid">
    	<!-- Body Content -->
 		<?php echo $template['body']; ?>	
    </div>
   
    <!-- Page specific Js -->
	<?php echo $template['metadata']; ?>
    </body>
</html>
