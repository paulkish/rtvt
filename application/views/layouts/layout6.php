<!DOCTYPE html>
<html>
	<head>
		<!-- Metadata -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title><?php echo $template['title']; ?></title>

	    <!-- Bootstrap Css -->

	    <link href="<?php echo base_url('resources/css');?>/global.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url('resources/css');?>/ratin-bootstrap.css" rel="stylesheet" media="screen">
	    <link href="<?php echo base_url('resources/css/chosen');?>/chosen.css" rel="stylesheet" media="screen">

	    <!-- Jquery -->
	    <script src="<?php echo base_url('resources/js');?>/jquery-1.8.3.min.js"></script>
    </head>

    <body>
    <?php echo $template['body']; ?>

 	<!-- Site Wide Js -->
    <script src="<?php echo base_url('resources/js');?>/bootstrap.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.marquee.js"></script>
	<script src="<?php echo base_url('resources/js');?>/site.js"></script>
	<script src="<?php echo base_url('resources/js');?>/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/dt_bootstrap.js"></script>
    <script src="<?php echo base_url('resources/js');?>/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.form.js"></script>
    <script src="<?php echo base_url('resources/js');?>/jquery.validate.js"></script>

    <script type="text/javascript">
        $(".chzn-select").chosen();
    </script>

    <!-- Page specific Js -->
    <?php echo $template['metadata']; ?>
    </body>
</html>
