<script>
$(document).ready(function() {
	$('#product-table a:first').tab('show'); 
});
</script>
<ul class="nav nav-tabs" id="product-table">
	<?php foreach ($grade as $g){?>
    	<li><a href="<?php echo '#grade'.$g->id; ?>" data-toggle="tab"><?php echo $g->name; ?></a></li>
    <?php }; ?>
</ul>
<div class="tab-content">
	<?php foreach ($grade as $g){?>
		<div class="tab-pane" id="<?php echo 'grade'.$g->id; ?>">
			<?php echo $this->warehouse->product_volume($g->id);?>
		</div>
	<?php } ;?>
</div>
