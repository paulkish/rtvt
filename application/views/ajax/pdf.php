<?php 
	// Redirect output to a client’s web browser (PDF)
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment;filename="'.$name.'.pdf"');
	header('Cache-Control: max-age=0');

	$objWriter = IOFactory::createWriter($data, 'PDF');
	$objWriter->save('php://output');
	exit;