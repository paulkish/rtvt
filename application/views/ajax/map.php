<div style="font-size:9px;">
<?php
	//Map Info Window
	$tmpl = array ('table_open'=>'<table class="table-condensed">');
	$this->table->set_template($tmpl); 
	$this->table->add_row('Capacity',$info->Capacity);
	$this->table->add_row('Contact Person',$info->contactPerson);
	$this->table->add_row('Tel',$info->phone);
	$this->table->add_row('Email',$info->email);
	echo '<strong>'.$info->warehouse_name.'</strong>';
	echo '<hr class="small">';
	echo '<small>last update:<em>'.$time->time.' '.$time->date.'</em></small>';
	echo $this->table->generate();
	echo '<hr class="small">';
	$this->table->clear();
	$tmpl = array ('table_open'=>'<table class="table table-condensed table-bordered">');
	$this->table->set_template($tmpl); 
	$this->table->set_heading('Product', 'Grade', 'Stock', '+/-');
	foreach($volume as $v)
	{
	    if($v->direction == 'IN'){
		    $sign = '<span class="text-up">+'.$v->volume.'</span> ';
		}else{
			$sign = '<span class="text-down">-'.$v->volume.'</span> ';
	    }
	    $this->table->add_row($v->product_name,$v->name, $v->stock,$sign);
	}
	echo $this->table->generate();
	echo anchor("rtvt/site/warehouse_info/$id",'View More');
?>
</div>