<script type="text/javascript">
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'chart',
                defaultSeriesType: 'line',
                zoomType: 'x'
            },
            title: {
                text: 'Drag mouse to view in detail | Click legend to toggle series',
                x: -20,
                style:{
                    color: '#3E576F',
                    fontSize: '13px'
                }

            },
            xAxis: {
            	type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Volumes'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+this.y +' MT';
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [
			<?php
                $count = count($data);
                $reps = 0;
			    foreach($data as $cv)
			    {
                    $reps ++;
			        echo '{';
			        echo "name:'$cv->name',";
			        echo "data:[".$cv->data."]";
                    if($reps === $count)
			            echo '}';
                    else
                        echo '},';
			    }
			?>
			]
        });
    });
    
</script>
<!-- Chart Div -->
<div style="width:95%; height: 300px;" id="chart"></div>