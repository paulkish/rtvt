<div class="info"></div>
<form id="subscribe_form" class="form-horizontal" action="<?php echo base_url('site/register'); ?>" method="POST">
     <div class="control-group">
        <label class="control-label" for="inputName">Name</label>
        <div class="controls">
            <input type="text" name="name" id="inputName" placeholder="Name">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Email</label>
        <div class="controls">
            <input type="text" name="email" id="inputEmail" placeholder="Email">
        </div>
    </div>
   
    <div class="control-group">
        <div class="controls">
            <label class="checkbox">
                <input value="1" type="checkbox"/> Subscribe to Newsletter
            </label>
            <input type="submit" id="submit" value="Subscribe" class="btn"/>
        </div>
    </div>
</form>
<script>
    $(document).ready(function() {
        $('#subscribe_form').ajaxForm(function() { 
            $('.info').html('Thank you for your subscription!');
            $('#myModal').modal('hide')
        }); 
        
    });
</script>


