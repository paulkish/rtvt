<div class="row-fluid">
	<div class="span3">
		<?php include('menu.php'); ?> 
	</div>
	<div class="span9">
		<div class="row-fluid">
			<div class="span12">
				<div class="pull-left">
					<h3><?php echo lang('index_heading');?></h3>
					<p><?php echo lang('index_subheading');?></p>
				</div>
				<div class="pull-right">
					<?php echo anchor('admin/auth/create_user', lang('index_create_user_link'),array('class'=>'btn'))?>
				</div>
			</div>
		</div>
		<div id="infoMessage"><?php echo $message;?></div>
		
		<table class="table table-bordered table-condensed">
			<tr>
				<th><?php echo lang('index_fname_th');?></th>
				<th><?php echo lang('index_lname_th');?></th>
				<th><?php echo lang('index_email_th');?></th>
				<th>Created On</th>
				<th>Last Login</th>
				<th><?php echo lang('index_groups_th');?></th>
				<th><?php echo lang('index_status_th');?></th>
				<th><?php echo lang('index_action_th');?></th>
			</tr>
			<?php foreach ($users as $user):?>
				<tr>
					<td><?php echo $user->first_name;?></td>
					<td><?php echo $user->last_name;?></td>
					<td><?php echo $user->email;?></td>
					<td><?php echo date('jS M Y',$user->created_on);?></td>
					<td><?php echo date('jS M Y',$user->last_login);?></td>
					<td>
						<?php foreach ($user->groups as $group):?> 
							<?php echo anchor("admin/auth/edit_group/".$group->id,'['.$group->name.']') ;?>
						<?php endforeach?>
					</td>
					<td><?php echo ($user->active) ? anchor("admin/auth/deactivate/".$user->id, lang('index_active_link')) : anchor("admin/auth/activate/". $user->id, lang('index_inactive_link'));?></td>
					<td><?php echo anchor("admin/auth/edit_user/".$user->id, 'Edit') ;?> | <?php echo anchor("admin/auth/delete_user/".$user->id, 'Delete') ;?></td>
				</tr>
			<?php endforeach;?>
		</table>
		<div class="pull-left">
            <?php echo $this->pagination->create_links(); ?>
        </div>
		<div class="pull-right">
	        <form class="form-inline" id="page-size-form" method="get">
	            <select id="page-size" name="per_page" class="page-size form-control">
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="75">75</option>
                    <option value="100">100</option>
                </select>
	        </form>
        </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		// On change set cookie for pagination library to read
		$('.page-size').change(function(){
		    $.cookie('per_page', $(this).val());
		    window.location.reload(true); 
		});

		var pagesize = $.cookie("per_page");
		if(typeof pagesize != 'undefined'){
			$('.page-size').val(pagesize);
		}
	});
</script>