<div class="row-fluid">
	<div class="span3">
		<?php include('menu.php'); ?> 
	</div>
	<div class="span9">
		<h3><?php echo lang('create_user_heading');?></h3>
		<p><?php echo lang('create_user_subheading');?></p>

		<div id="infoMessage"><?php echo $message;?></div>

		<?php echo form_open("admin/auth/create_user");?>

		<p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
		</p>
		<p>
            <?php echo lang('create_user_lname_label', 'first_name');?> <br />
            <?php echo form_input($last_name);?>
		</p>
		<p>
            <?php echo lang('create_user_category_label', 'category');?> <br />
            <?php echo form_dropdown(
            $category['name'],
             array('Farmer'=>'Farmer/Producer','Processor'=>'Processor/Trader','Researcher/Student'=>'Researcher/Student','NGO'=>'NGO','Policy Maker/Government'=>'Policy Maker/Government','Other'=>'Other'),
            $category['value']
            );?>
		</p>	
		<p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
		</p>
		<p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
		</p>
		<p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
		</p>
		<p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
		</p>

		<p><?php echo form_submit('submit', lang('create_user_submit_btn'),'class = "btn"');?></p>

		<?php echo form_close();?>
	</div>
</div>
