<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<ul class="nav nav-list">
				<li class="nav-header">Manage</li>
				<li><?php echo anchor('admin/auth/create_user', lang('index_create_user_link'))?></li> 
				<li><?php echo anchor('admin/auth/create_group', lang('index_create_group_link'))?></li>
			</ul>
		</div>
	</div>
	<div class="span9">
		<h3><?php echo lang('deactivate_heading');?></h3>
		<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

		<?php echo form_open("admin/auth/deactivate/".$user->id);?>

		<p>
			<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
			<input type="radio" name="confirm" value="yes" checked="checked" />
			<?php echo lang('deactivate_confirm_n_label', 'confirm');?>
			<input type="radio" name="confirm" value="no" />
	   </p>

	   <?php echo form_hidden($csrf); ?>
	   <?php echo form_hidden(array('id'=>$user->id)); ?>

	   <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),'class = "btn"');?></p>

	   <?php echo form_close();?>
	</div>
</div>