<div class="row-fluid">
	<div class="span3">
		<?php include('menu.php'); ?> 
	</div>
	<div class="span9">
		<div class="row-fluid">
			<div class="span12">
				<div class="pull-left">
					<h3>Groups</h3>
					<p>User Groups</p>
				</div>
				<div class="pull-right">
					<?php echo anchor('admin/auth/create_group', lang('index_create_group_link'),array('class'=>'btn'))?>
				</div>
			</div>
		</div>

		<div id="infoMessage"><?php echo $message;?></div>
		
		<table class="table table-bordered table-condensed">
			<tr>
				<th>Group</th>
				<th>Description</th>
				<th>Action</th>
			</tr>
			<?php foreach ($groups as $group):?>
				<tr>
					<td><?php echo $group->name;?></td>
					<td><?php echo $group->description;?></td>
					<td><?php echo anchor("admin/auth/edit_group/".$group->id, 'Edit') ;?></td>
				</tr>
			<?php endforeach;?>
		</table>
	</div>
</div>
</p>