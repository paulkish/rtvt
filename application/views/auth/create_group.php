<div class="row-fluid">
	<div class="span3">
		<div class="well">
			<ul class="nav nav-list">
				<li class="nav-header">Manage</li>
				<li><?php echo anchor('admin/auth/create_user', lang('index_create_user_link'))?></li> 
				<li><?php echo anchor('admin/auth/create_group', lang('index_create_group_link'))?></li>
			</ul>
		</div>
	</div>
	<div class="span9">
		<h3><?php echo lang('create_group_heading');?></h3>
		<p><?php echo lang('create_group_subheading');?></p>
		
		<div id="infoMessage"><?php echo $message;?></div>
		
		<?php echo form_open("admin/auth/create_group");?>
		<p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
		</p>
		<p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
		</p>
		<p><?php echo form_submit('submit', lang('create_group_submit_btn'),'class = "btn"');?></p>
		<?php echo form_close();?>
	</div>
</div>



