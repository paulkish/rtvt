<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supermarket extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('smarket_model'));
		$this->load->library(array('session','ion_auth','grocery_CRUD'));

        //Load db connection
        $this->db = $this->load->database('smarket', TRUE);

		if(!$this->ion_auth->logged_in())
		{
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('admin/auth/login');
		}
    }

    public function index($id=1)
    {
    	//Products Menu
    	$products = $this->smarket_model->get_list('product','id,product_name','object');

    	//Towns
    	$towns = $this->smarket_model->get_list('town','id,town_name','object');
    	//Weights
    	$weights = $this->smarket_model->get_list('weight','id,weight','object');
    	//Count
    	$count = 0;

    	//build grid
    	$output = array('products'=>$products,'towns'=>$towns,'weights'=>$weights,'id'=>$id);

    	//check if form submitted
    	if($this->input->post('submit'))
    	{
    		foreach($this->input->post() as $key => $value)
    		{
    			if($key != 'submit' && $key != 'date' && $key != 'brand' && $value != '' && !empty($value))
    			{
    				$data = explode('_',$key);
	    			$date = $this->input->post('date');
                    $brand = $this->input->post('brand');

	    			//build insert
	    			$sprices = array(
	    				'town_id'=>$data[0],
	    				'product_id'=>$id,
	    				'supermarket_id'=>$data[2],
	    				'weight_id'=>$data[1],
	    				'price'=>$value,
	    				'date'=>$date,
                        'brand_id'=>$brand
	    			);

	    			//insert
	    			$insert = $this->smarket_model->add('supermarket_price',$sprices);
	    			if($insert)
	    				$count++; //count inserted records
    			}
    		}

    		//display flash message on success
    		$this->session->set_flashdata('info', '<div class="alert alert-success">'.$count.' record(s) added</div>');
    	    // redirect to page
            redirect('admin/supermarket/index');
        }

    	//build layout
    	$this->template->set_layout('layout5');
    	$this->template->title('Ratin Admin | Supermarket Grid');
    	$this->template->append_metadata('<script src="'.base_url('resources/js/').'/zebra_datepicker.js"></script>');
		$this->template->build('pages/supermarket',$output);
    }


    //Supermarket Prices CRUD
    public function prices()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('supermarket_price')
        ->set_subject('Supermarket Price')
        ->columns('town_id','product_id','brand_id','weight_id','supermarket_id','price','date')
        ->fields('town_id','product_id','brand_id','weight_id','supermarket_id','price','date')
        ->display_as('product_id','Product')
        ->display_as('brand_id','Brand')
        ->display_as('town_id','Town')
        ->display_as('weight_id','Weight')
        ->display_as('supermarket_id','Supermarket')
        ->order_by('date','desc')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //relations
        $this->grocery_crud
        ->set_relation('product_id','product','product_name')
        ->set_relation('brand_id','brand','brand_name')
        ->set_relation('town_id','town','town_name')
        ->set_relation('weight_id','weight','weight')
        ->set_relation('supermarket_id','supermarket','supermarket_name');

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Supermarket Prices');
        $this->template->build('pages/supermarket_manage',$output);
    }

    //Towns CRUD
    public function towns()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('town')
        ->set_subject('Town')
        ->columns('town_name')
        ->fields('town_name')
        ->display_as('town_name','Name')
        //->display_as('country_id','Country')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Towns');
        $this->template->build('pages/supermarket_manage',$output);
    }

    //brand CRUD
    public function brands()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('brand')
        ->set_subject('Brands')
        ->columns('brand_name','product_id')
        ->fields('brand_name','product_id')
        ->display_as('brand_name','Brand')
        ->display_as('product_id','Product')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //relations
        $this->grocery_crud
        ->set_relation('product_id','product','product_name');

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Brands');
        $this->template->build('pages/supermarket_manage',$output);
    }

    //Products CRUD
    public function products()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('product')
        ->set_subject('Product')
        ->columns('product_name')
        ->fields('product_name')
        ->display_as('product_name','Name')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Products');
        $this->template->build('pages/supermarket_manage',$output);
    }

    //Weight CRUD
    public function weight()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('weight')
        ->set_subject('Weight')
        ->columns('weight')
        ->fields('weight')
        ->display_as('weight','Weight')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Weight');
        $this->template->build('pages/supermarket_manage',$output);
    }

    //Supermarket CRUD
    public function supermarkets()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('supermarket')
        ->set_subject('Supermarket')
        ->columns('supermarket_name','town_id')
        ->fields('supermarket_name','town_id')
        ->display_as('town_id','Town')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //relations
        $this->grocery_crud
        ->set_relation('town_id','town','town_name');

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Supermarkets');
        $this->template->build('pages/supermarket_manage',$output);
    }
}
