<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('session','grocery_CRUD','ion_auth'));

		if(!$this->ion_auth->logged_in())
		{
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('admin/auth/login');
		}
	}

	function _crud_output($output=null,$name=null)
	{
		$this->template->set_layout('layout3');
		$this->template->title('Ratin Admin | '.$name);
		$this->template->build('pages/manage',$output);
	}

	// default landing
	function index(){
		// redirect to articles
		redirect('admin/manage/articles');
	}

	//default landing page - Tradepoints
	function tradepoints()
	{
		$this->load->model('country');
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('trade_point')
		->set_subject('Trade Point')
		->unset_export()
		->unset_print();
		$this->grocery_crud->unset_columns('id','trade_point_alias','code','status','country_id','extent_X_MAX','extent_Y_MAX','extent_Y_MIN','extent_X_MIN');
		$this->grocery_crud->unset_fields('id','code','status','country_id','extent_X_MAX','extent_Y_MAX','extent_Y_MIN','extent_X_MIN');
		$this->grocery_crud->field_type('Publish','dropdown',array('0'=>'No','1'=>'Yes'));
		$this->grocery_crud->field_type('TYPE','dropdown',array('Market' => 'Market', 'Warehouse'=>'Warehouse','Border'=>'Border'));
		
		$countries = $this->country->countrylist();
		$list = array();
		foreach ($countries as $country) {
			$list[$country->country_name] = $country->country_name;
		}
		$this->grocery_crud->field_type('country','dropdown',$list);
	
		$this->grocery_crud
			->display_as('trade_point_id','Tradepoint ID')
			->display_as('trade_point_name','Name')
			->display_as('POINT_X','Point X')
			->display_as('POINT_Y','Point Y')
			->display_as('TYPE','Type');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Trade Points');

	}

	//articles data
	function articles()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('articles')
		->set_subject('News Article')
		->columns('title','category','created_by','created_on','published')
		->fields('title','category','description','content','published','file','created_by','created_on')
		->required_fields('title','category','description','content','published')
		->unset_texteditor('description')
		->field_type('published','dropdown',array('0' => 'No', '1' => 'Yes'))
		->set_field_upload('file','assets/uploads/files')
		->order_by('created_on','desc')
		->change_field_type('created_by','invisible')
		->change_field_type('created_on','invisible')
		->unset_export()
		->unset_print();

		$this->grocery_crud
		->set_relation('created_by','users_ion','username')
		->set_relation('category','categories','category');

		$this->grocery_crud
		->callback_before_insert(array($this,'article_create_callback'));

		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'News Articles');
	}

	//categories
	function categories()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('categories')
		->set_subject('Article Categories')
		->unset_export()
		->unset_publish()
		->unset_print();
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Article Categories');
	}

	//bulletins data
	function bulletins()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('bulletins')
		->set_subject('Bulletin/Report')
		->columns('title','created_by','created_on','published')
		->fields('title','description','published','file','created_by','created_on')
		->required_fields('title','description','content','published')
		->unset_texteditor('description')
		->field_type('published','dropdown',array('0' => 'No', '1' => 'Yes'))
		->set_field_upload('file','assets/uploads/files')
		->order_by('created_on','desc')
		->change_field_type('created_by','invisible')
		->change_field_type('created_on','invisible')
		->unset_export()
		->unset_print();

		$this->grocery_crud
		->set_relation('created_by','users_ion','username');

		$this->grocery_crud
		->callback_before_insert(array($this,'article_create_callback'));

		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Bulletins & Reports');
	}

	//exchange rates
	function rates()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('exchange_rates')
		->set_subject('Exchange Rate')
		->order_by('date','desc')
		->unset_export()
		->unset_print();
		$this->grocery_crud->unset_columns('params');
		$this->grocery_crud->unset_fields('params');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Exchange Rates');
	}

	//market data
	function markets()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('daily_trading_info')
		->set_subject('Market Data')
		->columns('market_id','product_id','retail_price','wholesale_price','date','date_time','publish')
		->fields('market_id','product_id','retail_price','wholesale_price','date','publish')
		->display_as('product_id','Product Name')
		->display_as('market_id','Market Name')
		->field_type('publish','dropdown',array('0' => 'No', '1' => 'Yes'))
		->order_by('date_time','desc')
		->unset_export()
		->unset_print();

		$this->grocery_crud->callback_column('date_time',array($this,'datetime_callback'));

		$this->grocery_crud
		->set_relation('product_id','product','product_name')
		->set_relation('market_id','trade_point','trade_point_name',array('TYPE' => 'Market'));

		$this->grocery_crud // convert price before insert
		->callback_before_insert(array($this,'price_callback'))
		->callback_before_update(array($this,'price_callback'));

		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Market Data');
	}

	//warehouse data
	function warehouses()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('warehouse_data_flow')
		->set_subject('Warehouse Data')
		->columns('whouse_id','grade_id','direction','volume','stock','date','time','publish')
		->fields('whouse_id','grade_id','direction','volume','date','time','publish')
		->display_as('grade_id','Grade')
		->display_as('whouse_id','Warehouse')
		->field_type('publish','dropdown',array('0' => 'No', '1' => 'Yes'))
		->field_type('direction','dropdown',array('IN' => 'IN', 'OUT' => 'OUT'))
		->order_by('date','desc')
		->unset_export()
		->unset_print();
		$this->grocery_crud
		->set_primary_key('id','pdct_grade')
		->set_relation('grade_id','pdct_grade','{product_name}-{name}')
		->set_relation('whouse_id','trade_point','trade_point_name',array('TYPE' => 'Warehouse'));
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Warehouse Data');
	}

	//border data
	function borders()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('crossborder_tradeflow')
		->set_subject('Border Data')
		->columns('border_id','product_id','volume','Source_Country','Destination_Country','date','publish')
		->fields('border_id','product_id','volume','Source_Country','Destination_Country','date','publish')
		->display_as('product_id','Product Name')
		->display_as('border_id','Border')
		->field_type('publish','dropdown',array('0' => 'No', '1' => 'Yes'))
		->field_type('Source_Country','dropdown',array('Kenya'=>'Kenya','Uganda'=>'Uganda','Tanzania'=>'Tanzania','Rwanda'=>'Rwanda','Burundi'=>'Burundi'))
		->field_type('Destination_Country','dropdown',array('Kenya'=>'Kenya','Uganda'=>'Uganda','Tanzania'=>'Tanzania','Rwanda'=>'Rwanda','Burundi'=>'Burundi'))
		->order_by('date','desc')
		->unset_export()
		->unset_print();
		$this->grocery_crud
		->set_relation('product_id','product','product_name')
		->set_relation('border_id','trade_point','trade_point_name',array('TYPE' => 'Border'));
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Border Data');
	}

	//Countries
	function countries()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('country')
		->set_subject('Country')
		->unset_export()
		->unset_print();
		$this->grocery_crud->unset_columns('id','No_of_markets','unit_of_measure(kgs)');
		$this->grocery_crud->unset_fields('id','No_of_markets','unit_of_measure(kgs)');
		$this->grocery_crud->field_type('publish', 'dropdown',array('0' => 'No', '1' => 'Yes'));
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Countries');
	}

	//users
	function users()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('user')
		->set_subject('Agent')
		->unset_export()
		->unset_print();
		$this->grocery_crud->columns('full_name','username','email','trade_point_id','publish');
		$this->grocery_crud->unset_fields('user_id','id','imei','category','created_by','created_on','updated_on','updated_by','active_status');
		$this->grocery_crud->display_as('web_access','Web Access');
		$this->grocery_crud->field_type('publish', 'dropdown',array('0' => 'No', '1' => 'Yes'));
		$this->grocery_crud->field_type('web_access', 'dropdown',array('0' => 'No', '1' => 'Yes'));
		$this->grocery_crud->set_relation('trade_point_id','trade_point','trade_point_name');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Agents');
	}

	//products
	function products()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('product')
		->set_subject('Product')
		->unset_export()
		->unset_print();
		$this->grocery_crud->unset_columns('id','product_id');
		$this->grocery_crud->unset_fields('id','product_id');
		$this->grocery_crud->field_type('publish', 'dropdown',array('0' => 'No', '1' => 'Yes'));
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Products');
	}

	//grades
	function grades()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('grade')
		->set_subject('Grade')
		->display_as('productId','Product Name')
		->unset_export()
		->unset_print()
		->unset_publish()
		->unset_texteditor('description');
		$this->grocery_crud->set_relation('productId','product','product_name');
		$this->grocery_crud->unset_columns('id');
		$this->grocery_crud->unset_fields('id');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Grades');
	}

	//market reports
	function field_reports()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('market_reports')
		->set_subject('Field Reports')
		->order_by('date','desc')
		->unset_export()
		->unset_print()
		->unset_add()
		->unset_edit()
		->unset_publish();
		$this->grocery_crud->unset_columns('id');
		$this->grocery_crud->unset_fields('id');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Field Reports');
	}

	//warehouse info
	function warehouse_info()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('warehouse_info')
		->set_subject('Warehouse Info')
		->display_as('tradepoint_Id','Warehouse Name')
		->field_type('publish','dropdown',array('0' => 'No', '1' => 'Yes'))
		->set_field_upload('photo','assets/uploads/')
		->unset_export()
		->unset_publish()
		->unset_print();
		$this->grocery_crud->set_relation('tradepoint_Id','trade_point','trade_point_name',array('TYPE' => 'Warehouse'));
		$this->grocery_crud->unset_columns('id','availableVolume','whouseType','photo','products','info_source','email','phone')
		->unset_fields('id','availableVolume','products','info_source');
		$this->grocery_crud->field_type('Ownership', 'dropdown',array('Private' => 'Private',
		'Government' => 'Government','NGO'=>'NGO','Cooperative'=>'Cooperative'));
		$this->grocery_crud->unset_edit_fields('tradepoint_Id');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Warehouse Info');
	}

	//warehouse info
	function subscribers()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('alert_subscribers')
		->set_subject('SMS Subscribers')
		->unset_publish()
		->unset_add()
		->unset_edit()
		->display_as('status','SMS Broadcasts')
		->field_type('status', 'dropdown',array('0' => 'No','1'=>'Yes'))
		->field_type('category', 'dropdown',array('0' => 'Student/Researcher','1'=>'Trader','2'=>'Farmer','3'=>'Policy maker/Government'))
		->unset_columns('id');

		$output = $this->grocery_crud->render();

		$this->_crud_output($output,'SMS Subscribers');
	}

	//market data
	function rfbs()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('rfbs_volume')
		->set_subject('RFBS Volume')
		->columns('product_id','country_id','volume','date','publish')
		->fields('product_id','country_id','volume','date','publish')
		->display_as('product_id','Product')
		->display_as('country_id','Country')
		->field_type('publish','dropdown',array('0' => 'No', '1' => 'Yes'))
		->order_by('date','desc')
		->unset_export()
		->unset_print();

		$this->grocery_crud
		->set_relation('product_id','product','product_name')
		->set_relation('country_id','country','Country_name');
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'RFBS Volume');
	}


	//batch processing
	function batch()
	{
		if($this->input->post())
		{
			$selection = $this->input->post('selection',TRUE);
			$table = $this->input->post('table',TRUE);
			$action = $this->input->post('action',TRUE);
			$pk = $this->input->post('pk',TRUE);

			//selection is split into an array
			$item_ids = explode('|',$selection);
			$count = 0;
			foreach($item_ids as $id)
			{
				if($id != '')
				{
					if($action=='delete')
					{
						$this->db->delete($table, array($pk=>$id));
						$count++;
					}
					elseif($action=='publish')
					{
						$this->db->update($table,array('publish'=>1),array($pk=>$id));
						$count++;
					}
					elseif($action=='unpublish')
					{
						$this->db->update($table,array('publish'=>0),array($pk=>$id));
						$count++;
					}
				}
			}
			echo '<div class="alert alert-sucess">
					Info: '.$count.' row(s) affected
				 </div>';
		}
	}

	function grid($id=1)
	{
		$this->load->model(array('product','country','warehouse','market'));
		$countries = $this->country->country_list($condition=FALSE);
		$product = $this->product->product_list();

		//Check for post data
		if($this->input->post('submit'))
		{
			$count = count($this->input->post('market'));
			$market = $this->input->post('market');
			$rp = $this->input->post('retail');
			$wp = $this->input->post('whole');
			$date = $this->input->post('date');

			$inserts = 0;
			for($i=0; $i<=$count-1; $i++)
			{
				//Check if data exists
				$exists = $this->market->check_exist($market[$i],$id,$date);
				if($exists === 0)
				{
					if(isset($rp[$i]) AND isset($wp[$i]) AND $rp[$i] != 0 AND $wp[$i] != 0)
					{
						$prices = array(
							'market_id'=>$market[$i],
							'product_id'=>$id,
							'retail_price'=>$rp[$i],
							'wholesale_price'=>$wp[$i],
							'date'=>$date
						);
						$this->market->add($prices);
						$inserts ++;
					}
				}
			}
			$msg = $inserts. ' record(s) added';
		}
		else
		{
			$msg = NULL;
		}

		$this->template->set_layout('layout5');
		$this->template->title('Ratin Admin | Data Grid');
		$this->template->append_metadata('<script src="'.base_url('resources/js/').'/zebra_datepicker.js"></script>');
		$this->template->build('pages/grid',array('countries'=>$countries,'products'=>$product,'id'=>$id,'msg'=>$msg));
	}


	//RFBS Grid
	public function rfbs_grid($id=1)
	{
		$this->load->model(array('product','country','warehouse','market'));
		$countries = $this->country->country_list($condition=FALSE);
		$product = $this->product->product_list();
		$msg = NULL;

		//Check for post data
		if($this->input->post('submit'))
		{
			$count = count($this->input->post('product'));
			$product = $this->input->post('product');
			$volume = $this->input->post('volume');
			$country = $this->input->post('country');
			$date = $this->input->post('date');

			$inserts = 0;
			for($i=0; $i<=$count-1; $i++)
			{
				//Check if data exists
				$exists = $this->market->rfbs_check_exist($country[$i],$product[$i],$date);
				if($exists === 0)
				{
					if(isset($volume[$i]) AND $volume[$i] != 0)
					{
						$prices = array(
							'product_id'=>$product[$i],
							'country_id'=>$country[$i],
							'volume'=>$volume[$i],
							'date'=>$date
						);
						$this->market->add_volume($prices);
						$inserts ++;
					}
				}
			}
			$msg = $inserts. ' record(s) added';
		}
		else
		{
			$msg = NULL;
		}

		$this->template->set_layout('layout5');
		$this->template->title('Ratin Admin | RFBS Grid');
		$this->template->append_metadata('<script src="'.base_url('resources/js/').'/zebra_datepicker.js"></script>');
		$this->template->build('pages/rfbs_grid',array('countries'=>$countries,'products'=>$product,'msg'=>$msg,'id'=>$id));
	}

	//email subscribers
	function email_subscribers()
	{
		$this->grocery_crud->set_theme('twitter-bootstrap');
		$this->grocery_crud->set_table('ratin_subscription')
		->set_subject('Email Subscribers')
		->order_by('name','asc')
		->field_type('subscribe','dropdown',array('0' => 'No', '1' => 'Yes'))
		->field_type('category','multiselect',array('1' => 'Market', '2' => 'Warehouse', '3'=>'Border'));
		$this->grocery_crud->unset_columns('id','confirm_id');
		$this->grocery_crud->unset_fields('id','confirm_id');
		$this->grocery_crud->unset_publish();
		$output = $this->grocery_crud->render();
		$this->_crud_output($output,'Email Subscribers');
	}

	// article time callback
	function article_create_callback($post_array) {
		date_default_timezone_set('Africa/Nairobi');
	  	$post_array['created_on'] = date('Y-m-d H:i:s');
	  	$post_array['created_by'] = $this->ion_auth->get_user_id();
	  	return $post_array;
	} 

	// market price callback
	function price_callback($post_array){
		$market = $post_array['market_id'];
		$this->db->select("convert_to_dollar_in($market) as rate");
		$query = $this->db->get();
        $rate = $query->row('rate');
		$post_array['retail_price'] = $post_array['retail_price'] / $rate;
		$post_array['wholesale_price'] = $post_array['wholesale_price'] / $rate;
		return $post_array;
	}

	// date time callback
	function datetime_callback($value,$row){
		return date('H:i:s',strtotime($value));
	}
}