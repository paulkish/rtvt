<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class miller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('millers_model'));
		$this->load->library(array('session','ion_auth','grocery_CRUD'));
        
        //Load db connection
        $this->db = $this->load->database('miller', TRUE);

		if(!$this->ion_auth->logged_in())
		{
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('admin/auth/login');
		}
    }

    public function index($id=1)
    {
    	//Products Menu
    	$products = $this->millers_model->get_list('product','id,product_name','object');
    	//Towns
    	$towns = $this->millers_model->get_list('town','id,town_name','object');
    	//Weights
    	$weights = $this->millers_model->get_list('weight','id,weight','object');
    	//Count
    	$count = 0;

    	//build grid
    	$output = array('products'=>$products,'towns'=>$towns,'weights'=>$weights,'id'=>$id);

    	//check if form submitted
    	if($this->input->post('submit'))
    	{
    		foreach($this->input->post() as $key => $value)
    		{
    			if($key != 'submit' && $key != 'date' && $key != 'brand' && $value != '' && !empty($value))
    			{
    				$data = explode('_',$key);
	    			$date = $this->input->post('date');
                    $brand = $this->input->post('brand');

	    			//build insert
	    			$sprices = array(
	    				'town_id'=>$data[0],
	    				'product_id'=>$id,
	    				'miller_id'=>$data[2],
	    				'weight_id'=>$data[1],
	    				'price'=>$value,
	    				'date'=>$date,
                        'brand_id'=>$brand
	    			);

	    			//insert
	    			$insert = $this->millers_model->add('miller_price',$sprices);
	    			if($insert)
	    				$count++; //count inserted records
    			}
    		}

    		//display flash message on success
    		$this->session->set_flashdata('info', '<div class="alert alert-success">'.$count.' record(s) added</div>');   
            //redirect
            redirect('admin/miller/index');
        }

    	//build layout
    	$this->template->set_layout('layout5');
    	$this->template->title('Ratin Admin | Millers Grid');
    	$this->template->append_metadata('<script src="'.base_url('resources/js/').'/zebra_datepicker.js"></script>');
		$this->template->build('pages/miller',$output);
    }


    //miller Prices CRUD
    public function prices()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('miller_price')
        ->set_subject('Miller Price')
        ->columns('town_id','product_id','brand_id','weight_id','miller_id','price','date')
        ->fields('town_id','product_id','brand_id','weight_id','miller_id','price','date')
        ->display_as('product_id','Product')
        ->display_as('brand_id','Grade')
        ->display_as('town_id','Town')
        ->display_as('weight_id','Weight')
        ->display_as('miller_id','Miller')
        ->order_by('date','desc')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //relations
        $this->grocery_crud
        ->set_relation('product_id','product','product_name')
        ->set_relation('brand_id','brand','brand_name')
        ->set_relation('town_id','town','town_name')
        ->set_relation('weight_id','weight','weight')
        ->set_relation('miller_id','miller','miller_name');

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | miller Prices');
        $this->template->build('pages/miller_manage',$output);
    }

    //Towns CRUD
    public function towns()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('town')
        ->set_subject('Town')
        ->columns('town_name')
        ->fields('town_name')
        ->display_as('town_name','Name')
        //->display_as('country_id','Country')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Towns');
        $this->template->build('pages/miller_manage',$output);
    }

    //Products CRUD
    public function products()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('product')
        ->set_subject('Product')
        ->columns('product_name')
        ->fields('product_name')
        ->display_as('product_name','Name')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Products');
        $this->template->build('pages/miller_manage',$output);
    }

    //Grade CRUD
    public function grades()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('brand')
        ->set_subject('Grades')
        ->columns('brand_name','product_id')
        ->fields('brand_name','product_id')
        ->display_as('brand_name','Grade')
        ->display_as('product_id','Product')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //relations
        $this->grocery_crud
        ->set_relation('product_id','product','product_name');

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Grades');
        $this->template->build('pages/miller_manage',$output);
    }

    //Weight CRUD
    public function weight()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('weight')
        ->set_subject('Weight')
        ->columns('weight')
        ->fields('weight')
        ->display_as('weight','Weight')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | Weight');
        $this->template->build('pages/miller_manage',$output);
    }

    //miller CRUD
    public function millers()
    {
        //theme
        $this->grocery_crud->set_theme('twitter-bootstrap'); 
        
        //build crud
        $this->grocery_crud
        ->set_table('miller')
        ->set_subject('miller')
        ->columns('miller_name','town_id')
        ->fields('miller_name','town_id')
        ->display_as('town_id','Town')
        ->unset_export()
        ->unset_publish()
        ->unset_print();

        //relations
        $this->grocery_crud
        ->set_relation('town_id','town','town_name');

        //output
        $output = $this->grocery_crud->render();
        $this->db = $this->load->database('localhost',true);
        $this->template->set_layout('layout3');
        $this->template->title('Ratin Admin | millers');
        $this->template->build('pages/miller_manage',$output);
    }
}
