<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model(array('codegen_model','export','product','chart','country','market'));
        $this->load->library(array('session','table','pagination','PHPExcel/IOFactory','ion_auth'));
        $this->load->helper(array('form','url','codegen_helper','html','conversion'));

        $this->template->set_layout('default'); //set layout to default

        $this->template->set_metadata('description',
    	'EAGC RATIN System is a quick way for farmers, traders and processors to get regional market information anywhere, any time, easily using mobile phones or computers RATIN was developed to provide members and stakeholders with improved early warning marketing and trade information, leading to more efficient and competitive transactions in food trade between surplus and deficit regions.
		RATIN provides real time, relevant and accurate information with regional coverage in 5 countries including Kenya, Uganda, Tanzania, Burundi and Rwanda. Plans are in place to expand the coverage of RATIN to all the 10 countries in the Eastern African region and further develop partnerships in the Southern, Western and Northern Africa regions for increased market coverage.'
		);
        
        $this->template->set_metadata('keywords','market prices,crossborder volumes,border volumes, regional grain news,kenya,uganda,tanzania,rwanda,burundi,south sudan,maize,beans,rice,wheat,sorghum,millet,East Africa Grain Trade, Agriculture Price Data, Food Balance Sheet, regional agricultural trade intelligence network, Grain cereals and pulses in Africa');
        $this->template->set_metadata('author','EAGC RATIN');
    }

    // landing page
	public function index()
	{
		$this->data['products_list'] = $this->product->product_list();
		$latest_date = $this->market->latest_date();
		
		// build array of products
		$products = $this->codegen_model->get('product','product_id,product_name',5,0,'array',FALSE,'product_id IN(1,2,3,4,5)'); 
		foreach ($products as $key => $product) {
			// home page stuff
			$this->data['markets'][$product['product_id']] = $this->codegen_model->get('daily_data','market_name,country,date,retail_Price,wholesale_Price',5,0,'array','date',"publish = 1 AND market_id IN(3,7,15,33,1758) AND product_id = ".$product['product_id']." AND date = '$latest_date'");     
	    	$this->data['markets_header'][$product['product_id']] = array('Market','Country','Date','Retail','Wholesale');
	    	$this->data['borders'][$product['product_id']] = $this->codegen_model->get('vw_crossborder','Source_Country,Destination_Country,trade_point_name,date,volume',5,0,'array','date','publish = 1 AND product_id = '.$product['product_id']);     
			$this->data['borders_header'][$product['product_id']] = array('Source','Destination','Border','Date','Volume (MT)');
		}

		$this->data['markets_marquee'] = $this->codegen_model->get_all('daily_data','market_name,country,date,product_name,retail_Price,wholesale_Price','array','date',"publish = 1 AND date = '$latest_date'"); 

	    $this->data['products'] = $products; // use in building tabs
	    $this->data['currencies'] = $this->codegen_model->get('country','Currency',10,0,'array',FALSE,'publish = 1'); 

	    $this->data['news'] = $this->codegen_model->get('articles','articles.id,title,description',2,0,'array','id','published = 1');

	    // home page chart prices for last month average
	    $product_id = $this->input->get('product_market') != NULL ? $this->input->get('product_market') : 1 ;
	    $country = $this->input->get('country') != NULL ? $this->input->get('country') : 'Kenya';
		
		$product_name = $this->product->productName($product_id); // product name

		// build array of country capitals
		$capitals = array();
		$capital_id = array('3'=>'Nairobi','7'=>'Dar es salaam','15'=>'Bujumbura','1758'=>'Kigali','33'=>'Kampala');
		foreach ($capital_id as $key => $value) {
			$where = "publish = 1 AND product_id = $product_id AND market_id = $key AND date BETWEEN current_date - interval 7 day AND current_date ";
			$capital[$value] = $this->codegen_model->get_all('daily_data','date,wholesale_Price as wholesale','array','date',$where,'date');
		}

		$this->data['market_chart'] = $this->chart->capital_price($capital,$product_name->product_name);


		$product_id = $this->input->get('product_border') != NULL ? $this->input->get('product_border') : 1 ;
	    $product_name = $this->product->productName($product_id);
	    $where = "publish = 1 AND product_id = $product_id AND date BETWEEN current_date - interval 7 day AND current_date ";
		$chart = $this->codegen_model->get_all('vw_crossborder','trade_point_name,sum(volume) as volume,date','array','date',$where,'border_id,date'); 
		// get data for each border into array
		$data = array();
		$count = 0;
		foreach ($chart as $value) {
			$data[$value['trade_point_name']][$count]['date'] = $value['date'];
			$data[$value['trade_point_name']][$count]['volume'] = $value['volume'];
			$count++;
		}
		$this->data['border_chart'] = $this->chart->sum_volume($data,$product_name->product_name);

	    $this->data['bulletins'] = $this->codegen_model->get('bulletins','bulletins.id,title,description,file',2,0,'array','id','published = 1'); 
	    $this->template->title('RATIN | Home');
		$this->template->build('site/index',$this->data);
	}

	// about page
	public function about($page)
	{
		$id = urldecode($page);

	    // home page stuff
	    $this->data['article'] = $this->codegen_model->record('articles','title,description,content',"id = $id AND category = 4");

	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | '.$this->data['article']->title);
	    $this->template->set_metadata('description',$this->data['article']->description);
		$this->template->build('site/about',$this->data);
	}

	// markets page - export of data
	public function market()
	{   
		$this->data['products'] = $this->product->product_list();
		$product = $this->input->get('product') != NULL ? $this->input->get('product') : 1 ;

		$where = 'date >= date_sub(now(),'.limit_data().') and publish = 1 and product_id ='.$product; // limit one week

		//paging
        $config['base_url'] = base_url().'site/market';
        $config['total_rows'] = $this->codegen_model->count_where('daily_data',$where);
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 3;	

        $this->pagination->initialize($config); 

		$this->data['results'] = $this->codegen_model->get('daily_data','market_name,product_name,country,date,retail_Price,wholesale_Price',$config['per_page'],$this->uri->segment(3),'array','date',$where);     
		$this->data['header'] = array('Market','Product','Country','Date','Retail','Wholesale');

		$this->data['currencies'] = $this->codegen_model->get('country','Currency',10,0,'array',FALSE,'publish = 1'); 
		// get date from first query
		$end_date = $this->data['results'][0]['date'];
		$start = end($this->data['results']);
		$start_date = $start['date'];
		$where .= " AND date between '$start_date' AND '$end_date' ";
		$chart = $this->codegen_model->get_all('daily_data','date,avg(retail_Price) as retail,avg(wholesale_Price) as wholesale','array','date',$where,'date');     
		$product_name = $this->product->productName($product);
		$this->data['chart'] = $this->chart->average_price($chart,$product_name->product_name);

		if($this->uri->segment(4) == 'export' || $this->uri->segment(3) == 'export'){
			if($this->uri->segment(5) == 'excel' || $this->uri->segment(4) == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Market Prices','All',TRUE);
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'market_prices'));
			}elseif($this->uri->segment(5) == 'pdf' || $this->uri->segment(4) == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Market Prices','All',TRUE);
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'market_prices'));
			}
		}else{
			$this->template->set_layout('default');
	    	$this->template->title('RATIN | Markets');
			$this->template->build('site/market',$this->data);
		}
	}

	// markets page - allow search/filtering and export of data
	public function market_search()
	{        
		$this->data['products'] = $this->product->product_list();
		$this->data['countries'] = $this->country->countrylist();  	

        $start = $this->input->get('start');
		$end = $this->input->get('end');
		$product = $this->input->get('product') != NULL ? $this->input->get('product') : 1 ;
		$country = $this->input->get('country');
		$export = $this->input->get('export');
		$market = $this->input->get('market');
		$avg = $this->input->get('avg');
       
	    $where = 'date >= date_sub(now(),'.limit_data().') and publish = 1'; // limit one week
	
		if($start != NULL && $end != NULL){
			$where .= " AND date BETWEEN '$start' AND '$end'";
		}
		if($product != NULL){
			$where .= " AND product_id = $product";
		}
		if($country != NULL){
			$where .= " AND country = '$country'";
		}
		if($market != NULL){
			$markets = implode(',',$market);
			$where .= " AND market_id IN ($markets)";
		}

		//paging
        $config['base_url'] = base_url().'site/market_search';
        $config['total_rows'] = $this->codegen_model->count_where('daily_data',$where);
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 3;	
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);

		$this->data['header'] = array('Market','Product','Country','Date','Retail','Wholesale');
		$this->data['rates'] = exchange_rate(); 
		$this->data['currencies'] = $this->codegen_model->get('country','Currency',10,0,'array',FALSE,'publish = 1'); 
		$this->data['results'] = $this->codegen_model->get('daily_data','market_name,product_name,country,date,retail_Price,wholesale_Price',$config['per_page'],$this->uri->segment(3),'array','date',$where);     
		
		// avg
		if($avg != NULL){
			switch ($avg) {
				case '1':
					$group = 'date,market_id';
					$date = 'date';
					$order = 'date';
					break;
				case '2':
					$group = 'YEAR(date),MONTH(date),WEEK(date),market_id';
					$date = ' DATE_FORMAT(date,"Week %U,%Y") date ';
					$order = 'DATE_FORMAT(date,"%Y-%m-%U")';
					break;
				case '3':
					$group = 'YEAR(date),MONTH(date),market_id';
					$date = ' DATE_FORMAT(date,"%b,%Y") date ';
					$order = 'DATE_FORMAT(date,"%Y-%m")';
					break;
				case '4':
					$group = 'YEAR(date),market_id';
					$date = ' DATE_FORMAT(date,"%Y") date ';
					$order = 'Year(date)';
					break;
				default:
					$group = 'date,market_id';
					$date = 'date';
					$order = 'date';
					break;
			}


			$config['total_rows'] = count($this->codegen_model->get_all('daily_data','id','array','date',$where,$group));
			$this->db->_protect_identifiers=false;
			$this->data['results'] = $this->codegen_model->get('daily_data','market_name,product_name,country,'.$date.',avg(nullif(retail_Price,0)) as retail_Price,avg(nullif(wholesale_Price,0)) as wholesale_Price',$config['per_page'],$this->uri->segment(3),'array',$order,$where,$group);
			$this->db->_protect_identifiers=true;
		}

		$this->data['avg'] = $avg; // pass this to view for conversion function

		// default
		if($start == NULL && $end == NULL)
			$where .= " AND date BETWEEN current_date - interval 30 day AND current_date";
		$chart = $this->codegen_model->get_all('daily_data','date,avg(retail_Price) as retail,avg(wholesale_Price) as wholesale','array','date',$where,'date');     
		$product_name = $this->product->productName($product);
		$this->data['chart'] = $this->chart->average_price($chart,$product_name->product_name);
		if($export){
			if($export == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Market Prices','All',TRUE,$avg);
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'market_prices'));
			}elseif($export == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Market Prices','All',TRUE,$avg);
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'market_prices'));
			}
		}else{
			$this->pagination->initialize($config); 
			$this->template->set_layout('default');
	    	$this->template->title('RATIN | Markets Search');
			$this->template->build('site/market_search',$this->data);
		}
	}

	// markets page - allow search/filtering and export of data
	public function page($country)
	{   
		$country = urldecode($country);
		$this->data['products'] = $this->product->product_list();
		$product = $this->input->get('product') != NULL ? $this->input->get('product') : 1 ;

	    $where = "date >= date_sub(now(),".limit_data().") and publish = 1 and country = '$country' and product_id = $product"; // limit one week
	  
		//paging
        $config['base_url'] = base_url().'site/page/'.$country;
        $config['total_rows'] = $this->codegen_model->count_where('daily_data',$where);
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 4;	
       	$config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		
        $this->pagination->initialize($config); 

		$this->data['results'] = $this->codegen_model->get('daily_data','market_name,product_name,date,retail_Price,wholesale_Price',$config['per_page'],$this->uri->segment(4),'array','date',$where);     
		$this->data['header'] = array('Market','Product','Date','Retail','Wholesale');

		$this->data['currencies'] = $this->codegen_model->get('country','Currency',10,0,'array',FALSE,'publish = 1'); 
		$this->data['country'] = $country;
		// get date from first query
		$end_date = $this->data['results'][0]['date'];
		$start = end($this->data['results']);
		$start_date = $start['date'];
		$where .= " AND date between '$start_date' AND '$end_date' ";
		$chart = $this->codegen_model->get_all('daily_data','date,avg(retail_Price) as retail,avg(wholesale_Price) as wholesale','array','date',$where,'date');     
		$product_name = $this->product->productName($product);
		$this->data['chart'] = $this->chart->average_price($chart,$product_name->product_name);
		if($this->uri->segment(4) == 'export' || $this->uri->segment(5) == 'export'){
			if($this->uri->segment(5) == 'excel' || $this->uri->segment(6) == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Market Prices','All',TRUE);
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'market_prices'));
			}elseif($this->uri->segment(5) == 'pdf' || $this->uri->segment(6) == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Market Prices','All',TRUE);
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'market_prices'));
			}
		}else{
			$this->template->set_layout('default');
	    	$this->template->title('RATIN | Country Page | '.$country);
			$this->template->build('site/country_page',$this->data);
		}
	}

	// border page
	public function border()
	{
		$this->data['products'] = $this->product->product_list();
		$product = $this->input->get('product') != NULL ? $this->input->get('product') : 1 ;

	    $where = "date >= date_sub(now(),".limit_data().") and publish = 1 and product_id = $product"; // limit one week

	    //paging
        $config['base_url'] = base_url().'site/border';
        $config['total_rows'] = $this->codegen_model->count_where('vw_crossborder',$where);
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 3;	
        $this->pagination->initialize($config);
       	
        //$where = "publish = 1 AND product_id = $product";
		$this->data['results'] = $this->codegen_model->get('vw_crossborder','Source_Country,Destination_Country,product_name,trade_point_name,volume,date',$config['per_page'],$this->uri->segment(3),'array','date',$where);     
		$this->data['header'] = array('Source Country','Destination Country','Product','Border','Volume (MT)','Date');
		
		$end_date = $this->data['results'][0]['date'];
		$start = end($this->data['results']);
		$start_date = $start['date'];
		$where .= " AND date between '$start_date' AND '$end_date' ";
		$chart = $this->codegen_model->get_all('vw_crossborder','trade_point_name,sum(volume) as volume,date','array','date',$where,'border_id,date'); 
		// get data for each border into array
		$data = array();
		$count = 0;
		foreach ($chart as $value) {
			$data[$value['trade_point_name']][$count]['date'] = $value['date'];
			$data[$value['trade_point_name']][$count]['volume'] = $value['volume'];
			$count++;
		}
		$product_name = $this->product->productName($product);
		$this->data['chart'] = $this->chart->sum_volume($data,$product_name->product_name);
		if($this->uri->segment(4) == 'export' || $this->uri->segment(3) == 'export'){
			if($this->uri->segment(5) == 'excel' || $this->uri->segment(4) == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Crossborder Volumes','All');
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'crossborder_volumes'));
			}elseif($this->uri->segment(5) == 'pdf' || $this->uri->segment(4) == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Crossborder Volumes','All');
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'crossborder_volumes'));
			}
		}else{
		    //Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Crossborder');
			$this->template->build('site/border',$this->data);
		}
	}

	// border page
	public function border_search()
	{	
		$this->data['products'] = $this->product->product_list();
		$this->data['countries'] = $this->country->countrylist();   	

        $start = $this->input->get('start');
		$end = $this->input->get('end');
		$product = $this->input->get('product') != NULL ? $this->input->get('product') : 1 ;
		$source_country = $this->input->get('source_country');
		$dest_country = $this->input->get('dest_country');
		$export = $this->input->get('export');
        $where = "date >= date_sub(now(),".limit_data().") and publish = 1 and product_id = $product";

		if($start != NULL && $end != NULL){
			$where .= " AND date BETWEEN '$start' AND '$end'";
		}
		if($source_country != NULL){
			$where .= " AND Source_Country = '$source_country'";
		}
		if($dest_country != NULL){
			$where .= " AND Destination_Country = '$dest_country'";
		}

		$config['base_url'] = base_url().'site/border_search';
        $config['total_rows'] = $this->codegen_model->count_where('vw_crossborder',$where);
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 3;	
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        $this->pagination->initialize($config); 

		$this->data['header'] = array('Source Country','Destination_Country','Product','Border','Volume (MT)','Date');
		$this->data['results'] = $this->codegen_model->get('vw_crossborder','Source_Country,Destination_Country,product_name,trade_point_name,volume,date',$config["per_page"],$this->uri->segment(3),'array','date',$where);     
		
		if($start == NULL && $end == NULL)
			$where .= " AND date BETWEEN current_date - interval 60 day AND current_date";
		$chart = $this->codegen_model->get_all('vw_crossborder','trade_point_name,sum(volume) as volume,date','array','date',$where,'border_id,date'); 
		// get data for each border into array
		$data = array();
		$count = 0;
		foreach ($chart as $value) {
			$data[$value['trade_point_name']][$count]['date'] = $value['date'];
			$data[$value['trade_point_name']][$count]['volume'] = $value['volume'];
			$count++;
		}
		$product_name = $this->product->productName($product);
		$this->data['chart'] = $this->chart->sum_volume($data,$product_name->product_name);

		if($export){
			if($export == 'excel'){	
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Crossborder Volumes','All');
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'crossborder_volumes'));
			}elseif($export == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Crossborder Volumes','All');
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'crossborder_volumes'));
			}
		}else{
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Crossborder Search');
			$this->template->build('site/border_search',$this->data);
		}
	}

	// supermarket page
	public function supermarket()
	{
		$this->load->model('smarket_model');

	    //paging
        $config['base_url'] = base_url().'site/supermarket';
        $config['total_rows'] = $this->smarket_model->count('vw_price');
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 3;	
        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->smarket_model->get('vw_price','town_name,product_name,supermarket_name,weight,price,date',$config['per_page'],$this->uri->segment(3),'array','date');     
		$this->data['header'] = array('Town','Product','Supermarket','Weight (Kgs)','Price','Date');
		if($this->uri->segment(4) == 'export' || $this->uri->segment(3) == 'export'){
			if($this->uri->segment(5) == 'excel' || $this->uri->segment(4) == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Supermarket Prices','All');
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'supermarket_prices'));
			}elseif($this->uri->segment(5) == 'pdf' || $this->uri->segment(4) == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Supermarket Prices','All');
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'supermarket_prices'));
			}
		}else{
			$this->load->database('localhost', TRUE);

		    //Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Supermarket');
			$this->template->build('site/supermarket',$this->data);
		}
	}

	// supermarket page
	public function supermarket_search()
	{
		$this->load->model('smarket_model');
		$start = $this->input->get('start');
		$end = $this->input->get('end');
		$export = $this->input->get('export');
        $where = '';
		if($start != NULL && $end != NULL){
			$where .= "date BETWEEN '$start' AND '$end'";
		}

		$this->data['results'] = $this->smarket_model->get('vw_price','town_name,product_name,supermarket_name,weight,price,date',10,0,'array','date',$where);     
		$this->data['header'] = array('Town','Product','Supermarket','Weight (Kgs)','Price','Date');
		if($export){
			if($export == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Supermarket Prices','All');
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'supermarket_prices'));
			}elseif($export == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Supermarket Prices','All');
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'supermarket_prices'));
			}
		}else{
			$this->load->database('localhost', TRUE);

		    //Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Supermarket Search');
			$this->template->build('site/supermarket_search',$this->data);
		}
	}

	// miller page
	public function miller()
	{
		$this->load->model('millers_model');

	    //paging
        $config['base_url'] = base_url().'site/supermarket';
        $config['total_rows'] = $this->millers_model->count('vw_price');
        $config["per_page"] = $this->input->cookie('per_page') ? $this->input->cookie('per_page') : 10;
        $config["uri_segment"] = 3;	
        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->millers_model->get('vw_price','town_name,product_name,miller_name,weight,price,date',$config['per_page'],$this->uri->segment(3),'array','date');     
		$this->data['header'] = array('Town','Product','Miller','Weight (Kgs)','Price','Date');
		if($this->uri->segment(4) == 'export' || $this->uri->segment(3) == 'export'){
			if($this->uri->segment(5) == 'excel' || $this->uri->segment(4) == 'excel'){
				$export = $this->export->excel($this->data['header'],$this->data['results'],'Miller Prices','All');
				$this->load->view('ajax/excel',array('data'=>$export,'name'=>'miller_prices'));
			}elseif($this->uri->segment(5) == 'pdf' || $this->uri->segment(4) == 'pdf'){
				$export = $this->export->pdf($this->data['header'],$this->data['results'],'Miller Prices','All');
				$this->load->view('ajax/pdf',array('data'=>$export,'name'=>'miller_prices'));
			}
		}else{
			$this->load->database('localhost', TRUE);
		    //Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Miller');
			$this->template->build('site/miller',$this->data);
		}
	}

	// news page
	public function news()
	{
		$tables = array(
            'users_ion'=>'articles.created_by=users_ion.id'
        );
	    $config['base_url'] = base_url().'site/news';
        $config['total_rows'] = $this->codegen_model->count_join('articles',$tables);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;	
        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->codegen_model->join_tables('articles','articles.id,title,description,username,articles.created_on',$config['per_page'],$this->uri->segment(3),$tables,'published = 1','articles.id'); 
		$this->data['categories'] = $this->codegen_model->get_list('categories','id,category');
	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | News');
	    $this->template->set_metadata('description','RATIN News');
		$this->template->build('site/news',$this->data);
	}

	// bulletin page
	public function bulletins()
	{
		$tables = array(
            'users_ion'=>'bulletins.created_by=users_ion.id'
        );
	    $config['base_url'] = base_url().'site/bulletins';
        $config['total_rows'] = $this->codegen_model->count_join('bulletins',$tables);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;	
        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->codegen_model->join_tables('bulletins','bulletins.id,title,description,username,bulletins.created_on,file',$config['per_page'],$this->uri->segment(3),$tables,'published = 1','bulletins.id'); 
	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | Bulletins');
		$this->template->build('site/bulletins',$this->data);
	}

	// news article page
	public function news_article($id)
	{
		$this->data['article'] = $this->codegen_model->record('articles','title,description,content,file,created_by,created_on',"id = $id");
	    $this->data['user'] = $this->codegen_model->record('users_ion','username',"id = ".$this->data['article']->created_by);
	    $this->data['categories'] = $this->codegen_model->get_list('categories','id,category');
	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | News | '.$this->data['article']->title);
	    $this->template->set_metadata('description',$this->data['article']->description);
		$this->template->build('site/news_article',$this->data);
	}

	// category page
	public function category($id)
	{
		$tables = array(
            'users_ion'=>'articles.created_by=users_ion.id'
        );
	    $config['base_url'] = base_url().'site/category/'.$id;
        $config['total_rows'] = $this->codegen_model->count_join('articles',$tables);
        $config["per_page"] = 5;
        $config["uri_segment"] = 4;	
        $this->pagination->initialize($config); 	

		$this->data['results'] = $this->codegen_model->join_tables('articles','articles.id,title,description,username,articles.created_on',$config['per_page'],$this->uri->segment(4),$tables,"published = 1 and articles.category = $id",'articles.id');
		$this->data['categories'] = $this->codegen_model->get_list('categories','id,category');
		$this->data['category'] = $this->codegen_model->record('categories','category',"id = $id");
	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | News | '.$this->data['category']->category);
	    $this->template->set_metadata('description','RATIN News '.$this->data['category']->category);
		$this->template->build('site/news',$this->data);
	}

	// contact page
	public function contact()
	{
	    // home page stuff

	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | Contact');
		$this->template->build('site/contact');
	}

	// map page
	public function map()
	{
	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | Certified Warehouses');
		$this->template->build('site/map');
	}

	// certified map page
	public function grain_storage()
	{
	    //Build view
	    $this->template->set_layout('default');
	    $this->template->title('RATIN | Grain Storage Facilities Within EAC');
		$this->template->build('site/grain_storage');
	}

	// signup page
	public function signup()
	{
		$this->load->helper('language');
		$this->load->library('form_validation');
		$this->lang->load('auth');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

	    //validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required|xss_clean');
		$this->form_validation->set_rules('category', $this->lang->line('create_user_validation_category_label'), 'required|xss_clean');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'phone'      => $this->input->post('phone'),
				'category'   => $this->input->post('category'),
			);
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data))
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("site/login", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'class' =>'form-control',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'class' =>'form-control',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'class' =>'form-control',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['phone'] = array(
				'name'  => 'phone',
				'id'    => 'phone',
				'type'  => 'text',
				'class' =>'form-control',
				'value' => $this->form_validation->set_value('phone'),
			);
			$this->data['category'] = array(
				'name'=>'category',
				'value' => $this->form_validation->set_value('category'),
			);
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'class' =>'form-control',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'class' =>'form-control',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			//Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Signup');
			$this->template->build('site/signup',$this->data);
		}
	}

	// login page
	public function login()
	{
		$this->load->helper('language');
		$this->load->library('form_validation');
		$this->lang->load('auth');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true){
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)){
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('site/index', 'refresh');
			} else {
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('site/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		} else {
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'class'=>'form-control',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'class'=>'form-control',
				'type' => 'password',
			);

			//Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Login');
			$this->template->build('site/login',$this->data);
		}
	}

	//log the user out
	public function logout()
	{
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('site/login', 'refresh');
	}

	//forgot password
	public function forgot()
	{
		$this->load->helper('language');
		$this->load->library('form_validation');
		$this->lang->load('auth');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required');
		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
				'class'=>'form-control'
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			} else {
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//Build view
		    $this->template->set_layout('default');
		    $this->template->title('RATIN | Forgot Password');
			$this->template->build('site/forgot',$this->data);
		} else {
			// get identity for that email
			$config_tables = $this->config->item('tables', 'ion_auth');
			$identity = $this->db->where('email', $this->input->post('email'))->limit('1')->get($config_tables['users'])->row();

			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten){
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("site/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("site/forgot", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset($code = NULL)
	{
		if (!$code){
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user){
			$this->load->helper('language');
			$this->load->library('form_validation');
			$this->lang->load('auth');
			$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false) {
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'class'=> 'form-control',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'class'=> 'form-control',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
		    	$this->template->title('RATIN | Reset Password');
				$this->template->build('site/reset',$this->data);
			} else {
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')){
					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);
					show_error($this->lang->line('error_csrf'));
				} else {
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change){
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						$this->logout();
					} else {
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('site/reset/' . $code, 'refresh');
					}
				}
			}
		} else {
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("site/forgot", 'refresh');
		}
	}

	//activate the user
	public function activate($id, $code=false)
	{
		if ($code !== false){
			$activation = $this->ion_auth->activate($id, $code);
		} else if ($this->ion_auth->is_admin()){
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation){
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("site/login", 'refresh');
		} else {
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("site/forgot", 'refresh');
		}
	}

	public function subscribe(){
		$this->load->model('subscribers');
        if($this->input->post())
        {
           	$email = $this->input->post('email',TRUE);
           	$subscribe = $this->input->post('subscribe',TRUE);
           	if($this->input->post('category'))
           		$category = implode(',',$this->input->post('category',TRUE));
           	else
           		$category = NULL;

           	if($subscribe==1)
           	{
                //Check if email exists
                $check = $this->subscribers->checkemail($email);
                if(!$check){
                    //Generate random number
                    $this->load->helper('string');
                    $this->load->library('email');
                    $rand_string = random_string('alnum', 16);
                    $rand = sha1($rand_string);

                    $data = array('email'=>$email,'confirm_id'=>$rand,'category'=>$category);
                    $data = $this->subscribers->add($data);

                    if($data){

                        $email_id = $this->subscribers->findEmail($email);
                        $id = $email_id->id;
                        $msg = 'You have received this email because you subscribed to receive updates from RATIN.';
                        $msg .= 'To complete your subscription please click on this link. <br><br><a href="'.base_url('site/confirm_subscription/'.$id.'/'.$rand).'">Confirm Email Subscription</a>';
                        //Build mail template
                        $this->template->set_layout('layout2');
                        $this->template->inject_partial('title','Confirm Subscription');
                        $data = $this->template->build('ajax/data',array('data'=>$msg),true);
                        //Send confimamail
                        $this->email->set_mailtype("html");
                        $this->email->to($email);
                        $this->email->from('no_reply@ratin.net', 'Subscription Confirmation');
                        $this->email->subject('Subscription Confirmation');
                        $this->email->message($data);
                        $this->email->send();

                        //Status infomation
                        $this->load->view('ajax/data',array('data'=>"You have been successfully subscribed.
                        An email has been sent to $email with instructions on how to complete the subscription.
                        If the email is not visible in your inbox please check your spam box and mark as not spam."));
                    }
                }
                else{
                	$data = array('category'=>$category);
                    $data = $this->subscribers->edit($email,$data);
                    $this->load->view('ajax/data',array('data'=>'Your subscription preferences have been saved'));
                }
           }
           elseif($subscribe==0){
               //Check if email exists
               $check = $this->subscribers->checkemail($email);
               if($check){
                   $data = array('subscribe'=>0);
                   $data = $this->subscribers->edit($email,$data);
                   if($data){
                   $this->load->view('ajax/data',array('data'=>'You have been successfully unsubscribed'));
                   }
               }
               else{
                   $this->load->view('ajax/data',array('data'=>'This email address does not exist in the system'));
               }
           }

        }
    }

    public function confirm_subscription($email,$id){
    	$this->load->model('subscribers');
    	$this->template->set_layout('default');
	    $this->template->title('Confirm Subscription');
        $check = $this->subscribers->checkemailId($email);
        if($check){
            $confirm = $this->subscribers->findEmailId($email);
            if($confirm->confirm_id === $id)
            {
                $update = $this->subscribers->updateEmail($email);
                if($update){
                    $this->template->build('ajax/data',array('data'=>'Your subscription is complete'));
                }
                else{
                    $this->template->build('ajax/data',array('data'=>'Error1'.$id.$email));
                }
            }
            else{
                $this->template->build('ajax/data',array('data'=>'Error2'.$id));
            }
        }
        else{
           $this->template->build('ajax/data',array('data'=>'Error3'.$email));
        }
    }

    public function market_list(){
    	$this->load->model(array('market'));
    	$country = $this->input->get('country');
    	$markets = $this->market->market_list(NULL,$country);  
    	echo json_encode($markets);
    }

	// csfr nonce
	protected function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	// valid nonce
	protected function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
/* End of file site.php */
/* Location: ./application/controllers/site.php */