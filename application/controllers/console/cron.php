<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('codegen_model');
        $this->load->library('rest');
    }
    
    // get latest rates from exchangerates.co.ke
    public function rates($date = NULL){
    	if($date === NULL){
    		$date = date('Y-m-d');
    	}
    	// check if data for today exists
    	$exists = $this->codegen_model->record('exchange_rates','id',"date = '$date'");
    	if(!$exists){
			$config = array(
				'server' => "http://exchangerates.co.ke/api/$date/key/"
	        );

			// Rest config
			$this->rest->initialize($config);

			// Get latest rate
			$rates = $this->rest->get('qcHBkjqObEkwVeyJKmOf');

			// check if status is 200
			if($this->rest->status() == 200){
				// convert rates to rate vs dollar as is they exist as rates vs kes
				foreach ($rates->msg->rates as $key => $value) {
					if($value->currency == 'USD'){
						// base 
						$base_rate = $value->selling; // rate is KES/USD
						break;
					}
				}

				foreach ($rates->msg->rates as $key => $value) {
					$conversion = ($base_rate / $value->selling);
					switch ($value->currency) {
						case 'RWF':
							$rwf = $conversion;
							break;
						case 'BIF':
							$bif = $conversion;
							break;
						case 'TZS':
							$tzs = $conversion;
							break;
						case 'UGX':
							$ugx = $conversion;
							break;
						default:
							break;
					}
				}

				$data = array(
					'kshusd'=>$base_rate,
					'tshusd'=>$tzs,
					'ugxusd'=>$ugx,
					'bifusd'=>$bif,
					'rwfusd'=>$rwf,
					'date'=>$date
				); 

				// write to db rate
				$this->codegen_model->add('exchange_rates',$data);
			}
		}
    }
}