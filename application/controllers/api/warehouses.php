<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Warehouses extends REST_Controller
{
    //Access and Rate Limiting
    protected $methods = array(
        'list_get' => array('level' => 1,'limit' => 30),
        'daily_get' => array('level' => 1, 'limit' => 20),
        'search_get' => array('level' => 10, 'limit' => 10),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','warehouse'));
    }

    /**
    * Warehouse List
    */
    public function list_get()
    {
        $warehouse = $this->get('id');
        $country = $this->get('country');
        $list = $this->warehouse->warehouses_list($warehouse,$country);
        $this->response($list, 200);
    }

    /**
    * Warehouse Prices for the Day
    */
    public function daily_get()
    {
        $warehouse = $this->get('id');
        $grade = $this->get('grade');
        $list = $this->warehouse->daily_volume($warehouse,$grade);
        $this->response($list, 200);
    }

    /**
    * Warehouse Prices Search
    */
    public function search_get()
    {
        $warehouse = $this->get('id');
        $grade = $this->get('grade');
        $start = $this->get('start');
        $end = $this->get('end');
        $list = $this->warehouse->search_volume($warehouse,$grade,$start,$end);
        $this->response($list, 200);
    }
}