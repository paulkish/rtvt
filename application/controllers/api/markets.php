<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Markets extends REST_Controller
{
    protected $methods = array(
        'list_get' => array('level' => 10,'limit' => 1),
        'daily_get' => array('level' => 1, 'limit' => 20),
        'search_get' => array('level' => 10, 'limit' => 10),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','market'));
    }

	/**
    * Market List
    */
    public function list_get()
    {
        $market = $this->get('id');
        $country = $this->get('country');
        $list = $this->market->market_list($market,$country);
        $this->response($list, 200);
    }

    /**
    * Market Prices for the Day
    */
    public function daily_get()
    {
        $market = $this->get('market');
        $product = $this->get('product');
        $list = $this->market->daily_prices($market,$product);
        $this->response($list, 200);
    }

    /**
    * Market Prices Search
    */
    public function search_get()
    {
        $market = $this->get('market');
        $product = $this->get('product');
        $start = $this->get('start');
        $end = $this->get('end');
        $list = $this->market->search_prices($market,$product,$start,$end);
        $this->response($list, 200);
    }
}