<?php defined('BASEPATH') OR exit('No direct script access allowed');

//This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Countries extends REST_Controller
{
    //Access and Rate Limiting
    protected $methods = array(
        'list_get' => array('level' => 1,'limit' => 30),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('country'));
    }

    /**
    * Product List
    */
    public function list_get()
    {
        $country = $this->get('id');
        $list = $this->country->countrylist($country);
        $this->response($list, 200);
    }
}