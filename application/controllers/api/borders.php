<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Borders extends REST_Controller
{
    //Access and Rate Limiting
    protected $methods = array(
        'list_get' => array('level' => 1,'limit' => 30),
        'daily_get' => array('level' => 1, 'limit' => 20),
        'search_get' => array('level' => 10, 'limit' => 10),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','border'));
    }

    /**
    * Border List
    */
    public function list_get()
    {
        $border = $this->get('id');
        $list = $this->border->border_list($border);
        $this->response($list, 200);
    }

    /**
    * Border Prices for the Day
    */
    public function daily_get()
    {
        $border = $this->get('border');
        $product = $this->get('product');
        $list = $this->border->daily_volume($border,$product);
        $this->response($list, 200);
    }

    /**
    * Border Prices Search
    */
    public function search_get()
    {
        $border = $this->get('border');
        $product = $this->get('product');
        $start = $this->get('start');
        $end = $this->get('end');
        $list = $this->border->search_volume($border,$product,$start,$end);
        $this->response($list, 200);
    }
}