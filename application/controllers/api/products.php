<?php defined('BASEPATH') OR exit('No direct script access allowed');

//This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Products extends REST_Controller
{
    //Access and Rate Limiting
    protected $methods = array(
        'list_get' => array('level' => 1,'limit' => 30),
        'grade_get' => array('level' => 1, 'limit' => 30),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product'));
    }

    /**
    * Product List
    */
    public function list_get()
    {
        $product = $this->get('id');
        $list = $this->product->product_list($product);
        $this->response($list, 200);
    }

    /**
    * Product List
    */
    public function grade_get()
    {
        $product = $this->get('id');
        $list = $this->product->grade_list($product);
        $this->response($list, 200);
    }
}