<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','country','warehouse','subscribers','auth','volume','market'));
        $this->load->library('session');
    }

	public function index()
	{
	    //Landing page data
	    $product = $this->product->product_list();
	    $country = $this->country->country_list();
	    $marquee = $this->warehouse->all_warehouse_data();
	   // $ip = $this->input->ip_address();
	    $detected_country = 'Kenya';

	    //Build view
	    $this->template->set_layout('layout1');
	    $this->template->title('Real Time Volume Tracking');
	    $this->template->inject_partial('help',
	    '<a href="'.base_url('rtvt/site/help/site').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal"
	    data-target="#myModal">USER GUIDE</a>');
	    $this->template->prepend_metadata('<script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/jquery.gomap-1.3.2.min.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/map.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/highcharts.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/exporting.js"></script>');
		$this->template->build('pages/site',array('product'=>$product,'country'=>$country,'marquee'=>$marquee,'detected_country'=>$detected_country));
	}

	public function map()
	{
		$product = $this->product->product_list();
	    $country = $this->country->country_list();

	    //Build view
	    $this->template->set_layout('layout1');
	    $this->template->title('Real Time Volume Tracking');
	    $this->template->inject_partial('help',
	    '<a href="'.base_url('rtvt/site/help/site').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal"
	    data-target="#myModal">USER GUIDE</a>');
	    $this->template->prepend_metadata('<script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/jquery.gomap-1.3.2.min.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/map.js"></script>');
	    $this->template->build('pages/map',array('product'=>$product,'country'=>$country));
	}

	public function json()
	{
	    if($this->input->post())
	    {
	        $country = $this->input->post('country');
            $product = $this->input->post('product');
	        //$capacity = $this->input->post('capacity');
            $category = $this->input->post('categories');
	        $search = $this->warehouse->warehouse_search($country,$product,NULL,$category);
	    }
	    else
	    {
		    $search = $this->warehouse->warehouse_list();
		}
		$warehouse_json = array(
		        'positions'=>$search
		);
		$data = json_encode($warehouse_json);
	    $this->load->view('ajax/json',array('data'=>$data));
	}

	public function ajax($id)
	{
	    $this->load->model('warehouse');
	    $info = $this->warehouse->warehouse_info($id);
	    $time = $this->warehouse->warehouse_update($id);
	    $volume = $this->warehouse->daily_volumes($id);
		$this->load->view('ajax/map',array('info'=>$info,'time'=>$time,'id'=>$id,'volume'=>$volume));
	}

    public function warehouse_info($id)
	{
	    $info = $this->warehouse->warehouse_info($id);
	    $vols = $this->warehouse->daily_volumes($id);
	    $table_vols = $this->warehouse->warehouse_vols($id);
	     //Build Page
	    $this->template->set_layout('layout1');
	    $this->template->title($info->warehouse_name);
	    $this->template->inject_partial('help',
	    '<a href="'.base_url('rtvt/site/help/warehouse').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal"
	    data-target="#myModal">USER GUIDE</a>');
	    //$this->template->append_metadata('<script src="'.base_url('resources/js').'/jquery.dataTables.min.js"></script>');
	    //$this->template->append_metadata('<script src="'.base_url('resources/js').'/dt_bootstrap.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/highcharts.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/exporting.js"></script>');
        $this->template->append_metadata('<script src="'.base_url('resources/js/tabletools/js').'/TableTools.min.js"></script>');
		$this->template->build('pages/warehouse',array('info'=>$info,'vols'=>$vols,'table_vols'=>$table_vols));
	}

	public function warehouse_json($id)
	{
         $data = $this->warehouse->json($id);
         $this->load->view('ajax/json',array('data'=>$data));
	}

	public function product_info($name)
	{
		$id = urldecode($name);
	    $data = $this->warehouse->warehouse_product($id);
	    $this->template->set_layout('layout1');
	    $this->template->title($id);
	    $this->template->inject_partial('help',
	    '<a href="'.base_url('rtvt/site/help/product').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal"
	    data-target="#myModal">USER GUIDE</a>');
	    //$this->template->append_metadata('<script src="'.base_url('resources/js').'/jquery.dataTables.min.js"></script>');
	    //$this->template->append_metadata('<script src="'.base_url('resources/js').'/dt_bootstrap.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/highcharts.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/exporting.js"></script>');
        $this->template->append_metadata('<script src="'.base_url('resources/js/tabletools/js').'/TableTools.min.js"></script>');
		$this->template->build('pages/product',array('data'=>$data,'name'=>$id));
	}

    public function product_json($name)
	{
        $data = $this->warehouse->warehouse_product($name);
        $this->load->view('ajax/json',array('data'=>$data));
	}

	public function storage()
	{
	    $this->template->set_layout('layout1');
	    $this->template->title('RTVT Facilities');
	    $warehouse = $this->warehouse->warehouse_link();
	    $this->template->inject_partial('help',
	    '<a href="'.base_url('rtvt/site/help/warehouse').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal"
	    data-target="#myModal">USER GUIDE</a>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js/tabletools/js').'/TableTools.min.js"></script>');
		$this->template->build('pages/warehouse_list',array('data'=>$warehouse));
	}

	public function product()
	{
	    $this->template->set_layout('layout1');
	    $this->template->title('RTVT Product Volumes');
	    $product = $this->product->product_list();
	    $country = $this->country->country_list();
        $this->template->inject_partial('help','<a href="'.base_url('rtvt/site/help/product').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal" data-target="#myModal">USER GUIDE</a>');
		$this->template->build('pages/product_list',array('product'=>$product,'country'=>$country));
	}

	public function search()
	{
	    $this->template->set_layout('layout1');
	    $this->template->title('RTVT Facilities');
	    $product = $this->product->product_list();
	    $country = $this->country->country_list();
	    $this->template->inject_partial('help',
	    '<a href="'.base_url('rtvt/site/help/search').'" role="button"
	    class="btn btn-small btn-success" data-toggle="modal"
	    data-target="#myModal">USER GUIDE</a>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js/tabletools/js').'/TableTools.min.js"></script>');
        $this->template->append_metadata('<script src="'.base_url('resources/js/').'/zebra_datepicker.js"></script>');
		$this->template->build('pages/search',array('product'=>$product,'country'=>$country));
	}

	public function chart($id)
	{
	    $data = $this->country->country_volumes($id);
	    $msg = 'Cumulative volume in RTVT storage facilities';
	    $this->load->view('ajax/chart',array('data'=>$data,'msg'=>$msg));
	}

	public function search_results()
	{
	    if($this->input->post())
	    {
	        $product = $this->input->post('product');
	        $country = $this->input->post('country');
	        $capacity = $this->input->post('capacity');
	        $category = $this->input->post('category');
            $start = $this->input->post('start');
            $end = $this->input->post('end');
	        $search = $this->warehouse->warehouse__search($country,$product,$capacity,$category,$start,$end);

	        $tmpl = array ( 'table_open'  => '<table class="plain table table-condensed table-bordered table-striped volumes">' );
	        $this->table->set_template($tmpl);
	        $this->table->set_heading('Warehouse Name','Country','Capacity(MT)','Ownership','Products');
	        foreach($search as $t)
	        {
	            $this->table->add_row(anchor('rtvt/site/warehouse_info/'.$t->i,$t->n),$t->country,$t->c,$t->o,
                '<table><tr><th>Product</th><th>Grade</th><th>Volume</th><th>Date</th></tr>'.$t->product.'</table>');
	        }
	        $data = $this->table->generate();
	    }
	    else
	    {
	        $data = 'Nothing to do here';
	    }
	    $this->load->view('ajax/data',array('data'=>$data));

	}

	public function product_chart($id,$country=NULL)
	{
		$id = urldecode($id);
	    $product = $this->product->product_volumes($id,$country);
	    $msg = ucfirst($id).' product volumes';
	    $this->load->view('ajax/chart',array('data'=>$product,'msg'=>$msg));
	}

	public function warehouse_chart($id)
	{
	    $warehouse = $this->warehouse->warehouse_volumes($id);
	    $msg = 'Warehouse volumes';
	    $this->load->view('ajax/chart',array('data'=>$warehouse,'msg'=>$msg));
	}

	public function help($page)
	{
	    $this->load->view('help/'.$page);
	}

    public function subscribe()
    {
        $this->load->view('pages/subscribe');
    }

    public function iframe_subscribe()
    {
    	$this->template->set_layout('layout6');
    	$this->template->build('pages/subscribe1');
    }

    public function register(){
        if($this->input->post())
        {
           	$name = $this->input->post('name',TRUE);
           	$email = $this->input->post('email',TRUE);
           	$subscribe = $this->input->post('subscribe',TRUE);
           	if($this->input->post('category'))
           		$category = implode(',',$this->input->post('category',TRUE));
           	else
           		$category = NULL;

           	if($subscribe==1)
           	{
                //Check if email exists
                $check = $this->subscribers->checkemail($email);
                if(!$check){
                    //Generate random number
                    $this->load->helper('string');
                    $this->load->library('email');
                    $rand_string = random_string('alnum', 16);
                    $rand = sha1($rand_string);

                    $data = array('name'=>$name,'email'=>$email,'confirm_id'=>$rand,'category'=>$category);
                    $data = $this->subscribers->add($data);

                    if($data){

                        $email_id = $this->subscribers->findEmail($email);
                        $id = $email_id->id;
                        $msg = $name.', <br><br><hr> You have received this email because you subscribed to receive updates from RTVT.';
                        $msg .= 'To complete your subscription please click on this link. <br><br><hr><a href="'.base_url('rtvt/site/confirm/'.$id.'/'.$rand).'">Confirm Email Subscription</a>';
                        //Build mail template
                        $this->template->set_layout('layout2');
                        $this->template->inject_partial('title','Confirm Subscription');
                        $data = $this->template->build('ajax/data',array('data'=>$msg),true);
                        //Send confimamail
                        $this->email->set_mailtype("html");
                        $this->email->to($email);
                        $this->email->from('no_reply@ratin.net', 'Subscription Confirmation');
                        $this->email->subject('Subscription Confirmation');
                        $this->email->message($data);
                        $this->email->send();

                        //Status infomation
                        $this->load->view('ajax/data',array('data'=>"You have been successfully subscribed.
                        An email has been sent to $email with instructions on how to complete the subscription.
                        If the email is not visible in your inbox please check your spam box and mark as not spam."));
                    }
                }
                else{
                	$data = array('category'=>$category);
                    $data = $this->subscribers->edit($email,$data);
                    $this->load->view('ajax/data',array('data'=>'Your subscription preferences have been saved'));
                }
           }
           elseif($subscribe==0){
               //Check if email exists
               $check = $this->subscribers->checkemail($email);
               if($check){
                   $data = array('subscribe'=>0);
                   $data = $this->subscribers->edit($email,$data);
                   if($data){
                   $this->load->view('ajax/data',array('data'=>'You have been successfully unsubscribed'));
                   }
               }
               else{
                   $this->load->view('ajax/data',array('data'=>'This email address does not exist in the system'));
               }
           }

        }
    }

    public function confirm($email,$id){
    	$this->template->set_layout('layout1');
	    $this->template->title('Confirm Subscription');
        $check = $this->subscribers->checkemailId($email);
        if($check){
            $confirm = $this->subscribers->findEmailId($email);
            if($confirm->confirm_id === $id)
            {
                $update = $this->subscribers->updateEmail($email);
                if($update){
                    $this->template->build('ajax/data',array('data'=>'Your subscription is complete'));
                }
                else{
                    $this->template->build('ajax/data',array('data'=>'Error1'.$id.$email));
                }
            }
            else{
                $this->template->build('ajax/data',array('data'=>'Error2'.$id));
            }
        }
        else{
           $this->template->build('ajax/data',array('data'=>'Error3'.$email));
        }
    }

    public function login()
	{
        $data = $this->session->all_userdata();
        if(!isset($data['logged_in']))
		{
            $this->template->set_layout('layout1');
            $this->template->title('RTVT Login');
            $this->template->inject_partial('help','<a href="'.base_url('rtvt/site/help/login').'" role="button"
            class="btn btn-small btn-success" data-toggle="modal" data-target="#myModal">USER GUIDE</a>');
            $this->template->build('pages/login');
        }
        else
		{
			//Check if market or warehouse
			if(isset($data['warehouse']))
			{
				redirect('/rtvt/site/entry', 'refresh');
			}
			else
			{
				redirect('/rtvt/site/marketentry', 'refresh');
			}
        }
    }

    public function checklogin()
	{
        if($this->input->post())
		{
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //Check if exist if exist
            $login = $this->auth->checkExists($username,$password);
            if($login)
			{
               $tradepoint = $this->auth->getTradePoint($username);
			   $tradepoint_type = $this->auth->getTradePointType($tradepoint);
			   if($tradepoint_type == 'Warehouse')
				{
					//Start warehouse session
					$userdata = array(
					   'username'  => $username,
					   'warehouse' => $tradepoint,
					   'logged_in' => TRUE
					);
					$this->session->set_userdata($userdata);
					redirect('/rtvt/site/entry', 'refresh');
				}
				elseif($tradepoint_type == 'Market')
				{
					//Start warehouse session
					$userdata = array(
					   'username'  => $username,
					   'market' => $tradepoint,
					   'logged_in' => TRUE
					);
					$this->session->set_userdata($userdata);
					redirect('/rtvt/site/marketentry', 'refresh');
				}
            }
            else{
                $this->template->set_layout('layout1');
                $this->template->title('RTVT Error');
                $this->template->inject_partial('help','<a href="'.base_url('rtvt/site/help/login').'" role="button"
                class="btn btn-small btn-success" data-toggle="modal" data-target="#myModal">USER GUIDE</a>');
                $this->template->build('ajax/data',array('data'=>'Incorrect login information'));
            }
        }
        else{
            redirect('/rtvt/site/login', 'refresh');
        }
    }

    public function entry()
	{
    	//$this->output->enable_profiler(TRUE);
        $data = $this->session->all_userdata();
        if(isset($data['logged_in']))
		{
            if($data['logged_in'] === TRUE)
			{
                //Get warehouse information
                $warehouse= $this->warehouse->warehouse_info($data['warehouse']);
                $product = $this->product->product_list();
                $this->template->set_layout('layout1');
                $this->template->title('RTVT Data Entry');
                $this->template->inject_partial('help','<a href="'.base_url('rtvt/site/help/entry').'" role="button"
                class="btn btn-small btn-success" data-toggle="modal" data-target="#myModal">USER GUIDE</a>');
                $this->template->build('pages/entry',array('warehouse'=>$warehouse,'data'=>$data,'product'=>$product));

            }
            else
			{
                //Do something else
                redirect('/rtvt/site/login', 'refresh');
            }
        }
		else
		{
            redirect('/rtvt/site/login', 'refresh');
        }
    }

	public function marketentry()
	{
        $data = $this->session->all_userdata();
        if(isset($data['logged_in']))
		{
            if($data['logged_in'] === TRUE)
			{
                $market= $this->market->market_info($data['market']);
                $product = $this->product->product_list();
                $this->template->set_layout('layout1');
                $this->template->title('RTVT Data Entry');
                $this->template->inject_partial('help','<a href="'.base_url('rtvt/site/help/marketentry').'" role="button"
                class="btn btn-small btn-success" data-toggle="modal" data-target="#myModal">USER GUIDE</a>');
                $this->template->build('pages/marketentry',array('market'=>$market,'data'=>$data,'product'=>$product));
            }
            else
			{
                redirect('/rtvt/site/login', 'refresh');
            }
        }
		else
		{
            redirect('/rtvt/site/login', 'refresh');
        }
	}

    public function logout(){
        $this->session->sess_destroy();
        redirect('/rtvt/site/login', 'refresh');
    }

    public function grade(){
        if($this->input->post()){
            $product_id = $this->input->post('product',TRUE);
            $grade = $this->product->grade_list($product_id);
            $this->load->view('ajax/dropdown',array('grade'=>$grade));
        }
    }

    public function data_entry(){
        $data = $this->session->all_userdata();
        if(isset($data['logged_in']))
		{
            if($this->input->post())
			{
                $grade = $this->input->post('grade',TRUE);
                $product = $this->input->post('product',TRUE);
                $volume = $this->input->post('volume',TRUE);
                $direction = $this->input->post('direction',TRUE);
                $warehouse = $data['warehouse'];

                //Check so that output is not greater than
                if($direction == 'OUT')
                {
                	//Check last entry
                	$last_entry = $this->warehouse->checkLastEntry($warehouse,$grade);
                	if($volume > $last_entry->stock)
                	{
                		$this->load->view('ajax/data',array('data'=>'Error, you cannot remove more grain than what exists in the warehouse'));
                	}
                	else
                	{
                		$data = array('whouse_id'=>$warehouse,'grade_id'=>$grade,'direction'=>$direction,
                               'volume'=>$volume,'date'=>date('Y-m-d'),'time'=>date('H:i:s'));
		                $trade_vol = $this->volume->add($data);

		                if($trade_vol)
						{
		                    $product_name = $this->product->productName($product)->product_name;
		                    $grade_name = $this->product->gradeName($grade)->name;
		                    $this->load->view('ajax/data',array('data'=>'The entry of '.$volume.' MT of '.$product_name.'
		                        of '.$grade_name.' has been saved'));
		                }
		                else
						{
		                    $this->load->view('ajax/data',array('data'=>'Error, the data has not been saved'));
		                }
                	}
                }
                //If direction is IN check if warehouse has capacity
                else
                {
                	$capacity = $this->warehouse->checkCapacity($warehouse);
					$current_volume = $capacity->availableVolume + $volume;
                	if($current_volume > $capacity->Capacity)
                	{
                		$this->load->view('ajax/data',array('data'=>'Error, the incoming grain exceeds the warehouse capacity'));
                	}
                	else
                	{
                		$data = array('whouse_id'=>$warehouse,'grade_id'=>$grade,'direction'=>$direction,
                               'volume'=>$volume,'date'=>date('Y-m-d'),'time'=>date('H:i:s'));
		                $trade_vol = $this->volume->add($data);

		                if($trade_vol)
						{
		                    $product_name = $this->product->productName($product)->product_name;
		                    $grade_name = $this->product->gradeName($grade)->name;
		                    $this->load->view('ajax/data',array('data'=>'The entry of '.$volume.' MT of '.$product_name.'
		                        of '.$grade_name.' has been saved'));
		                }
		                else
						{
		                    $this->load->view('ajax/data',array('data'=>'Error, the data has not been saved'));
		                }
                	}
                }
            }
            else
			{
                redirect('/rtvt/site/entry', 'refresh');
            }
        }
        else
		{
            redirect('/rtvt/site/login', 'refresh');
        }
    }

	public function market_data_entry()
	{
		$data = $this->session->all_userdata();
        if(isset($data['logged_in']))
		{
            if($this->input->post())
			{
                $product = $this->input->post('product',TRUE);
                $retail = $this->input->post('retail',TRUE);
                $wholesale = $this->input->post('wholesale',TRUE);
                $market = $data['market'];

				//Check if data has been post

				$exists = $this->market->check_exist($market,$product,date('Y-m-d'));
				if($exists == 0)
				{
					$data = array(
						'market_id'=>$market,
						'retail_price'=>$retail,
						'wholesale_price'=>$wholesale,
						'product_id'=>$product,
						'date'=>date('Y-m-d')
					);

					//Save data
					$save = $this->market->add($data);
					if($save)
					{
						$this->load->view('ajax/data',array(
						'data'=>'The entry of retail price '.$retail.' and wholesale price '.$wholesale.
						' for '.$this->product->productName($product)->product_name.' has been saved. '));
					}
					else
					{
						$this->load->view('ajax/data',array('data'=>'The data could not be saved'));
					}
				}
				else
				{
					$this->load->view('ajax/data',array('data'=>'Todays market prices have for '.
					$this->product->productName($product)->product_name.
					' have already been submitted please try again tomorrow'));
				}
            }
            else
			{
                redirect('/rtvt/site/marketentry', 'refresh');
            }
        }
        else
		{
            redirect('/rtvt/site/login', 'refresh');
        }
	}

    public function rtvt()
    {
    	$this->template->set_layout('layout4');
	    $this->template->title('RTVT Product Volumes');
    	$product = $this->product->product_list();
	    $country = $this->country->country_list();
		$this->template->build('pages/rtvt',array('product'=>$product,'country'=>$country));
    }

    public function marquee()
    {
    	$this->template->set_layout('layout4');
	    $this->template->title('RTVT Product Volumes');
    	$product = $this->product->product_list();
	    $country = $this->country->country_list();
	    $marquee = $this->warehouse->all_warehouse_data();
	    $this->template->build('pages/marquee',array('product'=>$product,'country'=>$country,'marquee'=>$marquee));
    }

}
/* End of file site.php */
/* Location: ./application/controllers/site.php */