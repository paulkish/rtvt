<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->load->library('session');
    }
    
    public function index()
    {
        //Get Subscibers
        $this->template->set_layout('layout1');
        $this->template->title('Grain Standards');
        $this->template->build('pages/download');
    }

    public function pdf($file)
    {
        if(isset($file))
        {
            //Read the file's contents
            $data = file_get_contents('resources/docs/'.$file);
            if($data != FALSE)
            {
                force_download($file, $data);
            }
            else
            {
                echo 'File not found';
            }    
        }
        else
        {
            echo 'File not found';
        }
        
    }

}
    