<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Warehouse extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product_model','country','warehouse_model'));
    }
    
    public function info($id)
	{
	    $info = $this->warehouse->warehouse_info($id);
	     //Build Page
	    $this->template->set_layout('layout1');
	    $this->template->title($info->warehouse_name);
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/jquery.dataTables.min.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/dt_bootstrap.js"></script>');
		$this->template->build('pages/warehouse',array('info'=>$info));
	}
	
	public function json($id)
	{
         $data = $this->warehouse->json($id);  
         $this->load->view('ajax/json',array('data'=>$data));
	}
	

}
