<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','country','warehouse'));
    }
    
    public function info($name)
	{
	    $this->template->set_layout('layout1');
	    $this->template->title($name);
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/jquery.dataTables.min.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/dt_bootstrap.js"></script>');
		$this->template->build('pages/product');
	}
	
	public function json($name)
	{
        $data = $this->warehouse->warehouse_product($name);  
        $this->load->view('ajax/json',array('data'=>$data));
	}
}
