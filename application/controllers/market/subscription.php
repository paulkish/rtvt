<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscription extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','country','sms','market'));
    }

	public function register()
	{
		if($this->input->post())
		{
			$name = $this->input->post('name',TRUE);
			$phone = $this->input->post('phone',TRUE);
			$category = $this->input->post('category',TRUE);
			$status = $this->input->post('status',TRUE);

			$info = array(
				'name'=>$name,
				'phone_no'=>$phone,
				'category'=>$category,
				'status'=>$status
			);

			if($this->sms->check_exist($phone) == 0)
			{
				$add = $this->sms->add($info);
				if($add)
					$msg = 'You are now subscribed for the RATIN SMS Service';
				else
					$msg = 'A technical fault has occurred';
			}
			else
			{
				$edit = $this->sms->edit($info,$phone);
				if($edit)
					$msg = 'Your preferences have been saved';
				else
					$msg = 'A technical fault has occurred';
			}


			$this->load->view('ajax/data',array('data'=>$msg));
		}
		else
		{
			$this->template->set_layout('layout6');
    		$this->template->build('pages/sms');
		}
	}
}
