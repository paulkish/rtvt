<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','country','warehouse','subscribers','market','border'));
        $this->load->library('email');
    }

    public function warehouse()
    {
        //Get Subscibers
        $list = $this->subscribers->subscibers_list();
        $product = $this->product->product_list();

        //Get Email content
        $this->template->set_layout('layout2');
	    $this->template->title('RTVT Volumes');
        $this->template->inject_partial('check',' ');
        $this->template->inject_partial('newsletter','RTVT Newsletter');
        $this->template->inject_partial('title','RTVT Warehouse Volumes');
        $msg = $this->template->build('email/warehouse',array('product'=>$product,'type'=>'warehouse'),true);
        $this->email->set_mailtype("html");
        foreach ($list as $l)
        {
            $category = explode(',',$l->category);
            if(in_array(2, $category))
            {
                $this->email->clear();
                $this->email->to($l->email);
                $this->email->from('no_reply@ratin.net', 'Real Time Volume Tracking');
                $this->email->subject('RTVT Daily Summary | '.date('Y-m-d'));
                $this->email->message($msg);
                $this->email->send();
            }
        }
    }

    public function view($type='market'){
        $product = $this->product->product_list();
        $this->template->set_layout('layout2');
	    $this->template->title('RATIN Newsletters');
        if($type=='market')
        {
            $this->template->inject_partial('title','RATIN Market Prices for '.$this->market->latest_date());
            $msg = $this->template->build('email/market',array('product'=>$product,'date'=>$this->market->latest_date(),'type'=>'market'));
        }
        elseif($type=='warehouse')
        {
            $this->template->inject_partial('title','RTVT Warehouse Volumes');
            $msg = $this->template->build('email/warehouse',array('product'=>$product,'type'=>'warehouse'));
        }
        elseif ($type=="border")
        {
            $this->template->inject_partial('title','RATIN Informal Crossborder Trade Volumes for '.$this->border->latest_date());
            $msg = $this->template->build('email/border',array('product'=>$product,'date'=>$this->border->latest_date(),'type'=>'border'));
        }

    }

    public function market()
    {
        //Get Subscibers
        $list = $this->subscribers->subscibers_list();
        $product = $this->product->product_list();

        //Get Email content
        $this->template->set_layout('layout2');
        $this->template->title('RATIN Market Prices');
        $this->template->inject_partial('check',' ');
        $this->template->inject_partial('title','RATIN Market Prices for '.date('Y-m-d'));
        $msg = $this->template->build('email/market',array('product'=>$product,'date'=>date('Y-m-d'),'type'=>'market'),true);
        $this->email->set_mailtype("html");
        foreach ($list as $l)
        {
            $category = explode(',',$l->category);
            if(in_array(1, $category))
            {
                $this->email->clear();
                $this->email->to($l->email);
                $this->email->from('no_reply@ratin.net', 'RATIN Market Prices');
                $this->email->subject('RATIN Market Prices | '.date('Y-m-d'));
                $this->email->message($msg);
                $this->email->send();
            }
        }


    }

    public function border()
    {
        //Get Subscibers
        $list = $this->subscribers->subscibers_list();
        $product = $this->product->product_list();

        //Get Email content
        $this->template->set_layout('layout2');
        $this->template->title('RATIN Border Volumes');
        $this->template->inject_partial('check',' ');
        $this->template->inject_partial('title','RATIN Informal Crossborder Trade Volumes for '.date('Y-m-d'));
        $msg = $this->template->build('email/border',array('product'=>$product,'date'=>date('Y-m-d'),'type'=>'border'),true);
        $this->email->set_mailtype("html");
        foreach ($list as $l)
        {
            $category = explode(',',$l->category);
            if(in_array(3, $category))
            {
                $this->email->clear();
                $this->email->to($l->email);
                $this->email->from('no_reply@ratin.net', 'RATIN Border Volumes');
                $this->email->subject('RATIN Informal Trade Volumes | '.date('Y-m-d'));
                $this->email->message($msg);
                $this->email->send();
            }
        }
    }
}
