<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Millers extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('millers_model'));
        $this->load->library('session');
    }

    public function index($product=1)
    {
    	$products = $this->millers_model->get_list('product','id,product_name',$array='object');
    	$price = $this->millers_model->get_list_where('vw_price','town_name,miller_name,weight,price,date',
    			array('product_id'=>$product),$array='object');

    	//Build view
	    $this->template->set_layout('layout7');
	    $this->template->title('Miller Prices');
	    $this->template->build('pages/miller_price',array('price'=>$price,'products'=>$products));
    }
}
