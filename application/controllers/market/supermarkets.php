<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supermarkets extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('smarket_model'));
        $this->load->library('session');
    }

    public function index($product=1)
    {
    	$products = $this->smarket_model->get_list('product','id,product_name',$array='object');
    	$price = $this->smarket_model->get_list_where('vw_price','town_name,supermarket_name,weight,price,date',
    			array('product_id'=>$product),$array='object');

    	//Build view
	    $this->template->set_layout('layout7');
	    $this->template->title('Supermarket Prices');
	    $this->template->build('pages/supermarket_price',array('price'=>$price,'products'=>$products));
    }
}
