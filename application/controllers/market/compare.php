<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compare extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('product','country','warehouse','subscribers','auth','volume','market'));
        $this->load->library(array('session','highcharts'));
    }

    public function index()
    {
    	//if form submitted
    	if($this->input->post('country') && $this->input->post('product'))
    	{
    		$product = $this->input->post('product',TRUE);
    		$country = $this->input->post('country',TRUE);
    		$start = $this->input->post('start',TRUE);
    		$end = $this->input->post('end',TRUE);
    	}
    	else
    	{
    		$product = 'Maize';
    		$country = 'Kenya';
  			$start = date('Y-m-d',strtotime('-12 month',time()));
    		$end = date('Y-m-d');
    	}

    	//prices and supply
    	$volumes = $this->market->rfbs_monthly_sum($product,$country,$start,$end);
    	$prices = $this->market->market_monthly_sum($product,$country,$start,$end);


    	//Chart
    	$chart = new Highcharts;
		$chart->chart->renderTo = "container";
		$chart->title->text = ucfirst("Average $product Wholesale Prices vs National Availability Volumes in $country");
		$chart->xAxis->type = "datetime";
		$chart->chart->zoomType = "x";
		$leftYaxis = new HighchartOption();

		$leftYaxis->labels->formatter = new HighchartJsExpr("function() {return this.value +'USD'; }");

		$leftYaxis->labels->style->color = "#89A54E";
		$leftYaxis->title->text = "Price";
		$leftYaxis->title->style->color = "#89A54E";

		$rightYaxis = new HighchartOption();
		$rightYaxis->title->text = "Volume";
		$rightYaxis->title->style->color = "#4572A7";

		$rightYaxis->labels->formatter = new HighchartJsExpr("function() {return this.value +' MT'; }");

		$rightYaxis->labels->style->color = "#4572A7";
		$rightYaxis->opposite = 1;
		$chart->yAxis = array($leftYaxis,$rightYaxis);

		$chart->tooltip->formatter = new HighchartJsExpr(
		    "function() {
		    	return '' + Highcharts.dateFormat('%e. %b  %Y',this.x) +' : '+ this.x +
		    	(this.series.name == 'Price' ? ' USD' : 'MT');
			}");

		$chart->legend->layout = "vertical";
		$chart->legend->align = "left";
		$chart->legend->x = 120;
		$chart->legend->verticalAlign = "top";
		$chart->legend->y = 100;
		$chart->legend->floating = 1;
		$chart->legend->backgroundColor = "#FFFFFF";

		//Supply Volume
		$supply = array();
		foreach ($volumes as $volume) {
			$date = date('Y, m-1',strtotime($volume->date));
			$supply[] = array(new HighchartJsExpr("Date.UTC($date)"),intval($volume->volume));
		}

		$chart->series[] = array(
		    'name' => "Supply",
		    'color' => "#4572A7",
		    'type' => "column",
		    'yAxis' => 1,
		    'data' =>$supply
		);

		//Market Prices Wholesale
		$wholesale_price = array();
		foreach ($prices as $price) {
			$date = date('Y, m-1',strtotime($price->date));
			$wholesale_price[] = array(new HighchartJsExpr("Date.UTC($date)"),intval($price->wholesale * 1000));
		}

		$chart->series[] = array(
		    'name' => "Wholesale Prices",
		    'color' => "#89A54E",
		    'type' => "spline",
		    'data' => $wholesale_price
		);

		//Market Prices Retail
		$retail_price = array();
		foreach ($prices as $price) {
			$data_price[] = floatval($price->retail * 1000);
			$date = date('Y, m-1',strtotime($price->date));
			$retail_price[] = array(new HighchartJsExpr("Date.UTC($date)"),intval($price->retail * 1000));
		}

		$chart->series[] = array(
		    'name' => "Retail Prices",
		    'color' => "#D62B1F",
		    'type' => "spline",
		    'data' => $retail_price
		);

		//Chart js data
		$js = $chart->render("chart");

    	//Layout
    	$this->template->set_layout('layout7');
	    $this->template->title('Product Wholesale Prices Vs National Availability Volumes');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/highcharts.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js').'/exporting.js"></script>');
	    $this->template->append_metadata('<script src="'.base_url('resources/js/').'/zebra_datepicker.js"></script>');

	    //View
	    $this->template->build('pages/compare',array('js'=>$js));

    }

}
