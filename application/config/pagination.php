<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Pagination configuration
$config["uri_segment"] = 4;
$config["num_links"] = 4;
$config["per_page"] = 5;

//Pagination customization
//Holder
$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';

//Start
$config['first_link'] = 'First';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';

//Current
$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';

//Numbers
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';

//Last
$config['last_link'] = 'Last';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

//Next
$config['next_link'] = '&raquo;';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';

//Prev
$config['prev_link'] = '&laquo;';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';